﻿using System.Web;
using System.Web.Mvc;

namespace Walk_The_Maze
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
