﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Walk_The_Maze.Startup))]
namespace Walk_The_Maze
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
