﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Walk_The_Maze.Models
{
    public class Score
    {
        public int ScoreID { get; set; }
        public string PlayerNickName { get; set; }
        public string MapName { get; set; }
        public DateTime DataUpload { get; set; }
        public float Points { get; set; }
    }
}
