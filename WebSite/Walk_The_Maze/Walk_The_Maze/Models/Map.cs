﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Walk_The_Maze.Models
{
    public class Map
    {
        [Key]
        [Required]
        [MaxLength(200)]
        [Display(Name = "Map Name")]
        public string MapName { get; set; }

        public string FileName { get; set; }

        public bool privado { get; set; }

        public string uploader { get; set; }
    }
}