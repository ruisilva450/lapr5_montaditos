﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Walk_The_Maze.Models
{
    public class Path
    {
        public int PathID { get; set; }
        public string PlayerNickName { get; set; }
        public string MapName { get; set; }
        public DateTime DataUpload { get; set; }
        public string PathLocation { get; set; }
    }
}