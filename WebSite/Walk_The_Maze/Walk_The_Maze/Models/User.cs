﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Walk_The_Maze.Models
{
    public class User
    {
        [Key]
        [Required]
        [MaxLength(20)]
        [Display(Name = "Nick Name")]
        public string NickName { get; set; } 

        [Required]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        public string UserPassword { get; set; }

    }
}