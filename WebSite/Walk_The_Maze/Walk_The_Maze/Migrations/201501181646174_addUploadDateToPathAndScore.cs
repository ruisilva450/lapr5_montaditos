namespace Walk_The_Maze.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addUploadDateToPathAndScore : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Path", "DataUpload", c => c.DateTime(nullable: false));
            AddColumn("dbo.Score", "DataUpload", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Score", "DataUpload");
            DropColumn("dbo.Path", "DataUpload");
        }
    }
}
