namespace Walk_The_Maze.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class api_player : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.User", "api_key", c => c.Guid());
        }
        
        public override void Down()
        {
            DropColumn("dbo.User", "api_key");
        }
    }
}
