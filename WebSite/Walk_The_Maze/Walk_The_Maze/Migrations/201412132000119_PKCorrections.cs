namespace Walk_The_Maze.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PKCorrections : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Path", "Player_NickName", "dbo.User");
            DropForeignKey("dbo.Score", "PlayerNickName", "dbo.User");
            DropForeignKey("dbo.Path", "Map_MapName", "dbo.Map");
            DropIndex("dbo.Path", new[] { "Map_MapName" });
            DropIndex("dbo.Path", new[] { "Player_NickName" });
            DropIndex("dbo.Score", new[] { "PlayerNickName" });
            DropPrimaryKey("dbo.User");
            DropPrimaryKey("dbo.Map");
            AlterColumn("dbo.User", "NickName", c => c.String(nullable: false, maxLength: 200));
            AlterColumn("dbo.Map", "MapName", c => c.String(nullable: false, maxLength: 200));
            AlterColumn("dbo.Path", "Map_MapName", c => c.String(maxLength: 200));
            AlterColumn("dbo.Path", "Player_NickName", c => c.String(maxLength: 200));
            AlterColumn("dbo.Score", "PlayerNickName", c => c.String(maxLength: 200));
            AddPrimaryKey("dbo.User", "NickName");
            AddPrimaryKey("dbo.Map", "MapName");
            CreateIndex("dbo.Path", "Map_MapName");
            CreateIndex("dbo.Path", "Player_NickName");
            CreateIndex("dbo.Score", "PlayerNickName");
            AddForeignKey("dbo.Path", "Player_NickName", "dbo.User", "NickName");
            AddForeignKey("dbo.Score", "PlayerNickName", "dbo.User", "NickName");
            AddForeignKey("dbo.Path", "Map_MapName", "dbo.Map", "MapName");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Path", "Map_MapName", "dbo.Map");
            DropForeignKey("dbo.Score", "PlayerNickName", "dbo.User");
            DropForeignKey("dbo.Path", "Player_NickName", "dbo.User");
            DropIndex("dbo.Score", new[] { "PlayerNickName" });
            DropIndex("dbo.Path", new[] { "Player_NickName" });
            DropIndex("dbo.Path", new[] { "Map_MapName" });
            DropPrimaryKey("dbo.Map");
            DropPrimaryKey("dbo.User");
            AlterColumn("dbo.Score", "PlayerNickName", c => c.String(maxLength: 128));
            AlterColumn("dbo.Path", "Player_NickName", c => c.String(maxLength: 128));
            AlterColumn("dbo.Path", "Map_MapName", c => c.String(maxLength: 128));
            AlterColumn("dbo.Map", "MapName", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.User", "NickName", c => c.String(nullable: false, maxLength: 128));
            AddPrimaryKey("dbo.Map", "MapName");
            AddPrimaryKey("dbo.User", "NickName");
            CreateIndex("dbo.Score", "PlayerNickName");
            CreateIndex("dbo.Path", "Player_NickName");
            CreateIndex("dbo.Path", "Map_MapName");
            AddForeignKey("dbo.Path", "Map_MapName", "dbo.Map", "MapName");
            AddForeignKey("dbo.Score", "PlayerNickName", "dbo.User", "NickName");
            AddForeignKey("dbo.Path", "Player_NickName", "dbo.User", "NickName");
        }
    }
}
