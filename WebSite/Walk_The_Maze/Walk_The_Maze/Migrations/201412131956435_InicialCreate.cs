namespace Walk_The_Maze.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InicialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.User",
                c => new
                    {
                        NickName = c.String(nullable: false, maxLength: 128),
                        UserPassword = c.String(nullable: false),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.NickName);
            
            CreateTable(
                "dbo.Map",
                c => new
                    {
                        MapName = c.String(nullable: false, maxLength: 128),
                        MapLocation = c.String(),
                    })
                .PrimaryKey(t => t.MapName);
            
            CreateTable(
                "dbo.Path",
                c => new
                    {
                        PathID = c.Int(nullable: false, identity: true),
                        PathLocation = c.String(),
                        Map_MapName = c.String(maxLength: 128),
                        Player_NickName = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.PathID)
                .ForeignKey("dbo.Map", t => t.Map_MapName)
                .ForeignKey("dbo.User", t => t.Player_NickName)
                .Index(t => t.Map_MapName)
                .Index(t => t.Player_NickName);
            
            CreateTable(
                "dbo.Score",
                c => new
                    {
                        ScoreID = c.Int(nullable: false, identity: true),
                        PlayerNickName = c.String(maxLength: 128),
                        MapName = c.String(),
                        Points = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.ScoreID)
                .ForeignKey("dbo.User", t => t.PlayerNickName)
                .Index(t => t.PlayerNickName);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Score", "PlayerNickName", "dbo.User");
            DropForeignKey("dbo.Path", "Player_NickName", "dbo.User");
            DropForeignKey("dbo.Path", "Map_MapName", "dbo.Map");
            DropIndex("dbo.Score", new[] { "PlayerNickName" });
            DropIndex("dbo.Path", new[] { "Player_NickName" });
            DropIndex("dbo.Path", new[] { "Map_MapName" });
            DropTable("dbo.Score");
            DropTable("dbo.Path");
            DropTable("dbo.Map");
            DropTable("dbo.User");
        }
    }
}
