namespace Walk_The_Maze.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class mapChanges : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Map", "privado", c => c.Boolean(nullable: false));
            AddColumn("dbo.Map", "uploader", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Map", "uploader");
            DropColumn("dbo.Map", "privado");
        }
    }
}
