namespace Walk_The_Maze.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class mapCorrections : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Map", "FileName", c => c.String());
            DropColumn("dbo.Map", "MapLocation");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Map", "MapLocation", c => c.String());
            DropColumn("dbo.Map", "FileName");
        }
    }
}
