namespace Walk_The_Maze.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PathCorrection : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Path", "Map_MapName", "dbo.Map");
            DropIndex("dbo.Path", new[] { "Map_MapName" });
            AddColumn("dbo.Path", "MapName", c => c.String());
            DropColumn("dbo.Path", "Map_MapName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Path", "Map_MapName", c => c.String(maxLength: 200));
            DropColumn("dbo.Path", "MapName");
            CreateIndex("dbo.Path", "Map_MapName");
            AddForeignKey("dbo.Path", "Map_MapName", "dbo.Map", "MapName");
        }
    }
}
