// <auto-generated />
namespace Walk_The_Maze.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.1-30610")]
    public sealed partial class addUploadDateToPathAndScore : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(addUploadDateToPathAndScore));
        
        string IMigrationMetadata.Id
        {
            get { return "201501181646174_addUploadDateToPathAndScore"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
