namespace Walk_The_Maze.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PlayerCorrections : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Path", "Player_NickName", "dbo.User");
            DropForeignKey("dbo.Score", "PlayerNickName", "dbo.User");
            DropIndex("dbo.Path", new[] { "Player_NickName" });
            DropIndex("dbo.Score", new[] { "PlayerNickName" });
            AddColumn("dbo.Path", "PlayerNickName", c => c.String());
            AlterColumn("dbo.Score", "PlayerNickName", c => c.String());
            DropColumn("dbo.Path", "Player_NickName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Path", "Player_NickName", c => c.String(maxLength: 200));
            AlterColumn("dbo.Score", "PlayerNickName", c => c.String(maxLength: 200));
            DropColumn("dbo.Path", "PlayerNickName");
            CreateIndex("dbo.Score", "PlayerNickName");
            CreateIndex("dbo.Path", "Player_NickName");
            AddForeignKey("dbo.Score", "PlayerNickName", "dbo.User", "NickName");
            AddForeignKey("dbo.Path", "Player_NickName", "dbo.User", "NickName");
        }
    }
}
