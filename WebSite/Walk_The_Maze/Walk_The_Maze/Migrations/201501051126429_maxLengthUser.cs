namespace Walk_The_Maze.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class maxLengthUser : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.User");
            AlterColumn("dbo.User", "NickName", c => c.String(nullable: false, maxLength: 20));
            AddPrimaryKey("dbo.User", "NickName");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.User");
            AlterColumn("dbo.User", "NickName", c => c.String(nullable: false, maxLength: 200));
            AddPrimaryKey("dbo.User", "NickName");
        }
    }
}
