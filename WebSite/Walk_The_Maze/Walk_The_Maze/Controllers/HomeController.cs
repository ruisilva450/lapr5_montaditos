﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Walk_The_Maze.DAL;
using Walk_The_Maze.Models;

namespace Walk_The_Maze.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(User u)
        {
            using (WalkTheMazeContext db = new WalkTheMazeContext())
            {
                var user = db.Users.Where(a => a.NickName.Equals(u.NickName) && a.UserPassword.Equals(u.UserPassword)).FirstOrDefault();

                if (user != null)
                {
                    if (user is Administrator)
                    {
                        Session["Role"] = "Administrator";
                    }
                    else if (user is Player)
                    {
                        Session["Role"] = "Player";
                    }
                    else
                    {
                        Session["Role"] = "ERROR";
                    }

                    Session["NickName"] = user.NickName.ToString();
                    return View("Index");

                }
            }

            return View("Login");
        }

        public ActionResult Logout()
        {
            Session.RemoveAll();
            Session.Clear();
            Session.Abandon();
            return RedirectToAction("Login");
        }
    }
}