﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Walk_The_Maze.DAL;
using Walk_The_Maze.Models;
using Ionic.Zip;
using System.IO;

namespace Walk_The_Maze.Controllers
{
    public class MapsController : Controller
    {
        private WalkTheMazeContext db = new WalkTheMazeContext();

        // GET: Maps
        public ActionResult Index()
        {
            List<Map> mapsToShow;
            if (Session.IsNewSession)
            {
                mapsToShow = db.Maps.Where(map => map.privado == false).ToList();
            }
            else
            {
                if (Session["Role"].ToString() == "Administrator")
                {
                    mapsToShow = db.Maps.ToList();
                }
                else
                {
                    string nickName = Session["NickName"].ToString();
                    mapsToShow = db.Maps.Where(map => map.privado == false).ToList();
                    List<Map> ownMaps = db.Maps.Where(map => map.uploader == nickName).ToList();
                    if (mapsToShow.Count.Equals(0))
                        mapsToShow = ownMaps;
                    else
                        foreach (var map in ownMaps)
                            if (0 == mapsToShow.Where(m => m.MapName == map.MapName).ToList().Count )
                                mapsToShow.Add(map);
                }
            }

            return View(mapsToShow);
        }

        // GET: Maps/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Map map = db.Maps.Find(id);
            if (map == null)
            {
                return HttpNotFound();
            }
            return View(map);
        }

        // GET: Maps/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Maps/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "MapName,FileName,privado")] Map map)
        {
            
            if (ModelState.IsValid)
            {
                map.uploader = Session["NickName"].ToString();
                db.Maps.Add(map);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(map);
        }

        [HttpPost]
        public ActionResult UploadFile(HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
                try
                {
                    string path = System.IO.Path.Combine(Server.MapPath(Resources.General.MapFileFormat),
                                               System.IO.Path.GetFileName(file.FileName));
                    file.SaveAs(path);

                    // TODO

                    ViewBag.Message = "File uploaded successfully";
                }
                catch (Exception ex)
                {
                    ViewBag.Message = "ERROR:" + ex.Message.ToString();
                }
            else
            {
                ViewBag.Message = "You have not specified a file.";
            }
            return View();
        }

        // GET: Maps/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Map map = db.Maps.Find(id);
            if (map == null)
            {
                return HttpNotFound();
            }
            return View(map);
        }

        // POST: Maps/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "MapName,FileName,privado,uploader")] Map map)
        {
            if (ModelState.IsValid)
            {
                db.Entry(map).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(map);
        }

        // GET: Maps/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Map map = db.Maps.Find(id);
            if (map == null)
            {
                return HttpNotFound();
            }
            return View(map);
        }

        // POST: Maps/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            Map map = db.Maps.Find(id);
            db.Maps.Remove(map);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
