﻿using Walk_The_Maze.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Walk_The_Maze.DAL
{
    public class WalkTheMazeContext : DbContext
    {

        public DbSet<Map> Maps { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Player> Players { get; set; }
        public DbSet<Administrator> Administrators { get; set; }
        public DbSet<Score> Scores { get; set; }
        public DbSet<Path> Paths { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}