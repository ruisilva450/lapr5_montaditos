﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.IO;
using Walk_The_Maze.DAL;
using Walk_The_Maze.Models;
using System.Web.Hosting;


namespace Walk_The_Maze.WebServices
{

    public class FileListElement
    {
        public string fileName { get; set; }
        public int fileSize { get; set; }
    }

    public class FileObject
    {
        public byte[] bytes { get; set; }
        public string filename { get; set; }
        public string fileType { get; set; }
    }

    public class ScoreObject
    {
        public string nickName { get; set; }
        public float score { get; set; }
    }

    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WebService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select WebService.svc or WebService.svc.cs at the Solution Explorer and start debugging.
    public class WebService : IWebService
    {
        WalkTheMazeContext db = new WalkTheMazeContext();

        public string login(string nickName, string password)
        {
            List<Player> listPlayers = db.Players.Where(player => player.NickName == nickName).ToList();
            if (listPlayers.Count != 0)
            {
                Player p = listPlayers.First();
                if (null != p)
                {
                    if (p.UserPassword == password)
                        return p.api_key.ToString(); // nickName && password
                    else
                        return ""; // nickName
                }
            }

            return ""; // error
        }

        public List<FileListElement> GetMapNameList(string api_key)
        {
            List<FileListElement> mapNameList = new List<FileListElement>();
            List<Map> mapsList = db.Maps.ToList();
            if (mapsList.Count != 0)
            {
                foreach (Map map in mapsList)
                {
                    FileListElement mapElem = new FileListElement();
                    mapElem.fileName = map.MapName.ToString();
                    string mapPath = HostingEnvironment.MapPath(Resources.General.MapDefaultLocation) + map.FileName.ToString();
                    if (File.Exists(mapPath))
                    {
                        if (map.privado)
                        {
                            if (api_key != "")
                            {
                                Guid sentApi = new Guid(api_key);
                                List<Player> listPlayers = db.Players.Where(player => player.api_key == sentApi).ToList();
                                if (listPlayers.Count != 0)
                                {
                                    Player player = listPlayers.First();
                                    if (player != null)
                                    {
                                        if (player.NickName == map.uploader)
                                        {
                                            mapElem.fileSize = File.ReadAllBytes(mapPath).Length;
                                            mapNameList.Add(mapElem);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            mapElem.fileSize = File.ReadAllBytes(mapPath).Length;
                            mapNameList.Add(mapElem);
                        }

                    }
                }
                return mapNameList;
            }
            return null;
        }

        public FileObject GetMap(string api_key, string mapName)
        {
            FileObject mapToSend = new FileObject();
            List<Map> listMaps = db.Maps.Where(map => map.MapName == mapName).ToList();
            if (listMaps.Count != 0)
            {
                Map selectedMap = listMaps.First();

                if (selectedMap != null)
                {
                    string FileName = HostingEnvironment.MapPath(Resources.General.MapDefaultLocation + selectedMap.FileName.ToString());

                    if (File.Exists(FileName))
                    {
                        if (selectedMap.privado)
                        {
                            if (api_key != "")
                            {
                                Guid sentApi = new Guid(api_key);
                                List<Player> listPlayers = db.Players.Where(player => player.api_key == sentApi).ToList();
                                if (listPlayers.Count != 0)
                                {
                                    Player player = listPlayers.First();
                                    if (player != null)
                                    {
                                        if (player.NickName == selectedMap.uploader)
                                        {
                                            mapToSend.bytes = File.ReadAllBytes(FileName);
                                            mapToSend.filename = System.IO.Path.GetFileName(FileName).Split('.').First();
                                            mapToSend.fileType = System.IO.Path.GetExtension(FileName).Replace(".", "");
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            mapToSend.bytes = File.ReadAllBytes(FileName);
                            mapToSend.filename = System.IO.Path.GetFileName(FileName).Split('.').First();
                            mapToSend.fileType = System.IO.Path.GetExtension(FileName).Replace(".", "");
                        }

                        return mapToSend;
                    }
                }
            }

            return null;
        }

        public int SendScore(string api_key, string map_name, float score)
        {
            Guid sentApi = new Guid(api_key);
            List<Player> listPlayers = db.Players.Where(player => player.api_key == sentApi).ToList();
            if (listPlayers.Count != 0)
            {
                Player player = listPlayers.First();
                if (player != null)
                {
                    Score newScore = new Score();
                    newScore.DataUpload = DateTime.Now;
                    newScore.PlayerNickName = player.NickName.ToString();
                    newScore.MapName = map_name;
                    newScore.Points = score;

                    db.Scores.Add(newScore);

                    db.SaveChanges();
                    return 1;
                }
            }
            return 0;
        }

        public List<float> getPlayerScore(string nickName)
        {
            List<float> scores = new List<float>();
            List<Score> allScores = db.Scores.ToList();
            List<Score> scoresOfPlayer = db.Scores.Where(score => score.PlayerNickName == nickName).ToList();

            if (scoresOfPlayer.Count != 0)
            {
                foreach (var item in scoresOfPlayer)
                    scores.Add(item.Points);

                return scores;
            }

            return null;
        }

        public List<string> getPlayersList()
        {
            List<string> playersList = new List<string>();

            foreach (Player player in db.Players.ToList())
                playersList.Add(player.NickName);

            return playersList;
        }

        public List<ScoreObject> getLeaderBoard()
        {
            List<ScoreObject> leaderBoard = new List<ScoreObject>();

            List<Player> players = db.Players.ToList();

            if (players.Count != 0)
            {
                foreach (var playerItem in players)
                {
                    List<float> scoreOfPlayerItem = getPlayerScore(playerItem.NickName);
                    if (scoreOfPlayerItem != null)
                    {
                        ScoreObject highScoreOfPlayerItem = new ScoreObject();
                        highScoreOfPlayerItem.score = scoreOfPlayerItem.Min();
                        highScoreOfPlayerItem.nickName = playerItem.NickName;
                        leaderBoard.Add(highScoreOfPlayerItem);
                    }
                }

                return leaderBoard;
            }

            return null;
        }

        public List<ScoreObject> getMapLeaderBoard(string mapName, int nRecords)
        {
            List<ScoreObject> leaderBoard = new List<ScoreObject>();

            List<Score> MapScores = db.Scores.Where(score => score.MapName == mapName).ToList();

            if (MapScores.Count != 0)
            {
                List<Score> SortedMapScores = MapScores.OrderBy(o => o.Points).Take(nRecords).ToList();

                foreach (Score scoreItem in SortedMapScores)
                {
                    ScoreObject score = new ScoreObject();

                    score.nickName = scoreItem.PlayerNickName;
                    score.score = scoreItem.Points;

                    leaderBoard.Add(score);
                }

                return leaderBoard;
            }

            return null;
        }

        public int SendPath(string api_key, string mapFileName, string pathName, byte[] bytes)
        {
            string pathLocation = HostingEnvironment.MapPath(Resources.General.PathDefaultLocation);

            Guid sentApi = new Guid(api_key);
            List<Player> listPlayers = db.Players.Where(player => player.api_key == sentApi).ToList();
            if (listPlayers.Count != 0)
            {
                Player player = listPlayers.First();
                if (player != null)
                {
                    FileObject pathToSend = new FileObject();
                    List<Map> listMaps = db.Maps.Where(map => map.FileName == mapFileName).ToList();
                    if (listMaps.Count != 0)
                    {
                        Map selectedMap = listMaps.First();

                        if (selectedMap != null)
                        {
                            pathLocation += pathName + ".path";
                            bool ficheiroCriado = false;
                            using (FileStream fs = File.Create(pathLocation))
                            {
                                // Add some information to the file.
                                fs.Write(bytes, 0, bytes.Length);
                                ficheiroCriado = true;
                            }

                            if (ficheiroCriado)
                            {
                                Models.Path newEntry = new Models.Path(); ;
                                newEntry.DataUpload = DateTime.Now;
                                newEntry.MapName = selectedMap.MapName;
                                newEntry.PathLocation = pathName + ".path";
                                newEntry.PlayerNickName = player.NickName;

                                db.Paths.Add(newEntry);
                                db.SaveChanges();
                                return 1;
                            }

                        }
                    }
                }
            }

            return 0;
        }

        public List<FileListElement> getPathsList(string nickName, string mapName)
        {
            List<FileListElement> pathNameList = new List<FileListElement>();
            List<Models.Path> mapPlayerPathList
                = db.Paths.Where(path => path.PlayerNickName == nickName).ToList().Where(path => path.MapName == mapName).ToList();
            foreach (Models.Path path in mapPlayerPathList)
            {
                FileListElement pathElem = new FileListElement();

                pathElem.fileName = path.PathLocation.ToString();
                byte[] bytes = File.ReadAllBytes(HostingEnvironment.MapPath("~/Paths/") + path.PathLocation.ToString());
                pathElem.fileSize = bytes.Length;
                pathNameList.Add(pathElem);
            }
            return pathNameList;
        }

        public FileObject GetPath(string nickName, string pathName)
        {
            string pathLocation = HostingEnvironment.MapPath("~/Paths/");

            List<Player> listPlayers = db.Players.Where(player => player.NickName == nickName).ToList();
            if (listPlayers.Count != 0)
            {
                Player player = listPlayers.First();
                if (player != null)
                {

                    FileObject pathToSend = new FileObject();
                    List<Models.Path> listPaths = db.Paths.Where(path => path.PathLocation == pathName).ToList();
                    if (listPaths.Count != 0)
                    {
                        Models.Path selectedPath = listPaths.First();

                        if (selectedPath != null)
                        {
                            string FileName = HostingEnvironment.MapPath("~/Paths/" + selectedPath.PathLocation.ToString());

                            if (File.Exists(FileName))
                            {
                                pathToSend.bytes = File.ReadAllBytes(FileName);
                                pathToSend.filename = System.IO.Path.GetFileName(FileName);
                                pathToSend.fileType = System.IO.Path.GetExtension(FileName).Replace(".", "");

                                return pathToSend;
                            }
                        }

                    }
                }
                return null;
            }
            return null;
        }

        public List<FileListElement> getMusicsList(string mapName)
        {
            string mapsLocation = HostingEnvironment.MapPath("~/Maps/");

            List<FileListElement> MusicsNameList = new List<FileListElement>();
            List<Map> mapMusicsList = db.Maps.Where(map => map.MapName == mapName).ToList();

            if (mapMusicsList.Count != 0)
            {
                Map mapaEscolhido = mapMusicsList.First();
                mapsLocation += System.IO.Path.GetFileNameWithoutExtension(mapaEscolhido.FileName);
                if (Directory.Exists(mapsLocation))
                {
                    foreach (string ficheiro in Directory.GetFiles(mapsLocation, "*.wav"))
                    {
                        FileListElement musicaElem = new FileListElement();
                        musicaElem.fileSize = File.ReadAllBytes(ficheiro).Length;
                        musicaElem.fileName = System.IO.Path.GetFileName(ficheiro);
                        MusicsNameList.Add(musicaElem);
                    }
                }
                else
                    return null;
            }
            return MusicsNameList;
        }

        public FileObject GetMusic(string mapName, string songName)
        {
            string mapsLocation = HostingEnvironment.MapPath("~/Maps/");

            FileObject musicToSend = new FileObject();
            List<Map> listMaps = db.Maps.Where(map => map.MapName == mapName).ToList();
            if (listMaps.Count != 0)
            {
                Map selectedMap = listMaps.First();

                if (selectedMap != null)
                {
                    mapsLocation += System.IO.Path.GetFileNameWithoutExtension(selectedMap.FileName);

                    if (Directory.Exists(mapsLocation))
                    {
                        foreach (string ficheiro in Directory.GetFiles(mapsLocation, "*.wav"))
                        {
                            if (System.IO.Path.GetFileName(ficheiro) == songName)
                            {
                                musicToSend.bytes = File.ReadAllBytes(ficheiro);
                                musicToSend.filename = System.IO.Path.GetFileName(ficheiro).Split('.').First();
                                musicToSend.fileType = System.IO.Path.GetExtension(ficheiro).Replace(".", "");
                            }
                        }


                        return musicToSend;
                    }
                }
            }
            return null;
        }

        public List<FileListElement> getTexturesList(string mapName)
        {
            string texturesLocation = HostingEnvironment.MapPath("~/Maps/");

            List<FileListElement> TexturesNameList = new List<FileListElement>();
            List<Map> mapTexturesList = db.Maps.Where(map => map.MapName == mapName).ToList();

            if (mapTexturesList.Count != 0)
            {
                Map mapaEscolhido = mapTexturesList.First();
                texturesLocation += System.IO.Path.GetFileNameWithoutExtension(mapaEscolhido.FileName);
                if (Directory.Exists(texturesLocation))
                {
                    foreach (string ficheiro in Directory.GetFiles(texturesLocation, "*.jpg"))
                    {
                        FileListElement texturaElem = new FileListElement();

                        texturaElem.fileSize = File.ReadAllBytes(ficheiro).Length;
                        texturaElem.fileName = System.IO.Path.GetFileName(ficheiro);
                        TexturesNameList.Add(texturaElem);
                    }
                    foreach (string ficheiro in Directory.GetFiles(texturesLocation, "*.bmp"))
                    {
                        FileListElement texturaElem = new FileListElement();

                        texturaElem.fileSize = File.ReadAllBytes(ficheiro).Length;
                        texturaElem.fileName = System.IO.Path.GetFileName(ficheiro);
                        TexturesNameList.Add(texturaElem);
                    }
                }
                else
                {
                    return null;
                }
            }


            return TexturesNameList;
        }

        public FileObject GetTexture(string mapName, string textureName)
        {
            string mapsLocation = HostingEnvironment.MapPath("~/Maps/");

            FileObject TextureToSend = new FileObject();
            List<Map> listMaps = db.Maps.Where(map => map.MapName == mapName).ToList();
            if (listMaps.Count != 0)
            {
                Map selectedMap = listMaps.First();

                if (selectedMap != null)
                {
                    mapsLocation += System.IO.Path.GetFileNameWithoutExtension(selectedMap.FileName);

                    if (Directory.Exists(mapsLocation))
                    {
                        foreach (string ficheiro in Directory.GetFiles(mapsLocation, "*.jpg"))
                        {
                            if (System.IO.Path.GetFileName(ficheiro) == textureName)
                            {
                                TextureToSend.bytes = File.ReadAllBytes(ficheiro);
                                TextureToSend.filename = System.IO.Path.GetFileName(ficheiro).Split('.').First();
                                TextureToSend.fileType = System.IO.Path.GetExtension(ficheiro).Replace(".", "");
                            }
                        }

                        foreach (string ficheiro in Directory.GetFiles(mapsLocation, "*.bmp"))
                        {
                            if (System.IO.Path.GetFileName(ficheiro) == textureName)
                            {
                                TextureToSend.bytes = File.ReadAllBytes(ficheiro);
                                TextureToSend.filename = System.IO.Path.GetFileName(ficheiro).Split('.').First();
                                TextureToSend.fileType = System.IO.Path.GetExtension(ficheiro).Replace(".", "");
                            }
                        }

                        return TextureToSend;
                    }
                }
            }
            return null;
        }

        public List<FileListElement> getModelsList(string mapName)
        {
            List<FileListElement> ModelsNameList = new List<FileListElement>();

            string modelsLocation = HostingEnvironment.MapPath("~/Maps/");

            List<Map> mapModelsList = db.Maps.Where(map => map.MapName == mapName).ToList();

            if (mapModelsList.Count != 0)
            {
                Map mapaEscolhido = mapModelsList.First();
                modelsLocation += System.IO.Path.GetFileNameWithoutExtension(mapaEscolhido.FileName);
                if (Directory.Exists(modelsLocation))
                {
                    foreach (string ficheiro in Directory.GetFiles(modelsLocation, "*.mdl"))
                    {
                        FileListElement modelElem = new FileListElement();

                        modelElem.fileSize = File.ReadAllBytes(ficheiro).Length;
                        modelElem.fileName = System.IO.Path.GetFileName(ficheiro);
                        ModelsNameList.Add(modelElem);
                    }
                }
                else
                    return null;
            }

            return ModelsNameList;
        }

        public FileObject getModel(string mapName, string modelName)
        {
            string mapsLocation = HostingEnvironment.MapPath("~/Maps/");

            FileObject ModelToSend = new FileObject();
            List<Map> listMaps = db.Maps.Where(map => map.MapName == mapName).ToList();
            if (listMaps.Count != 0)
            {
                Map selectedMap = listMaps.First();

                if (selectedMap != null)
                {
                    mapsLocation += System.IO.Path.GetFileNameWithoutExtension(selectedMap.FileName);

                    if (Directory.Exists(mapsLocation))
                    {
                        foreach (string ficheiro in Directory.GetFiles(mapsLocation, "*.mdl"))
                        {
                            if (System.IO.Path.GetFileName(ficheiro) == modelName)
                            {
                                ModelToSend.bytes = File.ReadAllBytes(ficheiro);
                                ModelToSend.filename = System.IO.Path.GetFileName(ficheiro).Split('.').First();
                                ModelToSend.fileType = System.IO.Path.GetExtension(ficheiro).Replace(".", "");
                            }
                        }

                        return ModelToSend;
                    }
                }
            }
            return null;
        }
    }
}
