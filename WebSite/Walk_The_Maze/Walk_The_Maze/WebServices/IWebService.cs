﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Walk_The_Maze.Models;

namespace Walk_The_Maze.WebServices
{
    [ServiceContract]
    public interface IWebService
    {
        [OperationContract]
        string login(string nickName, string password);

        [OperationContract]
        List<FileListElement> GetMapNameList(string api_key);

        [OperationContract]
        FileObject GetMap(string api_key, string mapName);

        [OperationContract]
        int SendScore(string api_key, string map_name, float score);

        [OperationContract]
        List<float> getPlayerScore(string nickName);

        [OperationContract]
        List<string> getPlayersList();

        [OperationContract]
        List<ScoreObject> getLeaderBoard();

        [OperationContract]
        List<ScoreObject> getMapLeaderBoard(string mapName, int nRecords);

        [OperationContract]
        int SendPath(string api_key, string mapFileName, string pathName, byte[] bytes);

        [OperationContract]
        List<FileListElement> getPathsList(string nickName, string mapName);

        [OperationContract]
        FileObject GetPath(string nickName, string pathName);

        [OperationContract]
        List<FileListElement> getMusicsList(string mapName);

        [OperationContract]
        FileObject GetMusic(string mapName, string songName);

        [OperationContract]
        List<FileListElement> getTexturesList(string mapName);

        [OperationContract]
        FileObject GetTexture(string mapName, string textureName);

        [OperationContract]
        List<FileListElement> getModelsList(string mapName);

        [OperationContract]
        FileObject getModel(string mapName, string modelName);
    }
}
