#include <list>


/************************************************************************/
/* Neste ficheiro header estao implementados metodos de obtencao de		*/
/* dados do webservice.													*/
/************************************************************************/

using namespace std;

namespace WebService
{
	//Converts type WCHAR* to char*
	char* wchar_to_char(WCHAR* word){
		int size = wcslen(word);
		if (size == 0)
		{
			return "";
		}
		char *reply = (char *)malloc(size);
		char DefChar = ' ';
		WideCharToMultiByte(
			CP_ACP, 0, word, -1, reply, size * 2, &DefChar, NULL);

		return reply;
	}

	//************************************
	// Purpose:	  Faz a ligacao ao servidor e pede a api_key correspondente a combinacao username + password fornecida
	// Method:    getApi
	// FullName:  getApi
	// Access:    public 
	// Returns:   int
	// Parameter: string username
	// Parameter: string password
	//************************************
	string getApi(string username, string password)
	{
		HRESULT hr = ERROR_SUCCESS;
		WS_ERROR* error = NULL;
		WS_HEAP* heap = NULL;
		WS_SERVICE_PROXY* proxy = NULL;

		WS_ENDPOINT_ADDRESS address = {};

		//endereco do servico
		WS_STRING url = WS_STRING_VALUE(L"http://wvm024.dei.isep.ipp.pt/WalkTheMaze/WebServices/webservice.svc");
		address.url = url;

		hr = WsCreateHeap(4096, 512, NULL, 0, &heap, error);
		WS_HTTP_BINDING_TEMPLATE templ = {};

		//criacao do proxy para o servico
		hr = BasicHttpBinding_IWebService_CreateServiceProxy(&templ, NULL, 0, &proxy, error);
		hr = WsOpenServiceProxy(proxy, &address, NULL, error);

		WCHAR* result;

		const char* user = username.c_str();
		const char* pass = password.c_str();

		// Convert to a wchar_t*
		size_t origsizeu = strlen(user) + 1;
		const size_t newsizeu = 100;
		size_t convertedCharsu = 0;
		wchar_t u[newsizeu];
		mbstowcs_s(&convertedCharsu, u, origsizeu, user, _TRUNCATE);

		// Convert to a wchar_t*
		size_t origsizep = strlen(pass) + 1;
		const size_t newsizep = 100;
		size_t convertedCharsp = 0;
		wchar_t p[newsizep];
		mbstowcs_s(&convertedCharsp, p, origsizep, pass, _TRUNCATE);

		hr = BasicHttpBinding_IWebService_login(proxy, u, p, &result, heap, NULL, 0, NULL, error);

		if (hr == 0x803d0010)
			return "";

		char *reply = wchar_to_char(result);

		char* fail = "";

		if (strcmp(reply, fail) == 0)
			return "";

		return reply;
	}

	vector<FileListElement> getMapList(string api_key)
	{
		HRESULT hr = ERROR_SUCCESS;
		WS_ERROR* error = NULL;
		WS_HEAP* heap = NULL;
		WS_SERVICE_PROXY* proxy = NULL;

		WS_ENDPOINT_ADDRESS address = {};

		//endereco do servico
		WS_STRING url = WS_STRING_VALUE(L"http://wvm024.dei.isep.ipp.pt/WalkTheMaze/WebServices/webservice.svc");
		address.url = url;

		hr = WsCreateHeap(4096, 512, NULL, 0, &heap, error);
		WS_HTTP_BINDING_TEMPLATE templ = {};

		//criacao do proxy para o servico
		hr = BasicHttpBinding_IWebService_CreateServiceProxy(&templ, NULL, 0, &proxy, error);
		hr = WsOpenServiceProxy(proxy, &address, NULL, error);

		FileListElement ** result;

		const char* api = api_key.c_str();

		// Convert to a wchar_t*
		size_t origsizea = strlen(api) + 1;
		const size_t newsizea = 100;
		size_t convertedCharsa = 0;
		wchar_t a[newsizea];
		mbstowcs_s(&convertedCharsa, a, origsizea, api, _TRUNCATE);

		unsigned int mapCount;

		hr = BasicHttpBinding_IWebService_GetMapNameList(proxy, a, &mapCount, &result, heap, NULL, 0, NULL, error);

		vector<FileListElement> mapList;
		if (hr == 0x803d0013)
			return vector<FileListElement>();

		for (unsigned int i = 0; i < mapCount; i++)
			mapList.push_back(*result[i]);

		return mapList;
	}

	//ex: para "test3" vai retornar "demo3"
	string getMap(string api_key, FileListElement map_escolhido)
	{

		HRESULT hr = ERROR_SUCCESS;
		WS_ERROR* error = NULL;
		WS_HEAP* heap = NULL;
		WS_SERVICE_PROXY* proxy = NULL;

		WS_ENDPOINT_ADDRESS address = {};

		//endereco do servico
		WS_STRING url = WS_STRING_VALUE(L"http://wvm024.dei.isep.ipp.pt/WalkTheMaze/WebServices/webservice.svc");
		address.url = url;

		int heapSize = map_escolhido.fileSize + 1000000;

		hr = WsCreateHeap(heapSize, 512, NULL, 0, &heap, error);
		WS_HTTP_BINDING_TEMPLATE templ = {};

		//criacao do proxy para o servico
		hr = BasicHttpBinding_IWebService_CreateServiceProxy(&templ, NULL, 0, &proxy, error);
		hr = WsOpenServiceProxy(proxy, &address, NULL, error);

		const char* api = api_key.c_str();

		// Convert to a wchar_t*
		size_t origsizea = strlen(api) + 1;
		const size_t newsizea = 100;
		size_t convertedCharsa = 0;
		wchar_t a[newsizea];
		mbstowcs_s(&convertedCharsa, a, origsizea, api, _TRUNCATE);

		FileObject* file;
		hr = BasicHttpBinding_IWebService_GetMap(proxy, a, map_escolhido.fileName, &file, heap, NULL, 0, NULL, error);


		byte* s = (byte *)malloc(map_escolhido.fileSize);
		s = file->bytes.bytes;

		string fileName = wchar_to_char(file->filename);

		string fileType = wchar_to_char(file->fileType);

		string filePath = MAPAS_LOCATION + fileName + "." + fileType;

		FILE * pFile1;
		FILE * pFile2;

		errno_t err1;
		errno_t err2;

		string defaultMapPath = RESOURCES_LOCATION + string("mapa.") + fileType;

		err1 = fopen_s(&pFile1, filePath.c_str(), "wb");
		err2 = fopen_s(&pFile2, defaultMapPath.c_str(), "wb");

		if (err1 == 0 && err2 == 0)
		{
			fwrite(s, sizeof(byte), map_escolhido.fileSize, pFile1);
			fwrite(s, sizeof(byte), map_escolhido.fileSize, pFile2);
			fclose(pFile1);
			fclose(pFile2);
			return fileName;
		}
		else{
			return "";
		}

	}

	bool sendScore(string api_key, string mapaName, float score)
	{
		HRESULT hr = ERROR_SUCCESS;
		WS_ERROR* error = NULL;
		WS_HEAP* heap = NULL;
		WS_SERVICE_PROXY* proxy = NULL;

		WS_ENDPOINT_ADDRESS address = {};

		//endereco do servico
		WS_STRING url = WS_STRING_VALUE(L"http://wvm024.dei.isep.ipp.pt/WalkTheMaze/WebServices/webservice.svc");
		address.url = url;

		hr = WsCreateHeap(4096, 512, NULL, 0, &heap, error);
		WS_HTTP_BINDING_TEMPLATE templ = {};

		//criacao do proxy para o servico
		hr = BasicHttpBinding_IWebService_CreateServiceProxy(&templ, NULL, 0, &proxy, error);
		hr = WsOpenServiceProxy(proxy, &address, NULL, error);

		int result;

		const char* api = api_key.c_str();

		// Convert to a wchar_t*
		size_t origsizea = strlen(api) + 1;
		const size_t newsizea = 100;
		size_t convertedCharsa = 0;
		wchar_t a[newsizea];
		mbstowcs_s(&convertedCharsa, a, origsizea, api, _TRUNCATE);

		const char* mapa = mapaName.c_str();

		// Convert to a wchar_t*
		size_t origsizem = strlen(mapa) + 1;
		const size_t newsizem = 100;
		size_t convertedCharsm = 0;
		wchar_t m[newsizem];
		mbstowcs_s(&convertedCharsm, m, origsizem, mapa, _TRUNCATE);

		hr = BasicHttpBinding_IWebService_SendScore(proxy, a, m, score, &result, heap, NULL, 0, NULL, error);

		if (result == 1)
			return true;

		return false;
	}

	vector<ScoreObject> getPlayerScore(string nickName)
	{
		vector<ScoreObject> scores;
		HRESULT hr = ERROR_SUCCESS;
		WS_ERROR* error = NULL;
		WS_HEAP* heap = NULL;
		WS_SERVICE_PROXY* proxy = NULL;

		WS_ENDPOINT_ADDRESS address = {};

		//endereco do servico
		WS_STRING url = WS_STRING_VALUE(L"http://wvm024.dei.isep.ipp.pt/WalkTheMaze/WebServices/webservice.svc");
		address.url = url;

		hr = WsCreateHeap(4096, 512, NULL, 0, &heap, error);
		WS_HTTP_BINDING_TEMPLATE templ = {};

		//TODO

		return scores;
	}

	vector<string> getPlayersList()
	{
		vector<string> playersList;

		HRESULT hr = ERROR_SUCCESS;
		WS_ERROR* error = NULL;
		WS_HEAP* heap = NULL;
		WS_SERVICE_PROXY* proxy = NULL;

		WS_ENDPOINT_ADDRESS address = {};

		//endereco do servico
		WS_STRING url = WS_STRING_VALUE(L"http://wvm024.dei.isep.ipp.pt/WalkTheMaze/WebServices/webservice.svc");
		address.url = url;

		hr = WsCreateHeap(4096, 512, NULL, 0, &heap, error);
		WS_HTTP_BINDING_TEMPLATE templ = {};

		//TODO

		return playersList;
	}

	vector<ScoreObject> getLeaderBoard()
	{
		vector<ScoreObject> leaderBoard;

		HRESULT hr = ERROR_SUCCESS;
		WS_ERROR* error = NULL;
		WS_HEAP* heap = NULL;
		WS_SERVICE_PROXY* proxy = NULL;

		WS_ENDPOINT_ADDRESS address = {};

		//endereco do servico
		WS_STRING url = WS_STRING_VALUE(L"http://wvm024.dei.isep.ipp.pt/WalkTheMaze/WebServices/webservice.svc");
		address.url = url;

		hr = WsCreateHeap(4096, 512, NULL, 0, &heap, error);
		WS_HTTP_BINDING_TEMPLATE templ = {};

		//TODO

		return leaderBoard;
	}

	vector<ScoreObject> getMapLeaderBoard(string mapName, int nRecords)
	{
		vector<ScoreObject> leaderBoard;

		HRESULT hr = ERROR_SUCCESS;
		WS_ERROR* error = NULL;
		WS_HEAP* heap = NULL;
		WS_SERVICE_PROXY* proxy = NULL;

		WS_ENDPOINT_ADDRESS address = {};

		//endereco do servico
		WS_STRING url = WS_STRING_VALUE(L"http://wvm024.dei.isep.ipp.pt/WalkTheMaze/WebServices/webservice.svc");
		address.url = url;

		hr = WsCreateHeap(4096, 512, NULL, 0, &heap, error);
		WS_HTTP_BINDING_TEMPLATE templ = {};

		//TODO

		return leaderBoard;
	}

	bool SendPath(string api_key, string mapName, string pathName, string pathLocation)
	{
		_WS_BYTES* fileInfo = new _WS_BYTES();

		if (fileExists(pathLocation))
			fileInfo->length = fileToByteArray(pathLocation, &fileInfo->bytes);
		else
			return false;

		HRESULT hr = ERROR_SUCCESS; 
		WS_ERROR* error = NULL;
		WS_HEAP* heap = NULL;
		WS_SERVICE_PROXY* proxy = NULL;

		WS_ENDPOINT_ADDRESS address = {};

		//endereco do servico
		WS_STRING url = WS_STRING_VALUE(L"http://wvm024.dei.isep.ipp.pt/WalkTheMaze/WebServices/webservice.svc");
		address.url = url;

		int heapSize = fileInfo->length + 1000000;

		hr = WsCreateHeap(heapSize, 512, NULL, 0, &heap, error);
		WS_HTTP_BINDING_TEMPLATE templ = {};

		//criacao do proxy para o servico
		hr = BasicHttpBinding_IWebService_CreateServiceProxy(&templ, NULL, 0, &proxy, error);
		hr = WsOpenServiceProxy(proxy, &address, NULL, error);

		const char* api = api_key.c_str();
		const char* map = mapName.c_str();
		const char* pathN = pathName.c_str();

		// Convert to a wchar_t*
		size_t origsizea = strlen(api) + 1;
		const size_t newsizea = 100;
		size_t convertedCharsa = 0;
		wchar_t a[newsizea];
		mbstowcs_s(&convertedCharsa, a, origsizea, api, _TRUNCATE);

		// Convert to a wchar_t*
		size_t origsizem = strlen(map) + 1;
		const size_t newsizem = 100;
		size_t convertedCharsm = 0;
		wchar_t m[newsizem];
		mbstowcs_s(&convertedCharsm, m, origsizem, map, _TRUNCATE);

		// Convert to a wchar_t*
		size_t origsizep = strlen(pathN) + 1;
		const size_t newsizep = 100;
		size_t convertedCharsp = 0;
		wchar_t p[newsizep];
		mbstowcs_s(&convertedCharsp, p, origsizep, pathN, _TRUNCATE);

		int result;

		hr = BasicHttpBinding_IWebService_SendPath(proxy, a, m, p, *fileInfo, &result, heap, NULL, 0, NULL, error);

		if (result == 1)
			return true;

		return false;
	}

	vector<FileListElement> getPathsList(string nickName, string mapName)
	{
		vector<FileListElement> pathsList;

		HRESULT hr = ERROR_SUCCESS;
		WS_ERROR* error = NULL;
		WS_HEAP* heap = NULL;
		WS_SERVICE_PROXY* proxy = NULL;

		WS_ENDPOINT_ADDRESS address = {};

		//endereco do servico
		WS_STRING url = WS_STRING_VALUE(L"http://wvm024.dei.isep.ipp.pt/WalkTheMaze/WebServices/webservice.svc");
		address.url = url;

		hr = WsCreateHeap(4096, 512, NULL, 0, &heap, error);
		WS_HTTP_BINDING_TEMPLATE templ = {};

		//criacao do proxy para o servico
		hr = BasicHttpBinding_IWebService_CreateServiceProxy(&templ, NULL, 0, &proxy, error);
		hr = WsOpenServiceProxy(proxy, &address, NULL, error);

		const char* nick = nickName.c_str();
		const char* map = mapName.c_str();

		// Convert to a wchar_t*
		size_t origsizen = strlen(nick) + 1;
		const size_t newsizen = 100;
		size_t convertedCharsn = 0;
		wchar_t n[newsizen];
		mbstowcs_s(&convertedCharsn, n, origsizen, nick, _TRUNCATE);

		// Convert to a wchar_t*
		size_t origsizem = strlen(map) + 1;
		const size_t newsizem = 100;
		size_t convertedCharsm = 0;
		wchar_t m[newsizem];
		mbstowcs_s(&convertedCharsm, m, origsizem, map, _TRUNCATE);

		FileListElement ** result;

		unsigned int pathCount;

		hr = BasicHttpBinding_IWebService_getPathsList(proxy, n, m, &pathCount, &result, heap, NULL, 0, NULL, error);

		vector<FileListElement> pathList;

		for (unsigned int i = 0; i < pathCount; i++)
			pathsList.push_back(*result[i]);

		return pathsList;
	}

	bool GetPath(string nickName, FileListElement pathElem)
	{
		bool downloadFeito = false;

		HRESULT hr = ERROR_SUCCESS;
		WS_ERROR* error = NULL;
		WS_HEAP* heap = NULL;
		WS_SERVICE_PROXY* proxy = NULL;

		WS_ENDPOINT_ADDRESS address = {};

		//endereco do servico
		WS_STRING url = WS_STRING_VALUE(L"http://wvm024.dei.isep.ipp.pt/WalkTheMaze/WebServices/webservice.svc");
		address.url = url;

		int heapSize = pathElem.fileSize + 1000000;

		hr = WsCreateHeap(heapSize, 512, NULL, 0, &heap, error);
		WS_HTTP_BINDING_TEMPLATE templ = {};

		//TODO

		return downloadFeito;
	}

	vector<FileListElement> getMusicsList(string mapName)
	{
		vector<FileListElement> musicsList;

		HRESULT hr = ERROR_SUCCESS;
		WS_ERROR* error = NULL;
		WS_HEAP* heap = NULL;
		WS_SERVICE_PROXY* proxy = NULL;

		WS_ENDPOINT_ADDRESS address = {};

		//endereco do servico
		WS_STRING url = WS_STRING_VALUE(L"http://wvm024.dei.isep.ipp.pt/WalkTheMaze/WebServices/webservice.svc");
		address.url = url;

		hr = WsCreateHeap(4096, 512, NULL, 0, &heap, error);
		WS_HTTP_BINDING_TEMPLATE templ = {};

		//criacao do proxy para o servico
		hr = BasicHttpBinding_IWebService_CreateServiceProxy(&templ, NULL, 0, &proxy, error);
		hr = WsOpenServiceProxy(proxy, &address, NULL, error);

		const char* map = mapName.c_str();

		// Convert to a wchar_t*
		size_t origsizem = strlen(map) + 1;
		const size_t newsizem = 100;
		size_t convertedCharsm = 0;
		wchar_t m[newsizem];
		mbstowcs_s(&convertedCharsm, m, origsizem, map, _TRUNCATE);

		FileListElement ** result;

		unsigned int musicsCount;

		hr = BasicHttpBinding_IWebService_getMusicsList(proxy, m, &musicsCount, &result, heap, NULL, 0, NULL, error);

		vector<FileListElement> pathList;

		for (unsigned int i = 0; i < musicsCount; i++)
			musicsList.push_back(*result[i]);

		return musicsList;
	}

	bool getMusic(string pastaMapa, FileListElement mapElem, FileListElement songElem)
	{
		bool downloadFeito = false;
		int fileSize = songElem.fileSize;
		string filePath = MAPAS_LOCATION + pastaMapa + "/" + wchar_to_char(songElem.fileName);

		string defaultMusicPath = SOUNDS_LOCATION + string(wchar_to_char(songElem.fileName));
		// VERIFICAR SE JA EXISTE FICHEIRO IGUAL E RETORNA SEM REESCREVER CASO SEJA
		if (isFileSameSize(filePath, songElem.fileSize))
		{
			if (isFileSameSize(defaultMusicPath, fileSize))
				return true;

			// COPIAR FICHEIRO PARA A PASTA RESOURCES/SOUNDS
			std::ifstream  src(filePath, std::ios::binary);
			std::ofstream  dst(defaultMusicPath, std::ios::binary);

			dst << src.rdbuf();

			return true;
		}

		HRESULT hr = ERROR_SUCCESS;
		WS_ERROR* error = NULL;
		WS_HEAP* heap = NULL;
		WS_SERVICE_PROXY* proxy = NULL;

		WS_ENDPOINT_ADDRESS address = {};

		//endereco do servico
		WS_STRING url = WS_STRING_VALUE(L"http://wvm024.dei.isep.ipp.pt/WalkTheMaze/WebServices/webservice.svc");
		address.url = url;

		//////////////////////////////////////////////////////////////////////////
		WS_CHANNEL_PROPERTY channelProperties[3];
		WS_ADDRESSING_VERSION addressingVersion = WS_ADDRESSING_VERSION_TRANSPORT;
		channelProperties[0].id = WS_CHANNEL_PROPERTY_ADDRESSING_VERSION;
		channelProperties[0].value = &addressingVersion;
		channelProperties[0].valueSize = sizeof(addressingVersion);

		WS_ENVELOPE_VERSION envelopeVersion = WS_ENVELOPE_VERSION_SOAP_1_1;
		channelProperties[1].id = WS_CHANNEL_PROPERTY_ENVELOPE_VERSION;
		channelProperties[1].value = &envelopeVersion;
		channelProperties[1].valueSize = sizeof(envelopeVersion);

		ULONG maxMessageSize = 20000000;
		channelProperties[2].id = WS_CHANNEL_PROPERTY_MAX_BUFFERED_MESSAGE_SIZE;
		channelProperties[2].value = &maxMessageSize;
		channelProperties[2].valueSize = sizeof(maxMessageSize);


		hr = WsCreateServiceProxy(
			WS_CHANNEL_TYPE_REQUEST,
			WS_HTTP_CHANNEL_BINDING,
			NULL,
			NULL,
			0,
			channelProperties,
			WsCountOf(channelProperties),
			&proxy,
			error);

		//////////////////////////////////////////////////////////////////////////

		int heapSize = songElem.fileSize + 1000000000;
		hr = WsCreateHeap(heapSize, 8192, NULL, 0, &heap, error);
		WS_HTTP_BINDING_TEMPLATE templ = {};

		//criacao do proxy para o servico
		hr = WsOpenServiceProxy(proxy, &address, NULL, error);

		FileObject* file;
		hr = BasicHttpBinding_IWebService_GetMusic(proxy, mapElem.fileName, songElem.fileName, &file, heap, NULL, 0, NULL, error);

		if (hr == 0x803d0008)
			return false;

		fileSize = file->bytes.length;
		byte* s = (byte *)malloc(fileSize);
		s = file->bytes.bytes;

		string fileName = wchar_to_char(file->filename);

		string fileType = wchar_to_char(file->fileType);

		string dir = MAPAS_LOCATION + pastaMapa + "/";

		FILE * pFile1;
		FILE * pFile2;

		errno_t err1;
		errno_t err2;

		if (!directoryExists(dir))
		{
			_mkdir(dir.c_str());
		}

		err1 = fopen_s(&pFile1, filePath.c_str(), "wb");
		err2 = fopen_s(&pFile2, defaultMusicPath.c_str(), "wb");
		if (err1 == 0 && err2 == 0)
		{
			fwrite(s, sizeof(byte), fileSize, pFile1);
			fwrite(s, sizeof(byte), fileSize, pFile2);
			fclose(pFile1);
			fclose(pFile2);
			downloadFeito = true;
		}
		else{
			downloadFeito = false;
		}

		return downloadFeito;
	}

	vector<FileListElement> getTextsList(string mapName)
	{
		vector<FileListElement> texturesList;

		HRESULT hr = ERROR_SUCCESS;
		WS_ERROR* error = NULL;
		WS_HEAP* heap = NULL;
		WS_SERVICE_PROXY* proxy = NULL;

		WS_ENDPOINT_ADDRESS address = {};

		//endereco do servico
		WS_STRING url = WS_STRING_VALUE(L"http://wvm024.dei.isep.ipp.pt/WalkTheMaze/WebServices/webservice.svc");
		address.url = url;

		hr = WsCreateHeap(4096, 512, NULL, 0, &heap, error);
		WS_HTTP_BINDING_TEMPLATE templ = {};

		//criacao do proxy para o servico
		hr = BasicHttpBinding_IWebService_CreateServiceProxy(&templ, NULL, 0, &proxy, error);
		hr = WsOpenServiceProxy(proxy, &address, NULL, error);

		const char* map = mapName.c_str();

		// Convert to a wchar_t*
		size_t origsizem = strlen(map) + 1;
		const size_t newsizem = 100;
		size_t convertedCharsm = 0;
		wchar_t m[newsizem];
		mbstowcs_s(&convertedCharsm, m, origsizem, map, _TRUNCATE);

		FileListElement ** result;

		unsigned int texturesCount;

		hr = BasicHttpBinding_IWebService_getTexturesList(proxy, m, &texturesCount, &result, heap, NULL, 0, NULL, error);

		vector<FileListElement> pathList;

		for (unsigned int i = 0; i < texturesCount; i++)
			texturesList.push_back(*result[i]);

		return texturesList;
	}

	bool getTexture(string pastaMapa, FileListElement mapElem, FileListElement textureElem)
	{
		bool downloadFeito = false;

		int fileSize = textureElem.fileSize;
		string filePath = MAPAS_LOCATION + pastaMapa + "/" + string(wchar_to_char(textureElem.fileName));

		string defaultTexturePath = TEXTURES_LOCATION + string(wchar_to_char(textureElem.fileName));
		// VERIFICAR SE JA EXISTE FICHEIRO IGUAL E RETORNA SEM REESCREVER CASO SEJA
		if (isFileSameSize(filePath, fileSize))
		{
			if (isFileSameSize(defaultTexturePath, fileSize))
				return true;

			// COPIAR FICHEIRO PARA A PASTA RESOURCES/IMAGES
			std::ifstream  src(filePath, std::ios::binary);
			std::ofstream  dst(defaultTexturePath, std::ios::binary);

			dst << src.rdbuf();

			return true;
		}
			

		HRESULT hr = ERROR_SUCCESS;
		WS_ERROR* error = NULL;
		WS_HEAP* heap = NULL;
		WS_SERVICE_PROXY* proxy = NULL;

		WS_ENDPOINT_ADDRESS address = {};

		//endereco do servico
		WS_STRING url = WS_STRING_VALUE(L"http://wvm024.dei.isep.ipp.pt/WalkTheMaze/WebServices/webservice.svc");
		address.url = url;

		//////////////////////////////////////////////////////////////////////////
		WS_CHANNEL_PROPERTY channelProperties[3];
		WS_ADDRESSING_VERSION addressingVersion = WS_ADDRESSING_VERSION_TRANSPORT;
		channelProperties[0].id = WS_CHANNEL_PROPERTY_ADDRESSING_VERSION;
		channelProperties[0].value = &addressingVersion;
		channelProperties[0].valueSize = sizeof(addressingVersion);

		WS_ENVELOPE_VERSION envelopeVersion = WS_ENVELOPE_VERSION_SOAP_1_1;
		channelProperties[1].id = WS_CHANNEL_PROPERTY_ENVELOPE_VERSION;
		channelProperties[1].value = &envelopeVersion;
		channelProperties[1].valueSize = sizeof(envelopeVersion);

		ULONG maxMessageSize = 20000000;
		channelProperties[2].id = WS_CHANNEL_PROPERTY_MAX_BUFFERED_MESSAGE_SIZE;
		channelProperties[2].value = &maxMessageSize;
		channelProperties[2].valueSize = sizeof(maxMessageSize);


		hr = WsCreateServiceProxy(
			WS_CHANNEL_TYPE_REQUEST,
			WS_HTTP_CHANNEL_BINDING,
			NULL,
			NULL,
			0,
			channelProperties,
			WsCountOf(channelProperties),
			&proxy,
			error);

		//////////////////////////////////////////////////////////////////////////

		int heapSize = fileSize + 1000000000;
		hr = WsCreateHeap(heapSize, 2048, NULL, 0, &heap, error);
		WS_HTTP_BINDING_TEMPLATE templ = {};

		//criacao do proxy para o servico
		//hr = BasicHttpBinding_IWebService_CreateServiceProxy(&templ, NULL, 0, &proxy, error);
		hr = WsOpenServiceProxy(proxy, &address, NULL, error);

		FileObject* file;
		hr = BasicHttpBinding_IWebService_GetTexture(proxy, mapElem.fileName, textureElem.fileName, &file, heap, NULL, 0, NULL, error);

		fileSize = file->bytes.length;
		byte* s = (byte *)malloc(fileSize);
		s = file->bytes.bytes;

		string fileName = wchar_to_char(file->filename);

		string fileType = wchar_to_char(file->fileType);

		string dir;
		dir = MAPAS_LOCATION + pastaMapa + "/";

		FILE * pFile1;
		FILE * pFile2;

		errno_t err1;
		errno_t err2;

		if (!directoryExists(dir))
		{
			_mkdir(dir.c_str());
		}

		err1 = fopen_s(&pFile1, filePath.c_str(), "wb");
		err2 = fopen_s(&pFile2, defaultTexturePath.c_str(), "wb");
		if (err1 == 0 && err2 == 0)
		{
			fwrite(s, sizeof(byte), fileSize, pFile1);
			fwrite(s, sizeof(byte), fileSize, pFile2);
			fclose(pFile1);
			fclose(pFile2);
			downloadFeito = true;
		}
		else{
			downloadFeito = false;
		}

		return downloadFeito;
	}

	static vector<FileListElement> getModelsList(string mapName)
	{
		vector<FileListElement> modelsList;

		HRESULT hr = ERROR_SUCCESS;
		WS_ERROR* error = NULL;
		WS_HEAP* heap = NULL;
		WS_SERVICE_PROXY* proxy = NULL;

		WS_ENDPOINT_ADDRESS address = {};

		//endereco do servico
		WS_STRING url = WS_STRING_VALUE(L"http://wvm024.dei.isep.ipp.pt/WalkTheMaze/WebServices/webservice.svc");
		address.url = url;

		hr = WsCreateHeap(4096, 512, NULL, 0, &heap, error);
		WS_HTTP_BINDING_TEMPLATE templ = {};

		//criacao do proxy para o servico
		hr = BasicHttpBinding_IWebService_CreateServiceProxy(&templ, NULL, 0, &proxy, error);
		hr = WsOpenServiceProxy(proxy, &address, NULL, error);

		const char* map = mapName.c_str();

		// Convert to a wchar_t*
		size_t origsizem = strlen(map) + 1;
		const size_t newsizem = 100;
		size_t convertedCharsm = 0;
		wchar_t m[newsizem];
		mbstowcs_s(&convertedCharsm, m, origsizem, map, _TRUNCATE);

		FileListElement ** result;

		unsigned int texturesCount;

		hr = BasicHttpBinding_IWebService_getModelsList(proxy, m, &texturesCount, &result, heap, NULL, 0, NULL, error);

		vector<FileListElement> pathList;

		for (unsigned int i = 0; i < texturesCount; i++)
			modelsList.push_back(*result[i]);

		return modelsList;
	}

	static bool getModel(string pastaMapa, FileListElement mapElem, FileListElement modelElem)
	{
		bool downloadFeito = false;

		int fileSize = modelElem.fileSize;
		string filePath = MAPAS_LOCATION + pastaMapa + "/" + wchar_to_char(modelElem.fileName);

		string defaultModelPath = MODELOS_LOCATION + string(wchar_to_char(modelElem.fileName));
		// VERIFICAR SE JA EXISTE FICHEIRO IGUAL E RETORNA SEM REESCREVER CASO SEJA
		if (isFileSameSize(filePath, modelElem.fileSize))
		{
			if (isFileSameSize(defaultModelPath,fileSize))
			return true;

			// COPIAR FICHEIRO PARA A PASTA RESOURCES/IMAGES
			std::ifstream  src(filePath, std::ios::binary);
			std::ofstream  dst(defaultModelPath, std::ios::binary);

			dst << src.rdbuf();

			return true;
		}

		HRESULT hr = ERROR_SUCCESS;
		WS_ERROR* error = NULL;
		WS_HEAP* heap = NULL;
		WS_SERVICE_PROXY* proxy = NULL;

		WS_ENDPOINT_ADDRESS address = {};

		//endereco do servico
		WS_STRING url = WS_STRING_VALUE(L"http://wvm024.dei.isep.ipp.pt/WalkTheMaze/WebServices/webservice.svc");
		address.url = url;

		//////////////////////////////////////////////////////////////////////////
		WS_CHANNEL_PROPERTY channelProperties[3];
		WS_ADDRESSING_VERSION addressingVersion = WS_ADDRESSING_VERSION_TRANSPORT;
		channelProperties[0].id = WS_CHANNEL_PROPERTY_ADDRESSING_VERSION;
		channelProperties[0].value = &addressingVersion;
		channelProperties[0].valueSize = sizeof(addressingVersion);

		WS_ENVELOPE_VERSION envelopeVersion = WS_ENVELOPE_VERSION_SOAP_1_1;
		channelProperties[1].id = WS_CHANNEL_PROPERTY_ENVELOPE_VERSION;
		channelProperties[1].value = &envelopeVersion;
		channelProperties[1].valueSize = sizeof(envelopeVersion);

		ULONG maxMessageSize = 20000000;
		channelProperties[2].id = WS_CHANNEL_PROPERTY_MAX_BUFFERED_MESSAGE_SIZE;
		channelProperties[2].value = &maxMessageSize;
		channelProperties[2].valueSize = sizeof(maxMessageSize);


		hr = WsCreateServiceProxy(
			WS_CHANNEL_TYPE_REQUEST,
			WS_HTTP_CHANNEL_BINDING,
			NULL,
			NULL,
			0,
			channelProperties,
			WsCountOf(channelProperties),
			&proxy,
			error);

		//////////////////////////////////////////////////////////////////////////
		int heapSize = modelElem.fileSize + 1000000000;
		hr = WsCreateHeap(heapSize, 8192, NULL, 0, &heap, error);
		WS_HTTP_BINDING_TEMPLATE templ = {};

		//criacao do proxy para o servico
		hr = WsOpenServiceProxy(proxy, &address, NULL, error);

		FileObject* file;
		hr = BasicHttpBinding_IWebService_getModel(proxy, mapElem.fileName, modelElem.fileName, &file, heap, NULL, 0, NULL, error);

		fileSize = file->bytes.length;
		byte* s = (byte *)malloc(fileSize);
		s = file->bytes.bytes;

		string fileName = wchar_to_char(file->filename);

		string fileType = wchar_to_char(file->fileType);

		string dir = MAPAS_LOCATION + pastaMapa + "/";

		FILE * pFile1;
		FILE * pFile2;

		errno_t err1;
		errno_t err2;

		string defaultMusicPath = MODELOS_LOCATION + fileName + "." + fileType;

		if (!directoryExists(dir))
		{
			_mkdir(dir.c_str());
		}

		err1 = fopen_s(&pFile1, filePath.c_str(), "wb");
		err2 = fopen_s(&pFile2, defaultMusicPath.c_str(), "wb");
		if (err1 == 0 && err2 == 0)
		{
			fwrite(s, sizeof(byte), fileSize, pFile1);
			fwrite(s, sizeof(byte), fileSize, pFile2);
			fclose(pFile1);
			fclose(pFile2);
			downloadFeito = true;
		}
		else{
			downloadFeito = false;
		}

		return downloadFeito;
	}


}