#include <string>

#include <iostream>
#include <string>
#include <sstream>
#include <vector>
//#include <atlstr.h> // http://stackoverflow.com/questions/20454793/converting-string-to-tchar-in-vc

// Necessario para os WebServices
#include "stdafx.h"
#include "WebServices.h"
#include "WebServices\schemas.microsoft.com.2003.10.Serialization.xsd.h"
#include "WebServices\schemas.microsoft.com.2003.10.Serialization.Arrays.xsd.h"
#include "WebServices\tempuri.org.xsd.h"
#include "WebServices\tempuri.org.wsdl.h"
#include "WebServices\Walk_The_Maze.WebServices.xsd.h"

#include "services.h"

using namespace std;

class WebClient
{
public:
	WebClient();
	~WebClient();

	string getNickName();

	void teste(string username, string password);

	int loginService(string username, string password);

	vector<FileListElement> getMapList();
	string getMap(FileListElement mapEscolhido);
	string getMapComplete(FileListElement mapEscolhido);
	bool sendScore(string mapName, float score);
	vector<string> getPlayersList();
	vector<ScoreObject> getPlayerScore();
	vector<ScoreObject> getPlayerScore(string nickname);
	vector<ScoreObject> getLeaderBoard();
	vector<ScoreObject> getMapLeaderBoard(string mapName, int nRecords);
	bool SendPath(string mapName, string pathName, string pathLocation);
	vector<FileListElement> getPathsList(string mapName);
	vector<FileListElement> getPathsList(string nickName, string mapName);
	bool GetPath(FileListElement pathName);
	bool GetPath(string nickName, FileListElement pathName);
	vector<FileListElement> getMusicsList(string mapName);
	bool getMusic(string mapaName, FileListElement mapElem, FileListElement songElem);
	vector<FileListElement> getTextsList(string mapName);
	bool getTexture(string mapaName, FileListElement mapElem, FileListElement textureElem);
	vector<FileListElement> getModelsList(string mapName);
	bool getModel(string mapaName, FileListElement mapElem, FileListElement textureElem);

private:
	string api_key;
	string nickname;
};

WebClient::WebClient()
{
}

WebClient::~WebClient()
{
}

string WebClient::getNickName()
{
	return nickname;
}

int WebClient::loginService(string username, string password)
{
	string api = WebService::getApi(username, password);
	if (api != "")
	{
		this->nickname = username;
		this->api_key = api;
		return 1;
	}
	return -1;
}

vector<FileListElement> WebClient::getMapList()
{
	vector<FileListElement> maps = WebService::getMapList(this->api_key);

	return maps;
}

string WebClient::getMap(FileListElement mapEscolhido)
{
	return WebService::getMap(api_key, mapEscolhido);
}

string WebClient::getMapComplete(FileListElement mapEscolhido)
{
	// FICHEIRO MAPA
	string mapDone = WebService::getMap(api_key, mapEscolhido);

	// TEXTURAS
	bool textsDone = true;
	vector<FileListElement> texsList = WebService::getTextsList(WebService::wchar_to_char(mapEscolhido.fileName));
	for each (auto tex in texsList)
		if (getTexture(mapDone, mapEscolhido, tex) == false)
			textsDone = false;

	// MUSICAS
	bool musicsDone = true;
	vector<FileListElement> musicsList = WebService::getMusicsList(WebService::wchar_to_char(mapEscolhido.fileName));
	for each (auto music in musicsList)
		if (!getMusic(mapDone, mapEscolhido, music))
			musicsDone = false;

	// MODELOS
	bool modelsDone = true;
	vector<FileListElement> modelsList = WebService::getModelsList(WebService::wchar_to_char(mapEscolhido.fileName));
	for each (auto model in modelsList)
		if (!getModel(mapDone, mapEscolhido, model))
			modelsDone = false;

	if (!mapDone.empty() && textsDone && musicsDone && modelsDone)
		return mapDone;
	return "";
}

bool WebClient::sendScore(string mapName, float score)
{
	return WebService::sendScore(api_key, mapName, score);
}

vector<ScoreObject> WebClient::getPlayerScore()
{
	return WebService::getPlayerScore(this->nickname);
}

vector<ScoreObject> WebClient::getPlayerScore(string nickname)
{
	return WebService::getPlayerScore(nickname);
}

vector<string> WebClient::getPlayersList()
{
	return WebService::getPlayersList();
}

vector<ScoreObject> WebClient::getLeaderBoard()
{
	return WebService::getLeaderBoard();
}

vector<ScoreObject> WebClient::getMapLeaderBoard(string mapName, int nRecords)
{
	return WebService::getMapLeaderBoard(mapName, nRecords);
}

bool WebClient::SendPath(string mapName, string pathName, string pathLocation)
{
	return WebService::SendPath(this->api_key, mapName, pathName, pathLocation);
}

vector<FileListElement> WebClient::getPathsList(string mapName)
{
	return WebService::getPathsList(this->nickname, mapName);
}

vector<FileListElement> WebClient::getPathsList(string nickName, string mapName)
{
	return WebService::getPathsList(nickname, mapName);
}

bool WebClient::GetPath(FileListElement pathEscolhido)
{
	return WebService::GetPath(this->nickname, pathEscolhido);
}

bool WebClient::GetPath(string nickName, FileListElement pathEscolhido)
{
	return WebService::GetPath(nickName, pathEscolhido);
}

vector<FileListElement> WebClient::getMusicsList(string mapName)
{
	return WebService::getMusicsList(mapName);
}

bool WebClient::getMusic(string mapaName, FileListElement mapElem, FileListElement songElem)
{
	return WebService::getMusic(mapaName, mapElem, songElem);
}

vector<FileListElement> WebClient::getTextsList(string mapName)
{
	return WebService::getTextsList(mapName);
}

bool WebClient::getTexture(string mapaName, FileListElement mapElem, FileListElement textureElem)
{
	return WebService::getTexture(mapaName, mapElem, textureElem);
}

vector<FileListElement> WebClient::getModelsList(string mapName)
{
	return WebService::getModelsList(mapName);
}

bool WebClient::getModel(string mapaName, FileListElement mapElem, FileListElement textureElem)
{
	return WebService::getModel(mapaName, mapElem, textureElem);
}

void WebClient::teste(string user, string pass)
{
	// LOGIN OK!
	bool loginErrado = true;
	while (loginErrado)
	{
		if (loginService(user, pass) == -1)
		{
			cout << "****" << endl;
			cout << "Erro a fazer login. Causas possiveis:" << endl;
			cout << "1. Nao estar ligado a Deinet." << endl;
			cout << "2. Username ou Password errada ou nao esta registado" << endl;
			cout << "****" << endl;

			system("PAUSE");
			system("cls");
		}
		else{
			cout << "Bem vindo " << getNickName() << endl;
			loginErrado = false;
		}
	}

	// DOWNLOAD DO MAPA OK!

	cout << "\n---MAPAS---" << endl;

	vector<FileListElement> maps = getMapList();

	cout << "Nome do mapa | Tamanho do mapa (bytes)" << endl;
	int i = 1;
	for each (auto var in maps)
	{
		cout << i << " - " << WebService::wchar_to_char(var.fileName) << " - " << var.fileSize << endl;
		i++;
	}
	cout << "Insira o indice do mapa" << endl;
	int index;
	cin >> index;

	// DOWNLOAD DO MAPA COMPLETO COM AS TEXTURAS E MUSICAS
	string nomeMapa = getMapComplete(maps.at(index - 1));
	if (!nomeMapa.empty()){
		cout << "Feito o download do mapa " << WebService::wchar_to_char(maps.at(index - 1).fileName) << endl;
		cout << "Nome do ficheiro: \"" << nomeMapa << ".mtdts\"" << endl;
		cout << "Foi criada uma pasta \"" << nomeMapa << "\" com todas as texturas e musicas do mapa" << endl;
	}
	else
		cout << "Erro a carregar o mapa" << endl;

	// DOWNLOAD APENAS DO FICHEIRO DO MAPA OK!
	/*FileListElement mapaEscolhido = maps.at(index - 1);
	string nomeFicheiroMapa = getMap(mapaEscolhido);
	if (!nomeFicheiroMapa.empty())
	cout << "Feito o download do mapa " << WebService::wchar_to_char(mapaEscolhido.fileName) << endl;
	else
	cout << "Fail" << endl;*/

	//// DONWLOAD DAS TEXTURAS OK!
	//cout << "Texturas que o mapa inclui:" << endl;
	//vector<FileListElement> texs = getTextsList(WebService::wchar_to_char(mapaEscolhido.fileName));
	//cout << "Nome da textura | Tamanho do ficheiro (bytes)" << endl;
	//i = 1;
	//for each (auto var in texs)
	//{
	//	cout << i << " - " << WebService::wchar_to_char(var.fileName) << " - " << var.fileSize << endl;
	//	i++;
	//}
	//cout << "Insira o indice da textura" << endl;
	//cin >> index;
	//FileListElement textEscolhida = texs.at(index - 1);

	//if (getTexture(nomeFicheiroMapa, mapaEscolhido, textEscolhida))
	//	cout << "Feito o download da musica " << WebService::wchar_to_char(textEscolhida.fileName) << endl;
	//else
	//	cout << "Fail" << endl;

	//cout << endl;

	// DOWNLOAD DAS MUSICAS
	/*cout << "Musicas que o mapa inclui:" << endl;
	vector<FileListElement> musics = getMusicsList(WebService::wchar_to_char(mapaEscolhido.fileName));
	cout << "Nome da musica | Tamanho do ficheiro (bytes)" << endl;
	i = 1;
	for each (auto var in musics)
	{
	cout << i << " - " << WebService::wchar_to_char(var.fileName) << " - " << var.fileSize << endl;
	i++;
	}
	cout << "Insira o indice da musica" << endl;
	cin >> index;
	FileListElement musicaEscolhida = musics.at(index - 1);

	if (getMusic(nomeFicheiroMapa, mapaEscolhido, musicaEscolhida))
	cout << "Feito o download da musica " << WebService::wchar_to_char(musicaEscolhida.fileName) << endl;
	else
	cout << "Fail" << endl;*/

	// ENVIO PONTUACAO OK!
	/*cout << "\n---PONTUACAO---" << endl;
	cout << "Insira a pontuacao: ";
	float score;
	cin >> score;
	if (this->sendScore(mapaEscolhido, score))
		cout << "Pontuacao enviada com sucesso" << endl;
	else
		cout << "Erro!" << endl;*/

	// UPLOAD CAMINHO OK!
	/*cout << "\n---PERCURSO---" << endl;
	string pathName = "teste";
	string path = CAMINHOS_LOCATION + pathName;
	cout << "A enviar o ficheiro " << path << endl;
	system("PAUSE");
	if (this->SendPath(WebService::wchar_to_char(mapaEscolhido.fileName), pathName, path))
		cout << "Caminho enviado com sucesso" << endl;
	else
		cout << "Erro!" << endl;*/

	// DOWNLOAD TEXTURA

	// DOWNLOAD MUSICA

	system("PAUSE");
}



