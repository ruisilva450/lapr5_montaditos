#ifndef Trig_
#define Trig_

#define USE_MATH_DEFINES

#include <math.h>
#include <vector>

using namespace std;

typedef struct {
	double x, y;
} Ponto;

Ponto calcPontoMedio(Ponto p1, Ponto p2) {
	Ponto pMed;

	pMed.x = (p1.x + p2.x) / 2;
	pMed.y = (p1.y + p2.y) / 2;

	return pMed;
}

float calcDist(Ponto p1, Ponto p2) {
	return (float)sqrt(pow(p1.x - p2.x, 2) + pow(p1.y - p2.y, 2));
}

vector<Ponto> divideCircPartesIguais(Ponto pCentro, float raio, int nDiv) {
	vector<Ponto> pontos;
	Ponto ponto;
	double angulo = 0;
	double anguloSomar = 2 * M_PI / nDiv;

	for (int i = 0; i<nDiv; i++) {
		ponto.x = raio*cos(angulo) + pCentro.x;
		ponto.y = raio*sin(angulo) + pCentro.y;
		pontos.push_back(ponto);
		//cout << "P" << i << ": \t ang=" << angulo*180/M_PI << "�\tx=" << ponto.x << "�\ty=" << ponto.y << endl;
		angulo += anguloSomar;
	}
	return pontos;
}

#endif