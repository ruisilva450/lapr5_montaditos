#pragma once

typedef struct teclas_t{
	GLboolean   up, down, left, right, som, luz, lanterna, disparo, colisoes, replay;
}teclas_t;

typedef struct pos_t{
	GLfloat    x, y, z;
}pos_t;

typedef struct objecto_t{
	pos_t    pos, nextPos;
	GLfloat  dir;
	GLfloat  vel;
	int vida;
}objecto_t;

typedef struct camera_t{
	pos_t    eye;
	GLfloat  dir_long;  // longitude olhar (esq-dir)
	GLfloat  dir_lat;   // latitude olhar	(cima-baixo)
	GLfloat  fov;
}camera_t;

typedef struct ESTADO{
	GLboolean	  modoReplay;
	char*		  ficheiroMapa;
	camera_t      camera;
	GLint         timer;
	GLint         mainWindow, topSubwindow, navigateSubwindow;
	teclas_t      teclas;
	GLboolean     localViewer;
	GLuint        vista[NUM_VISTAS];
	bool		  aEntrarQuatroSecreto;
}ESTADO;

typedef struct MODELO{
	GLuint			texID[NUM_VISTAS][NUM_TEXTURAS];
	GLuint			labirinto[NUM_VISTAS];
	GLuint			chao[NUM_VISTAS];
	objecto_t	    objecto;
	GLint			xMouse;
	GLint			yMouse;
	StudioModel		homer[NUM_VISTAS], inimigo[NUM_VISTAS];   // Modelo Homer e do inimigo!
	GLboolean		andar, aColidir, aCair;
	GLuint			prev;
	char*			quartoAtual;
}MODELO;

typedef struct Colisao{
	int nome, indice;
	float z1, z2;
}Colisao;
