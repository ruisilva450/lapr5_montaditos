﻿#include "modelo/Quarto.h"

void desenhaLuzesTeto(int idLuz, pos_t posicao)
{
	GLfloat light_position[] = { posicao.x*0.30, 2, posicao.y*0.30, 1.0 };
	GLfloat spot_direction[] = { posicao.x*0.30, -1.0, posicao.y*0.30 };
	GLfloat light_ambient[] = { 5, 5, 5, 1 };

	glLightfv(idLuz, GL_AMBIENT, light_ambient);
	glLightfv(idLuz, GL_POSITION, light_position);
	glLightf(idLuz, GL_CONSTANT_ATTENUATION, 1.5);
	glLightf(idLuz, GL_LINEAR_ATTENUATION, 0.1);
	glLightf(idLuz, GL_QUADRATIC_ATTENUATION, 0.1);
	glLightf(idLuz, GL_SPOT_CUTOFF, 9);
	glLightfv(idLuz, GL_SPOT_DIRECTION, spot_direction);
	glLightf(idLuz, GL_SPOT_EXPONENT, 90);

	glEnable(idLuz);
}

void desenhaLanterna(objecto_t homer, ESTADO &estado)
{
	float velocidade = 3;
	pos_t dir;
	dir.x = homer.pos.x + cos(homer.dir)*velocidade;
	dir.z = homer.pos.z + sin(-homer.dir)*velocidade;

	GLfloat light_ambient[] = { 5, 5, 5, 1 };
	GLfloat light_position[] = { homer.pos.x*0.30, 0.5*0.3, homer.pos.z*0.30, 1.0 };
	GLfloat spot_direction[] = { (dir.x*0.30), 0.5*0.3, (dir.z*0.30) };

	glLightfv(GL_LIGHT1, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT1, GL_POSITION, light_position);
	glLightf(GL_LIGHT1, GL_CONSTANT_ATTENUATION, 1.5);
	glLightf(GL_LIGHT1, GL_LINEAR_ATTENUATION, 0.1);
	glLightf(GL_LIGHT1, GL_QUADRATIC_ATTENUATION, 0.1);
	glLightf(GL_LIGHT1, GL_SPOT_CUTOFF, 10);
	glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, spot_direction);
	glLightf(GL_LIGHT1, GL_SPOT_EXPONENT, 75);

	if (estado.teclas.lanterna == AL_FALSE)
	{
		glDisable(GL_LIGHT1);
	}
	else
	{
		glEnable(GL_LIGHT1);
	}
}

void setLight(ESTADO &estado, Quarto q, objecto_t homer)
{
	GLfloat light_ambient[4];

	if (estado.teclas.luz == AL_FALSE)
	{
		light_ambient[0] = { 0.2f };
		light_ambient[1] = { 0.2f };
		light_ambient[2] = { 0.2f };
		light_ambient[3] = { 1.0f };
	}
	else
	{
		light_ambient[0] = { 0.8f };
		light_ambient[1] = { 0.8f };
		light_ambient[2] = { 0.8f };
		light_ambient[3] = { 1.0f };
	}
	
	GLfloat light_pos[4] = { 0, 20, 0, 1 };
	GLfloat light_diffuse[] = { 0.8f, 0.8f, 0.8f, 1.0f };
	GLfloat light_specular[] = { 0.5f, 0.5f, 0.5f, 1.0f };

	// ligar iluminaçăo
	glEnable(GL_LIGHTING);

	// ligar e definir fonte de luz 0
	glEnable(GL_LIGHT0);
	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_pos);
	
	vector<int> GL_lights = { GL_LIGHT2, GL_LIGHT3, GL_LIGHT4, GL_LIGHT5, GL_LIGHT6, GL_LIGHT7 };
	vector<pos_t> luzes = q.getPosLuzes();

	desenhaLanterna(homer, estado);

	for (unsigned i = 0; i < GL_lights.size(); i++){
		if (i <= luzes.size())
		{
			desenhaLuzesTeto(GL_lights[i], luzes[i]);
		}
		return;
	}
}

