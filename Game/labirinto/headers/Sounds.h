#pragma once

class Sound {
	ALuint		  buffer, source;
public:
	void InitAudio(string sfilePath);
	void switchAudio(GLboolean estado);
};

void Sound::InitAudio(string sfilePath){
	char * filePath = new char[sfilePath.length() + 1];
	std::strcpy(filePath, sfilePath.c_str());
	if (!fileExists(filePath))
		{
			printf("\nError: Audio Resource is missing!\n");
			system("PAUSE");
			cout << endl;
		}
		buffer = alutCreateBufferFromFile(filePath);
		alGenSources(1, &source);
		alSourcei(source, AL_BUFFER, buffer);
		switchAudio(AL_FALSE);
}

void Sound::switchAudio(GLboolean estado){
	ALint state;
	alGetSourcei(source, AL_SOURCE_STATE, &state);
	if (estado)
	{
		if (state != AL_PLAYING)
			alSourcePlay(source);
	}
	else
		if (state == AL_PLAYING)
			alSourceStop(source);
}