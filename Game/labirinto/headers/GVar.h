#ifndef M_PI
#define M_PI							3.1415926535897932384626433832795
#endif

#define RAD(x)							(M_PI*(x)/180)
#define GRAUS(x)						(180*(x)/M_PI)

#define	GAP								25

#define MAZE_HEIGHT						18
#define MAZE_WIDTH						18

#define	OBJECTO_ALTURA					0.4
#define OBJECTO_VELOCIDADE				3
#define OBJECTO_VELOCIDADE_QUEDA		OBJECTO_VELOCIDADE*0.03
#define OBJECTO_ROTACAO					5
#define OBJECTO_RAIO					0.12
#define SCALE_HOMER						0.005
#define EYE_ROTACAO						1
#define VELOCIDADE_PROJECTIL			0.35
#define VELOCIDADE_EXPLOSAO				0.05
#define VELOCIDADE_INIMIGO				0.1

#define SCALE_ZOMBIE					0.0075

#define RESOURCES_LOCATION				"Resources/"
#define MAPA_FILE						"Resources/mapa.mtdts"

// MODELOS
#define MODELOS_LOCATION				"Resources/Models/"
#define MODELO_PERSONAGEM				"Resources/Models/homer.mdl" //local
#define MODELO_INIMIGO					"Resources/Models/ini1.mdl" // vem do download

// TEXTURAS
#define TEXTURES_LOCATION				"Resources/Images/"
#ifdef _WIN32
#define NOME_TEXTURA_CUBOS				"Resources/Images/parede.bmp" //vem do download
#else
#define NOME_TEXTURA_CUBOS				"Resources/Images/parede.jpg" //vem do download
#endif
#ifdef _WIN32
#define NOME_TEXTURA_MENU_BACKGROUND	"Resources/Images/menubg.bmp" //local
#else
#define NOME_TEXTURA_MENU_BACKGROUND	"Resources/Images/menubg.jpg" //local
#endif
#define NOME_TEXTURA_CHAO				"Resources/Images/chao.jpg" //vem do download
#define NOME_TEXTURA_CEU_DIA			"Resources/Images/ceu_dia.jpg" //vem do download
#define NOME_TEXTURA_CEU_NOITE			"Resources/Images/ceu_noite.jpg" //vem do download

// MUSICAS
#define SOUNDS_LOCATION					"Resources/Sounds/"
#define MUSIC_BACKGROUND_FILE			"Resources/Sounds/bgmusic1.wav" //local
#define MUSIC_BACKGROUND_FILE_CUSTOM	"Resources/Sounds/custombgmusic1.wav" //local
#define MUSIC_DOH_FILE					"Resources/Sounds/Doh.wav" //vem do download
#define MUSIC_FIRE_FILE					"Resources/Sounds/tiro.wav" //vem do download
#define MUSIC_EXPLOSION_FILE			"Resources/Sounds/explosao.wav"

#define NUM_TEXTURAS					6
#define ID_TEXTURA_MENU_BACKGROUND		0
#define ID_TEXTURA_CUBOS				1
#define ID_TEXTURA_CHAO					2
#define ID_TEXTURA_PORTA				3
#define ID_TEXTURA_CEU_NOITE			4
#define ID_TEXTURA_CEU_DIA				5

#define NUM_VISTAS						2
#define VISTA_TOP						0
#define VISTA_NAVIGATE					1

//nomes para as colisoes

#define NOME_PAREDES					999
#define NOME_ALCAPOES					1000
#define NOME_OBSTACULOS					1001
#define NOME_PORTA_SECRETA				1002
#define NOME_INIMIGO_MIN				2000
#define NOME_INIMIGO_MAX				2100
#define NOME_PORTAS						3000


#define MAPAS_LOCATION					"Mapas/"
#define CAMINHOS_LOCATION				"Caminhos/"

/*variaveis e estruturas para as particulas de chuva*/
#define MAX_PARTICLES					50000
#define RAIN							0
#define SNOW							1
#define	HAIL							2
#define STEP							1