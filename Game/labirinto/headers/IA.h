void CheckCloseNPC(ESTADO &estado, pos_t homerPos, Quarto &quarto)
{

	for (int i = 0; i < quarto.getNumInimigos();i++)
	{
		objecto_t inimigo = quarto.getInimigo(i);

		if (homerPos.x > inimigo.pos.x - 2 && homerPos.x < inimigo.pos.x + 2 && homerPos.z > -inimigo.pos.y - 2 && homerPos.z < -inimigo.pos.y + 2)
		{
			// TODO achar angulo/direcao correto ao USAR TRIGONOMETRIA TRIA TRIA TRIA....

			pos_t hPos = homerPos;		// POSICAO DO HOMER
			
			pos_t iPos = inimigo.pos;	// POSICAO DO INIMIGO

			float rotacao = atan2((-hPos.z) - iPos.y, hPos.x - iPos.x) * 180 / M_PI;
			
			float rotacaoGraus = RAD(rotacao);

			inimigo.dir = rotacaoGraus;
			
			inimigo.nextPos.x = inimigo.pos.x + cos(inimigo.dir)*VELOCIDADE_INIMIGO;
			inimigo.nextPos.y = inimigo.pos.y - sin(-inimigo.dir)*VELOCIDADE_INIMIGO;

			quarto.updateInimigo(inimigo, i);
		}
	}
	
}