#pragma once
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sys/types.h>
#include <sys/stat.h>
#include <direct.h>


using namespace std;

#include "modelo/Mapa.h"

#define ALTURA_PAREDE	2
#define ALTURA_LUZ		3

//definicao da legenda do ficheiro de mapa
#define COORD_INICIO_PERSONAGEM "coord_inicio_personagem"
#define ID_QUARTO_INICIAL		"id_quarto_inicial"
#define DESTINO					"destino"
#define TEX_PATH				"tex_path"
#define ID_QUARTO				"id_quarto"
#define PAREDES					"paredes"
#define PORTA					"porta"
#define MURO					"muro"
#define MUROSECRETO				"murosecreto"
#define LUZ						"luz"
#define FONTE					"fonte"
#define ALCAPAO					"alcapao"
#define INIMIGO					"inimigo"
#define OBSTACULO				"obstaculo"
#define SEPARADOR				";"

char* stringToChar(string str)
{
	char *cstr = new char[str.length() + 1];
	strcpy(cstr, str.c_str());
	return cstr;
}

bool directoryExists(string pathname)
{
	struct stat info;
	if (stat(pathname.c_str(), &info) != 0)
		return false;
	else if (info.st_mode & S_IFDIR)  // S_ISDIR() doesn't exist on my windows
		return true;
	else
		return false;
}

//return o tamanho, o array de bytes fica no argumento que recebe
int fileToByteArray(string fileName, byte **fileBytes){
	FILE *f;
	fopen_s(&f, fileName.c_str(), "r");
	fseek(f, 0, SEEK_END);
	long fsize = ftell(f);
	fseek(f, 0, SEEK_SET);

	char *string = (char *)malloc(fsize + 1);
	fread(string, fsize, 1, f);
	fclose(f);

	string[fsize] = '\0';

	*fileBytes = (byte *)string;

	return fsize;
}

bool isFileSameSize(string filePath, int size)
{
	struct stat filestatus;
	stat(filePath.c_str(), &filestatus);
	if (filestatus.st_size == size)
		return true;
	return false;
}

bool fileExists(string path)
{
	return (bool)ifstream(path);
}

vector<string> split(string initString, string delimiter){
	auto start = 0U;
	auto end = initString.find(delimiter);

	vector<string> substrings;

	while (end != std::string::npos)
	{
		substrings.push_back(initString.substr(start, end - start));
		start = end + delimiter.length();
		end = initString.find(delimiter, start);
	}

	substrings.push_back(initString.substr(start, end));

	return substrings;
}

Mapa leFicheiro(string fileName){
	ifstream myfile;

	myfile.open(fileName, ios::in);
	if (!myfile.is_open()) {
		cout << "Erro ao abrir " << fileName << "para ler" << endl;
		exit(1);
	}

	string linha;

	//LE COORDENADAS DE INICIO
	myfile >> linha;

	pos_t coordenadasPersonagem;
	vector<string> substrings = split(linha, SEPARADOR);
	if (!(substrings[0]).compare(COORD_INICIO_PERSONAGEM)){
		vector<string> pos = split(substrings[1], ",");
		
		coordenadasPersonagem.x = stof(pos[0]);
		coordenadasPersonagem.y = stof(pos[1]);
		coordenadasPersonagem.z = stof(pos[2]);
	}

	myfile >> linha;
	substrings = split(linha, SEPARADOR);
	string idQuartoInicial;
	if (!(substrings[0]).compare(ID_QUARTO_INICIAL)){
		idQuartoInicial = substrings[1];		
	}
	Mapa mapa(idQuartoInicial, coordenadasPersonagem);

	//LE ID PORTA DESTINO
	myfile >> linha;
	string idPortaDestino;
	substrings = split(linha, SEPARADOR);
	if (!(substrings[0]).compare(DESTINO)){
		idPortaDestino = substrings[1];
	}

	//LE O CAMINHO DAS TEXTURAS
	myfile >> linha;

	substrings = split(linha, SEPARADOR);
	vector<string>::iterator it = substrings.begin();

	if (!(*it).compare(TEX_PATH)){
		it++;
		string textPath = *it;
		//TODO fazer algo com o caminho das texturas
	}

	myfile >> linha;
	while (!myfile.eof()){
		substrings = split(linha, SEPARADOR);

		it = substrings.begin();

		if (!(*it).compare(ID_QUARTO)){
			string idQuarto = *(++it);
			Quarto q(idQuarto);
			do{   //enquanto nao encontrar inicio de um novo quarto
				myfile >> linha;
				if (myfile.eof()) break;

				substrings = split(linha, SEPARADOR);
				it = substrings.begin();

				if (!(*it).compare(PAREDES)){
					it++;
					pos_t posInit, posFinal;
					for (; it != substrings.end();)
					{
						vector<string> pontosParede = split(*it, ",");
						posInit.x = stof(pontosParede[0]);
						posInit.y = stof(pontosParede[1]);
						posInit.z = ALTURA_PAREDE;
						if ((++it) != substrings.end()){
							pontosParede = split(*it, ",");
							posFinal.x = stof(pontosParede[0]);
							posFinal.y = stof(pontosParede[1]);
							posFinal.z = ALTURA_PAREDE;
							q.addBloco(Bloco(posInit, posFinal));
						}						
					}
					vector<string> pontosParede = split(substrings[1], ",");
					posFinal.x = stof(pontosParede[0]);
					posFinal.y = stof(pontosParede[1]);
					posFinal.z = ALTURA_PAREDE;
					q.addBloco(Bloco(posInit, posFinal));
					continue;
				}

				if (!(*it).compare(PORTA)){
					string idPorta = *(++it);
					string posicao = *(++it);
					vector<string> pos = split(posicao, ",");
					pos_t coordenadasPorta;
					coordenadasPorta.x = stof(pos[0]);
					coordenadasPorta.y = stof(pos[1]);
					coordenadasPorta.z = stof(pos[2]);

					float rotacao = stof(*(++it));
					bool destino = false;
					if (idPortaDestino == idPorta)
						destino = true;
					if (++it != substrings.end()){
						string portaDestino = *it;
						q.addPorta(Porta(idPorta,portaDestino,coordenadasPorta,rotacao));
					}
					else{
						q.addPorta(Porta(idPorta, coordenadasPorta, rotacao));
					}
					if (destino){
						mapa.setIdQuartoFinal(idQuarto);
						mapa.setPosDestino(coordenadasPorta);
					}

					continue;
				}

				if (!(*it).compare(MURO)){
					string pontoInicial = *(++it);
					vector<string> pos = split(pontoInicial, ",");
					pos_t coordenadasIniciais, coordenadasFinais;
					coordenadasIniciais.x = stof(pos[0]);
					coordenadasIniciais.y = stof(pos[1]);
					coordenadasIniciais.z = stof(pos[2]);

					string pontoFinal = *(++it);
					pos = split(pontoFinal, ",");
					coordenadasFinais.x = stof(pos[0]);
					coordenadasFinais.y = stof(pos[1]);
					coordenadasFinais.z = stof(pos[2]);
					Bloco bloco = Bloco(coordenadasIniciais, coordenadasFinais);
					bloco.setMuro();
					q.addBloco(bloco);
					continue;
				}

				if (!(*it).compare(MUROSECRETO)){
					string pontoInicial = *(++it);
					vector<string> pos = split(pontoInicial, ",");
					pos_t coordenadasIniciais, coordenadasFinais;
					coordenadasIniciais.x = stof(pos[0]);
					coordenadasIniciais.y = stof(pos[1]);
					coordenadasIniciais.z = ALTURA_PAREDE;

					string pontoFinal = *(++it);
					pos = split(pontoFinal, ",");
					coordenadasFinais.x = stof(pos[0]);
					coordenadasFinais.y = stof(pos[1]);
					coordenadasFinais.z = ALTURA_PAREDE;

					string invisibilidade = *(++it);
					int valor = stoi(invisibilidade);
					bool colisao = true;
					if (valor == 0)
						colisao = false;
					q.addBloco(Bloco(coordenadasIniciais, coordenadasFinais,colisao));
					continue;
				}

				if (!(*it).compare(LUZ)){
					string posicao = *(++it);
					vector<string> pos = split(posicao, ",");
					pos_t coordenadasPosicao;
					coordenadasPosicao.x = stof(pos[0]);
					coordenadasPosicao.y = stof(pos[1]);
					coordenadasPosicao.z = ALTURA_LUZ;
					string cor = *(++it);
					vector<string> canaisCor = split(cor, ",");
					float corR = stof(canaisCor[0]);
					float corG = stof(canaisCor[1]);
					float corB = stof(canaisCor[2]);
					float alpha = stof(canaisCor[3]);
					
					q.addLuz(Luz(coordenadasPosicao, corR, corG, corB, alpha));
					continue;
				}

				if (!(*it).compare(ALCAPAO)){
					string posicao = *(++it);
					vector<string> pos = split(posicao, ",");
					pos_t coordenadasPosicao;
					coordenadasPosicao.x = stof(pos[0]);
					coordenadasPosicao.y = stof(pos[1]);
					coordenadasPosicao.z = stof(pos[2]);
					if ((it+1) != substrings.end()){
						string quartoDestino = *(++it);
						string destino = *(++it);
						vector<string> pos2 = split(destino, ",");
						pos_t coordenadasDestino;
						coordenadasDestino.x = stof(pos2[0]);
						coordenadasDestino.y = stof(pos2[1]);
						coordenadasDestino.z = stof(pos2[2]);
						Alcapao al(coordenadasPosicao, quartoDestino, coordenadasDestino);
						q.addAlcapoes(al);
					}
					else{
						Alcapao al(coordenadasPosicao);
						q.addAlcapoes(al);
					}
				}

				if (!(*it).compare(INIMIGO)){
					string posicao = *(++it);
					vector<string> pos = split(posicao, ",");
					pos_t coordenadasPosicao;
					coordenadasPosicao.x = stof(pos[0]);
					coordenadasPosicao.y = stof(pos[1]);
					coordenadasPosicao.z = stof(pos[2]);
					objecto_t ini;
					ini.pos = coordenadasPosicao;
					ini.nextPos = coordenadasPosicao;
					ini.dir = 0;
					ini.vel = 0;
					ini.vida = 100;
					q.addInimigo(ini);
				}

				if (!(*it).compare(OBSTACULO)){
					string posicao = *(++it);
					vector<string> pos = split(posicao, ",");
					pos_t coordenadasPosicao;
					coordenadasPosicao.x = stof(pos[0]);
					coordenadasPosicao.y = stof(pos[1]);
					coordenadasPosicao.z = stof(pos[2]);

					string rotacao = *(++it);
					int valor = stoi(rotacao);

					objecto_t obs;
					obs.pos = coordenadasPosicao;
					obs.dir = valor;
					q.addObstaculo(obs);
				}

			} while (substrings[0].compare(ID_QUARTO));
			mapa.addQuarto(q);
		}
	}
	myfile.close();
	return mapa;
}

//bool escreveCaminho(string fileName)
//{
//	TODO
//
//	return false;
//}
//
//Caminho leFicheiroCaminho(string fileName)
//{
//	Caminho c;
//	TODO
//
//	return c;
//}
