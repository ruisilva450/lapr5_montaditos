//descomentar para desactivar a consola
//#pragma comment(linker, "/subsystem:\"windows\" /entry:\"mainCRTStartup\"") 

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <AL/alut.h>
#include <iomanip>

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#ifdef _WIN32
#include <GL/glaux.h>
#endif

#include "legacy/mathlib.h"
#include "legacy/studio.h"
#include "legacy/mdlviewer.h"


#pragma comment (lib, "glaux.lib")    /* link with Win32 GLAUX lib usada para ler bitmaps */

// funo para ler jpegs do ficheiro readjpeg.c
extern "C" int read_JPEG_file(const char *, char **, int *, int *, int *);


/************************
MODELO
*************************/

#include "headers/GVar.h"
#include "Trig.h"
#include "headers/Structs.h"
#include "fileUtils.h"
#include "WebService/WebClient.h"
#include "headers/Lights.h"
#include "headers/Sounds.h"
#include "headers/Rain.h"
#include "modelo/Pontuacao.h"
#include "modelo/Caminho.h"
#include "modelo/Projectil.h"
#include "headers/IA.h"
#include "glui.h"

/////////////////////////////////////
//variaveis globais

ESTADO estado;
MODELO modelo;
Mapa mapa;
Quarto q;
Pontuacao p;
WebClient webs;
Caminho caminho;
Rain chuva;
char* go = "";
int n = 0;
int nPorta = -1;
bool emPorta = false;
bool emPausa = FALSE;
vector <Projectil> projecteis, explosao;
time_t tempoTiro;

string ficheiroCaminho = "Caminhos/demo3.mtdts_p2.path";

GLUI *glui, *upload;

//Variáveis live
char username[sizeof(GLUI_String)] = "p2", password[sizeof(GLUI_String)] = "qwerty";
GLUI_StaticText *mensagemErroLogin, *mensagemErroMapa, *pontuacao;
GLUI_EditText *usernameTxt, *passwordTxt;
GLUI_Button *loginBot, *exitBot, *selectBot, *exitFim, *uploadBot, *restartBot, *exitUpload;
GLUI_Listbox *mapas;
Sound backgroundMusic, customBackgroundMusic, dohFX, tiroFX, explosaoFX;

void init(string mapLocation);
/************************
RENDERER
*************************/


///////////////////////////////////
/// Texturas

// S para windows (usa biblioteca glaux)
#ifdef _WIN32

AUX_RGBImageRec *LoadBMP(char *Filename)				// Loads A Bitmap Image
{
	FILE *File = NULL;									// File Handle

	if (!Filename)										// Make Sure A Filename Was Given
	{
		return NULL;									// If Not Return NULL
	}

	File = fopen(Filename, "r");							// Check To See If The File Exists

	if (File)											// Does The File Exist?
	{
		fclose(File);									// Close The Handle
		return auxDIBImageLoad(Filename);				// Load The Bitmap And Return A Pointer
	}

	return NULL;										// If Load Failed Return NULL
}
#endif


void createTextures(GLuint texID[])
{
	char *image;
	int w, h, bpp;

#ifdef _WIN32
	AUX_RGBImageRec *TextureImage[1];					// Create Storage Space For The Texture

	memset(TextureImage, 0, sizeof(void *) * 1);           	// Set The Pointer To NULL
#endif

	glGenTextures(NUM_TEXTURAS, texID);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

#ifdef _WIN32

	if (TextureImage[0] = LoadBMP(NOME_TEXTURA_CUBOS))
	{
		// Create MipMapped Texture
		glBindTexture(GL_TEXTURE_2D, texID[ID_TEXTURA_CUBOS]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);

		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, TextureImage[0]->sizeX, TextureImage[0]->sizeY, GL_RGB, GL_UNSIGNED_BYTE, TextureImage[0]->data);
	}
#else
	if (read_JPEG_file(NOME_TEXTURA_CUBOS, &image, &w, &h, &bpp))
	{
		// Create MipMapped Texture
		glBindTexture(GL_TEXTURE_2D, texID[ID_TEXTURA_CUBOS]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);

		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, w, h, GL_RGB, GL_UNSIGNED_BYTE, image);
	}
#endif
	else
	{
		printf("Textura %s not Found\n", NOME_TEXTURA_CUBOS);
		system("PAUSE");
		exit(0);
	}

#ifdef _WIN32

	if (TextureImage[0] = LoadBMP(NOME_TEXTURA_MENU_BACKGROUND))
	{
		// Create MipMapped Texture
		glBindTexture(GL_TEXTURE_2D, texID[ID_TEXTURA_MENU_BACKGROUND]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);

		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, TextureImage[0]->sizeX, TextureImage[0]->sizeY, GL_RGB, GL_UNSIGNED_BYTE, TextureImage[0]->data);
	}
#else
	if (read_JPEG_file(NOME_TEXTURA_MENU_BACKGROUND, &image, &w, &h, &bpp))
	{
		// Create MipMapped Texture
		glBindTexture(GL_TEXTURE_2D, texID[ID_TEXTURA_MENU_BACKGROUND]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);

		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, w, h, GL_RGB, GL_UNSIGNED_BYTE, image);
	}
#endif
	else
	{
		printf("Textura %s not Found\n", NOME_TEXTURA_CUBOS);
		system("PAUSE");
		exit(0);
	}

	if (read_JPEG_file(NOME_TEXTURA_CHAO, &image, &w, &h, &bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texID[ID_TEXTURA_CHAO]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, w, h, GL_RGB, GL_UNSIGNED_BYTE, image);
	}
	else{
		printf("Textura %s not Found\n", NOME_TEXTURA_CHAO);
		system("PAUSE");
		exit(0);
	}

	if (read_JPEG_file(NOME_TEXTURA_CEU_DIA, &image, &w, &h, &bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texID[ID_TEXTURA_CEU_DIA]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, w, h, GL_RGB, GL_UNSIGNED_BYTE, image);
	}
	else{
		printf("Textura %s not Found\n", NOME_TEXTURA_CEU_DIA);
		system("PAUSE");
		exit(0);
	}

	if (read_JPEG_file(NOME_TEXTURA_CEU_NOITE, &image, &w, &h, &bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texID[ID_TEXTURA_CEU_NOITE]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, w, h, GL_RGB, GL_UNSIGNED_BYTE, image);
	}
	else{
		printf("Textura %s not Found\n", NOME_TEXTURA_CEU_NOITE);
		system("PAUSE");
		exit(0);
	}

	glBindTexture(GL_TEXTURE_2D, NULL);
}

void createBGTextures(GLuint texID[])
{
	
#ifdef _WIN32
	AUX_RGBImageRec *TextureImage[1];					// Create Storage Space For The Texture

	memset(TextureImage, 0, sizeof(void *) * 1);           	// Set The Pointer To NULL
#endif

	glGenTextures(1, texID);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

#ifdef _WIN32

	if (TextureImage[0] = LoadBMP(NOME_TEXTURA_MENU_BACKGROUND))
	{
		// Create MipMapped Texture
		glBindTexture(GL_TEXTURE_2D, texID[ID_TEXTURA_CUBOS]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);

		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, TextureImage[0]->sizeX, TextureImage[0]->sizeY, GL_RGB, GL_UNSIGNED_BYTE, TextureImage[0]->data);
	}
#else
	{
		char *image;
		int w, h, bpp;
		if (read_JPEG_file(NOME_TEXTURA_MENU_BACKGROUND, &image, &w, &h, &bpp))
		{
			// Create MipMapped Texture
			glBindTexture(GL_TEXTURE_2D, texID[ID_TEXTURA_MENU_BACKGROUND]);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);

			gluBuild2DMipmaps(GL_TEXTURE_2D, 3, w, h, GL_RGB, GL_UNSIGNED_BYTE, image);
		}
	}
#endif
	else
	{
		printf("Textura %s not Found\n", NOME_TEXTURA_MENU_BACKGROUND);
		system("PAUSE");
		exit(0);
	}

	glBindTexture(GL_TEXTURE_2D, NULL);
}

void createDisplayLists(int janelaID, Quarto toDraw);

void reiniciarTeclas()
{
	estado.teclas.lanterna = AL_FALSE;
	estado.teclas.luz = AL_FALSE;
	estado.teclas.up = GL_FALSE;
	estado.teclas.down = GL_FALSE;
	estado.teclas.left = GL_FALSE;
	estado.teclas.right = GL_FALSE;
}

void reiniciarCamara()
{
	estado.camera.eye.x = 0;
	estado.camera.eye.y = OBJECTO_ALTURA * 2;
	estado.camera.eye.z = 0;
	estado.camera.dir_long = modelo.objecto.dir;
	estado.camera.dir_lat = 0;
}

void reiniciarJogo()
{
	init(estado.ficheiroMapa);
	q = mapa.getQuartoInicial();
	dohFX.switchAudio(AL_FALSE);
	go = "";
	p.reset();
	glDeleteLists(modelo.chao[VISTA_NAVIGATE], 1);
	modelo.quartoAtual = stringToChar(q.getID());
	createDisplayLists(VISTA_NAVIGATE, q);
	modelo.objecto.pos = mapa.getCoordInicioPersonagem();
	modelo.objecto.dir = 0;
	modelo.objecto.vida = 100;
	reiniciarTeclas();
	reiniciarCamara();
	emPausa = FALSE;
}



void desenhaPoligono(GLfloat a[], GLfloat b[], GLfloat c[], GLfloat  d[], GLfloat normal[], float tx, float ty)
{
	glBegin(GL_POLYGON);
	glNormal3fv(normal);
	glTexCoord2f(tx, ty);
	glVertex3fv(a);
	glTexCoord2f(tx, ty + 1);
	glVertex3fv(b);
	glTexCoord2f(tx + 1, ty + 1);
	glVertex3fv(c);
	glTexCoord2f(tx + 1, ty);
	glVertex3fv(d);
	glEnd();
}

void desenhaPoligono(GLfloat a[], GLfloat b[], GLfloat c[], GLfloat  d[], GLfloat normal[])
{
	glBegin(GL_POLYGON);
	glNormal3fv(normal);
	glColor3f(0.0, 0.0, 0.0);
	glVertex3fv(a);
	glVertex3fv(b);
	glVertex3fv(c);
	glVertex3fv(d);
	glEnd();
}

void desenhaPoligono(GLfloat a[], GLfloat b[], GLfloat c[], GLfloat  d[])
{
	glBegin(GL_POLYGON);
	//glColor3f(0.0, 0.0, 0.0);
	glVertex3fv(a);
	glVertex3fv(b);
	glVertex3fv(c);
	glVertex3fv(d);
	glEnd();
}


void desenhaAlcapoes(vector<Alcapao> alcapoes){
	GLfloat vertices[][3] = { { -0.5, -0.5, -0.5 },
	{ 0.5, -0.5, -0.5 },
	{ 0.5, 0.5, -0.5 },
	{ -0.5, 0.5, -0.5 },
	{ -0.5, -0.5, 0.5 },
	{ 0.5, -0.5, 0.5 },
	{ 0.5, 0.5, 0.5 },
	{ -0.5, 0.5, 0.5 } };
	GLfloat normais[][3] = { { 0, 0, -1 },
	{ 0, 1, 0 },
	{ -1, 0, 0 },
	{ 1, 0, 0 },
	{ 0, 0, 1 },
	{ 0, -1, 0 },
	};
	glPushName(NOME_ALCAPOES);
	float tx, ty;
	tx = 0;
	ty = 0;
	for (size_t i = 0; i < alcapoes.size(); i++){
		glPushName(i);
		glPushMatrix();
		glTranslatef(alcapoes.at(i).getPosicao().x, alcapoes.at(i).getPosicao().y + 0.52, alcapoes.at(i).getPosicao().z);
		desenhaPoligono(vertices[5], vertices[4], vertices[0], vertices[1], normais[5]);
		glPopMatrix();
		glPopName();
	}
	glPopName();
}

void desenhaCubo(int tipoTextura, GLuint texID)
{
	float tx, ty;
	GLfloat vertices[][3] = { { -0.5, -0.5, -0.5 },
	{ 0.5, -0.5, -0.5 },
	{ 0.5, 0.5, -0.5 },
	{ -0.5, 0.5, -0.5 },
	{ -0.5, -0.5, 0.5 },
	{ 0.5, -0.5, 0.5 },
	{ 0.5, 0.5, 0.5 },
	{ -0.5, 0.5, 0.5 } };
	GLfloat normais[][3] = { { 0, 0, -1 },
	{ 0, 1, 0 },
	{ -1, 0, 0 },
	{ 1, 0, 0 },
	{ 0, 0, 1 },
	{ 0, -1, 0 },
	};

	switch (tipoTextura)
	{
		case 0: tx = 0, ty = 0;
			break;
		case 1: tx = 0, ty = 0.25;
			break;
		case 2: tx = 0, ty = 0.5;
			break;
		case 3: tx = 0, ty = 0.75;
			break;
		case 4: tx = 0.25, ty = 0;
			break;

		default:
			tx = 0.75, ty = 0.75;
	}

	glBindTexture(GL_TEXTURE_2D, texID);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

	desenhaPoligono(vertices[1], vertices[0], vertices[3], vertices[2], normais[0], tx, ty);
	desenhaPoligono(vertices[2], vertices[3], vertices[7], vertices[6], normais[1], tx, ty);
	desenhaPoligono(vertices[3], vertices[0], vertices[4], vertices[7], normais[2], tx, ty);
	desenhaPoligono(vertices[6], vertices[5], vertices[1], vertices[2], normais[3], tx, ty);
	desenhaPoligono(vertices[4], vertices[5], vertices[6], vertices[7], normais[4], tx, ty);
	desenhaPoligono(vertices[5], vertices[4], vertices[0], vertices[1], normais[5], tx, ty);

	glBindTexture(GL_TEXTURE_2D, NULL);
}

// Desenha um polígono regular de n lados, com o centro geométrico no ponto de coordenadas (x0,y0,z0) e distância aos vértices 'r'
void desenhaCilindro(GLint n, GLfloat x, GLfloat y, GLfloat zbase, GLfloat ztopo, GLfloat r, int tipoTextura, GLuint texID) {

	Ponto pCentro;
	pCentro.x = x;
	pCentro.y = y;

	vector<Ponto> pontos = divideCircPartesIguais(pCentro, r, n);

	for each (Ponto p in pontos) {
		cout << "x: " << p.x << "\ty:" << p.y << endl;
	}
	
	glBindTexture(GL_TEXTURE_2D, texID);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

	glColor3f(0.8f, 0.8f, 0.8f);

	glBegin(GL_POLYGON);
		/*Desenha Base*/
		for (Ponto ponto : pontos) {
			glVertex3f(ponto.x, ponto.y, zbase);
		}
	glEnd();
	glBegin(GL_POLYGON);
		/*Desenha Topo*/
		for (Ponto ponto : pontos) {
			glVertex3f(ponto.x, ponto.y, ztopo);
		}
	glEnd();
	

		for (int i = 0; i < n; i++)
		{
			GLfloat v1[] = { pontos[i].x,		pontos[i].y,		zbase };
			GLfloat v2[] = { pontos[(i+1)%n].x, pontos[(i+1)%n].y,	zbase };
			GLfloat v3[] = { pontos[(i+1)%n].x, pontos[(i+1)%n].y,	ztopo };
			GLfloat v4[] = { pontos[i].x,		pontos[i].y,		ztopo };

			//cout << "i: " << i << "\t(i+1)%n: " << (i+1)%n << endl;

			desenhaPoligono(v1, v2, v3, v4);

			//GLfloat cantos[][3] = {
			//						{ pontos[i].x,		pontos[i].y,	zbase },
			//						{ pontos[i+1].x,	pontos[i+1].y,	zbase },
			//						{ pontos[i+1].x,	pontos[i+1].y,	ztopo },
			//						{ pontos[i].x,		pontos[i].y,	ztopo },
			//					  };

			//desenhaPoligono(cantos[0], cantos[1], cantos[2], cantos[3]);
		}

	glBindTexture(GL_TEXTURE_2D, NULL);
}

void desenhaCilindro(GLfloat x, GLfloat y, GLfloat raio, GLfloat altura, int slices) {
	
	//glColor3f(157/256.0, 93/256.0, 23/256.0);

	glPushMatrix();
	glBindTexture(GL_TEXTURE_2D, modelo.texID[VISTA_NAVIGATE][ID_TEXTURA_CUBOS]);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

		glTranslatef(x, y, 0);

		GLUquadricObj *quadObj = gluNewQuadric();
		gluQuadricOrientation(quadObj, GLU_OUTSIDE);
		gluQuadricNormals(quadObj, true);
		gluQuadricTexture(quadObj, true);

		gluCylinder(quadObj, raio, raio, altura, slices, 1);

		glPushMatrix();
			glTranslatef(0, 0, altura);
			gluDisk(quadObj, 0, raio, slices, 1);
		glPopMatrix();
		glBindTexture(GL_TEXTURE_2D, NULL);

	glPopMatrix();
}

void desenhaDisco(GLfloat x, GLfloat y, GLfloat raio, GLfloat altura, int slices) {

	glPushMatrix();
	glBindTexture(GL_TEXTURE_2D, modelo.texID[VISTA_NAVIGATE][ID_TEXTURA_CUBOS]);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glTranslatef(x, y, 0);

	GLUquadricObj *quadObj = gluNewQuadric();
	gluQuadricOrientation(quadObj, GLU_OUTSIDE);
	gluQuadricNormals(quadObj, true);
	gluQuadricTexture(quadObj, true);

	glPushMatrix();
	glTranslatef(0, 0, altura);
	gluDisk(quadObj, 0, raio, slices, 1);
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, NULL);

	glPopMatrix();
}


void desenhaPortas(vector<Porta> portas, int tipoTextura, int janelaID)
{
	pos_t posicao;
	int rotacao;
	GLuint texID = modelo.texID[janelaID][ID_TEXTURA_CUBOS];
	//cout << "\nNportas: " << portas.size();
	for (Porta porta : portas) {
		glPushName(NOME_PORTAS + stoi(porta.getId()));
		glPushMatrix();
		posicao = porta.getPosicao();
		rotacao = porta.getRotacao();
		//glTranslatef(MAZE_HEIGHT*0.5, 0.5, MAZE_WIDTH*0.5);
		//cout << "\nPorta x : " << posicao.x << "\ty: " << posicao.y;
		glTranslatef(posicao.x, 0.5, -posicao.y);
		glRotatef(rotacao, 0, 1, 0);
		glScalef(0.5, 1, 0.1);
		desenhaCubo(tipoTextura, texID);
		glPopMatrix();
		glPopName();
	}

	/*
	pos_t posicao;
	posicao.x = MAZE_HEIGHT*0.5;
	posicao.y = 0.5;
	posicao.z = MAZE_WIDTH*0.5;
	int rotacao = 0; //0-360
	desenhaPorta(posicao, rotacao, 1, modelo.texID[janelaID][ID_TEXTURA_CUBOS]);
	*/
}

void desenhaBloco2(Bloco bloco, int tipoTextura, GLuint texID)
{
	glPushMatrix();
	float tx, ty;
	glRotatef(bloco.getRotacao(), 0.0, 0.0, 1.0);
	GLfloat vertices[][3] = { { -0.5*bloco.getComprimento(), -0.5, 0 },
	{ 0.5*bloco.getComprimento(), -0.5, 0 },
	{ 0.5*bloco.getComprimento(), 0.5, 0 },
	{ -0.5*bloco.getComprimento(), 0.5, 0 },
	{ -0.5*bloco.getComprimento(), -0.5, bloco.getPosicaoFinal().z },
	{ 0.5*bloco.getComprimento(), -0.5, bloco.getPosicaoInicial().z },
	{ 0.5*bloco.getComprimento(), 0.5, bloco.getPosicaoInicial().z },
	{ -0.5*bloco.getComprimento(), 0.5, bloco.getPosicaoFinal().z } };
	GLfloat normais[][3] = { { 0, 0, -1 },
	{ 0, 1, 0 },
	{ -1, 0, 0 },
	{ 1, 0, 0 },
	{ 0, 0, 1 },
	{ 0, -1, 0 },
	};

	switch (tipoTextura)
	{
		case 0: tx = 0, ty = 0;
			break;
		case 1: tx = 0, ty = 0.25;
			break;
		case 2: tx = 0, ty = 0.5;
			break;
		case 3: tx = 0, ty = 0.75;
			break;
		case 4: tx = 0.25, ty = 0;
			break;

		default:
			tx = 0.75, ty = 0.75;
	}
	desenhaPoligono(vertices[4], vertices[5], vertices[6], vertices[7], normais[4], tx, ty);
	glPopMatrix();
}

void desenhaBloco(Bloco bloco, int tipoTextura, GLuint texID)
{

	glPushMatrix();
		float tx, ty;
		glRotatef(bloco.getRotacao(), 0.0, 0.0, 1.0);

		GLfloat vertices[][3] = { { -0.5*bloco.getComprimento(), -0.5, 0 },
								{ 0.5*bloco.getComprimento(), -0.5, 0 },
								{ 0.5*bloco.getComprimento(), 0.5, 0 },
								{ -0.5*bloco.getComprimento(), 0.5, 0 },
								{ -0.5*bloco.getComprimento(), -0.5, bloco.getPosicaoFinal().z },
								{ 0.5*bloco.getComprimento(), -0.5, bloco.getPosicaoInicial().z },
								{ 0.5*bloco.getComprimento(), 0.5, bloco.getPosicaoInicial().z },
								{ -0.5*bloco.getComprimento(), 0.5, bloco.getPosicaoFinal().z } };

		GLfloat normais[][3] = { { 0, 0, -1 },
		{ 0, 1, 0 },
		{ -1, 0, 0 },
		{ 1, 0, 0 },
		{ 0, 0, 1 },
		{ 0, -1, 0 },
		};

		switch (tipoTextura)
		{
			case 0: tx = 0, ty = 0;
				break;
			case 1: tx = 0, ty = 0.25;
				break;
			case 2: tx = 0, ty = 0.5;
				break;
			case 3: tx = 0, ty = 0.75;
				break;
			case 4: tx = 0.25, ty = 0;
				break;

			default:
				tx = 0.75, ty = 0.75;
		}

		glBindTexture(GL_TEXTURE_2D, texID);

		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

		desenhaPoligono(vertices[1], vertices[0], vertices[3], vertices[2], normais[0], tx, ty);
		desenhaPoligono(vertices[2], vertices[3], vertices[7], vertices[6], normais[1], tx, ty);
		desenhaPoligono(vertices[3], vertices[0], vertices[4], vertices[7], normais[2], tx, ty);
		desenhaPoligono(vertices[6], vertices[5], vertices[1], vertices[2], normais[3], tx, ty);
		desenhaPoligono(vertices[4], vertices[5], vertices[6], vertices[7], normais[4], tx, ty);
		desenhaPoligono(vertices[5], vertices[4], vertices[0], vertices[1], normais[5], tx, ty);

		glBindTexture(GL_TEXTURE_2D, NULL);

	glPopMatrix();
}

void desenhaObstaculo(int tipoTextura, GLuint texID)
{
	glPushMatrix();
	float tx, ty;
	GLfloat vertices[][3] = { { -0.5, -0.5, 0 },
	{ 0.5, -0.5, 0 },
	{ 0.5, 0.5, 0 },
	{ -0.5, 0.5, 0 },
	{ -0.5, -0.5, 0.5 },
	{ 0.5, -0.5, 0.5 },
	{ 0.5, 0.5, 0.5 },
	{ -0.5, 0.5, 0.5 } };
	GLfloat normais[][3] = { { 0, 0, -1 },
	{ 0, 1, 0 },
	{ -1, 0, 0 },
	{ 1, 0, 0 },
	{ 0, 0, 1 },
	{ 0, -1, 0 },
	};

	switch (tipoTextura)
	{
		case 0: tx = 0, ty = 0;
			break;
		case 1: tx = 0, ty = 0.25;
			break;
		case 2: tx = 0, ty = 0.5;
			break;
		case 3: tx = 0, ty = 0.75;
			break;
		case 4: tx = 0.25, ty = 0;
			break;

		default:
			tx = 0.75, ty = 0.75;
	}

	glBindTexture(GL_TEXTURE_2D, texID);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	desenhaPoligono(vertices[1], vertices[0], vertices[3], vertices[2], normais[0], tx, ty);
	desenhaPoligono(vertices[2], vertices[3], vertices[7], vertices[6], normais[1], tx, ty);
	desenhaPoligono(vertices[3], vertices[0], vertices[4], vertices[7], normais[2], tx, ty);
	desenhaPoligono(vertices[6], vertices[5], vertices[1], vertices[2], normais[3], tx, ty);
	desenhaPoligono(vertices[4], vertices[5], vertices[6], vertices[7], normais[4], tx, ty);
	desenhaPoligono(vertices[5], vertices[4], vertices[0], vertices[1], normais[5], tx, ty);

	glBindTexture(GL_TEXTURE_2D, NULL);
	glPopMatrix();
}

void desenhaLabirinto2(vector<Bloco> blocos, int janelaID)
{
	glColor3f(0.8f, 0.8f, 0.8f);
	glPushMatrix();
	//glTranslatef(-MAZE_HEIGHT*0.5, 0.5, -MAZE_WIDTH*0.5);
	//glTranslatef(0, 0.5, 0);
	glRotatef(-90, 1.0, 0.0, 0.0);
	//cout << "Zi: " << blocos.at(4).getPosicaoInicial().z << " , Zf: " << blocos.at(4).getPosicaoFinal().z << endl;

	for (unsigned i = 0; i < blocos.size(); i++)
	{
		if (blocos.at(i).isBlocoSolido())
			glPushName(i);
		else
			glPushName(NOME_PORTA_SECRETA);

		/*Desenha os cantos redondos dos blocos*/
		bool desenharCilindroInicial = true;
		bool desenharCilindroFinal = true;
		/*Verifica apenas os blocos que pertencem ao centro do labirinto*/
		if (blocos.at(i).isMuro() && !blocos.at(i).isRampa()) {
			/*Verifica se existe alguma rampa junto ao bloco atual*/
			for (size_t k = 0; k < blocos.size(); k++) {
				if (blocos.at(k).isRampa()) {

					if (blocos.at(i).getPosicaoInicial().x == blocos.at(k).getPosicaoInicial().x
						|| blocos.at(i).getPosicaoInicial().x == blocos.at(k).getPosicaoFinal().x
						|| blocos.at(i).getPosicaoInicial().y == blocos.at(k).getPosicaoInicial().y
						|| blocos.at(i).getPosicaoInicial().y == blocos.at(k).getPosicaoFinal().y)
					{
						desenharCilindroInicial = false;
					}
					if (blocos.at(i).getPosicaoFinal().x == blocos.at(k).getPosicaoInicial().x
						|| blocos.at(i).getPosicaoFinal().x == blocos.at(k).getPosicaoFinal().x
						|| blocos.at(i).getPosicaoFinal().y == blocos.at(k).getPosicaoInicial().y
						|| blocos.at(i).getPosicaoFinal().y == blocos.at(k).getPosicaoFinal().y)
					{
						desenharCilindroFinal = false;
					}
				}
			}

			/*Desenha os cilindros que não estão junto a rampas*/
			if (desenharCilindroInicial) {
				desenhaDisco(blocos.at(i).getPosicaoInicial().x, blocos.at(i).getPosicaoInicial().y, 0.5, 0.50, 32);
			}
			if (desenharCilindroFinal) {
				desenhaDisco(blocos.at(i).getPosicaoFinal().x, blocos.at(i).getPosicaoFinal().y, 0.5, 0.50, 32);
			}
		}

		glPushMatrix();
		glTranslatef(blocos.at(i).getPosicaoCentral().x, blocos.at(i).getPosicaoCentral().y, 0);
		desenhaBloco2(blocos.at(i), 0, modelo.texID[janelaID][ID_TEXTURA_CUBOS]);
		glPopMatrix();
		glPopName();
	}

	vector<objecto_t> obstaculos = q.getObstaculos();

	for (unsigned i = 0; i < obstaculos.size(); i++)
	{
		glPushName(NOME_OBSTACULOS);
		glPushMatrix();
		glTranslatef(obstaculos.at(i).pos.x, obstaculos.at(i).pos.y, 0);
		glRotatef(obstaculos.at(i).dir, 0, 0, 1);
		desenhaObstaculo(0, modelo.texID[janelaID][ID_TEXTURA_CUBOS]);
		glPopMatrix();
		glPopName();
	}
	glPopMatrix();

}


void desenhaLabirinto(vector<Bloco> blocos, int janelaID)
{
	glColor3f(0.8f, 0.8f, 0.8f);
	glPushMatrix();
	//glTranslatef(-MAZE_HEIGHT*0.5, 0.5, -MAZE_WIDTH*0.5);
	//glTranslatef(0, 0.5, 0);
	glRotatef(-90, 1.0, 0.0, 0.0);
	//cout << "Zi: " << blocos.at(4).getPosicaoInicial().z << " , Zf: " << blocos.at(4).getPosicaoFinal().z << endl;

	for (unsigned i = 0; i < blocos.size(); i++)
	{
			/*cout << "init\tx: " << blocos.at(i).getPosicaoInicial().x << "\ty: " << blocos.at(i).getPosicaoInicial().y << "\tz: " << blocos.at(i).getPosicaoInicial().z << endl;
			cout << "final\tx: " << blocos.at(i).getPosicaoFinal().x << "\ty: " << blocos.at(i).getPosicaoFinal().y << "\tz: " << blocos.at(i).getPosicaoFinal().z << endl << endl
			count++;*/

			if (blocos.at(i).isBlocoSolido())
				glPushName(i);
			else
				glPushName(NOME_PORTA_SECRETA);

			/*Desenha os cantos redondos dos blocos*/
			bool desenharCilindroInicial = true;
			bool desenharCilindroFinal = true;
			/*Verifica apenas os blocos que pertencem ao centro do labirinto*/
			if (blocos.at(i).isMuro() && !blocos.at(i).isRampa()) {
				/*Verifica se existe alguma rampa junto ao bloco atual*/
				for (size_t k = 0; k < blocos.size(); k++) {
					if (blocos.at(k).isRampa()) {

						if (   blocos.at(i).getPosicaoInicial().x	== blocos.at(k).getPosicaoInicial().x
							|| blocos.at(i).getPosicaoInicial().x	== blocos.at(k).getPosicaoFinal().x
							|| blocos.at(i).getPosicaoInicial().y	== blocos.at(k).getPosicaoInicial().y
							|| blocos.at(i).getPosicaoInicial().y	== blocos.at(k).getPosicaoFinal().y)
						{
							desenharCilindroInicial = false;
						}
						if (   blocos.at(i).getPosicaoFinal().x == blocos.at(k).getPosicaoInicial().x
							|| blocos.at(i).getPosicaoFinal().x == blocos.at(k).getPosicaoFinal().x
							|| blocos.at(i).getPosicaoFinal().y == blocos.at(k).getPosicaoInicial().y
							|| blocos.at(i).getPosicaoFinal().y == blocos.at(k).getPosicaoFinal().y)
						{
							desenharCilindroFinal = false;
						}
					}
				}

				/*Desenha os cilindros que não estão junto a rampas*/
				if (desenharCilindroInicial) {
					desenhaCilindro(blocos.at(i).getPosicaoInicial().x, blocos.at(i).getPosicaoInicial().y, 0.5, 0.997, 32);
				}
				if (desenharCilindroFinal) {
					desenhaCilindro(blocos.at(i).getPosicaoFinal().x, blocos.at(i).getPosicaoFinal().y, 0.5, 0.997, 32);
				}
			}

			glPushMatrix();
				glTranslatef(blocos.at(i).getPosicaoCentral().x, blocos.at(i).getPosicaoCentral().y, 0);
				desenhaBloco(blocos.at(i), 0, modelo.texID[janelaID][ID_TEXTURA_CUBOS]);
			glPopMatrix();
			glPopName();
	}

	vector<objecto_t> obstaculos = q.getObstaculos();

	for (unsigned i = 0; i < obstaculos.size(); i++)
	{
		glPushName(NOME_OBSTACULOS);
		glPushMatrix();
		glTranslatef(obstaculos.at(i).pos.x, obstaculos.at(i).pos.y, 0);
		glRotatef(obstaculos.at(i).dir, 0, 0, 1);
		desenhaObstaculo(0, modelo.texID[janelaID][ID_TEXTURA_CUBOS]);
		glPopMatrix();
		glPopName();
	}
	glPopMatrix();

}

void desenhaChao(vector<pos_t> pos, GLuint texID)
{
	GLfloat i, j;
	glBindTexture(GL_TEXTURE_2D, texID);

	glColor3f(0.5f, 0.5f, 0.5f);
	glPushMatrix();
	glRotatef(180, 1, 0, 0);
	for (i = pos.at(0).x; i < pos.at(2).x; i++)
	{
		for (j = pos.at(0).y; j < pos.at(2).y; j++)
		{
			glBegin(GL_POLYGON);
			glNormal3f(0, 1, 0);

			glTexCoord2f(1, 1);
			glVertex3f(i + STEP, 0, j + STEP);

			glTexCoord2f(0, 1);
			glVertex3f(i, 0, j + STEP);

			glTexCoord2f(0, 0);
			glVertex3f(i, 0, j);

			glTexCoord2f(1, 0);
			glVertex3f(i + STEP, 0, j);

			glEnd();
		}
	}
	glPopMatrix();
}

void desenhaCeu(vector<pos_t> pos)
{
	if (estado.teclas.luz)
		glBindTexture(GL_TEXTURE_2D, modelo.texID[VISTA_NAVIGATE][ID_TEXTURA_CEU_DIA]);
	else
		glBindTexture(GL_TEXTURE_2D, modelo.texID[VISTA_NAVIGATE][ID_TEXTURA_CEU_NOITE]);

	glColor3f(0.5f, 0.5f, 0.5f);
	glPushMatrix();
	glRotatef(-90, 1, 0, 0);
	pos_t canto_inf_esquerdo = pos.at(0);
	pos_t canto_inf_direito = pos.at(1);
	pos_t canto_sup_direito = pos.at(2);
	pos_t canto_sup_esquerdo = pos.at(3);

	GLfloat vertices[][3] = { { canto_inf_esquerdo.x - 10, canto_inf_esquerdo.y - 10, -1 }, //inf_esq_baixo
	{ canto_inf_esquerdo.x - 10, canto_inf_esquerdo.y - 10, 11 }, //inf_esq_cima
	{ canto_inf_direito.x + 10, canto_inf_direito.y - 10, -1 }, //inf_dir_baixo
	{ canto_inf_direito.x + 10, canto_inf_direito.y - 10, 11 }, //inf_dir_cima
	{ canto_sup_direito.x + 10, canto_sup_direito.y + 10, -1 }, //sup_dir_baixo
	{ canto_sup_direito.x + 10, canto_sup_direito.y + 10, 11 }, //sup_dir_cima 
	{ canto_sup_esquerdo.x - 10, canto_sup_esquerdo.y + 10, -1 }, //sup_esq_baixo
	{ canto_sup_esquerdo.x - 10, canto_sup_esquerdo.y + 10, 11 } //sup_esq_cima
	};
	GLfloat normais[][3] = { { 0, 0, 1 },
	{ 0, 1, 0 },
	{ 1, 0, 0 },
	{ 1, 0, 0 },
	{ 0, 0, 1 },
	{ 0, 1, 0 },
	};

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	desenhaPoligono(vertices[0], vertices[1], vertices[3], vertices[2], normais[0], 0, 0); // INFERIOR
	desenhaPoligono(vertices[2], vertices[3], vertices[5], vertices[4], normais[1], 0, 0); // DIREITO
	desenhaPoligono(vertices[4], vertices[5], vertices[7], vertices[6], normais[2], 0, 0); // SUPERIOR
	desenhaPoligono(vertices[6], vertices[7], vertices[1], vertices[0], normais[3], 0, 0); // ESQUERDO
	desenhaPoligono(vertices[1], vertices[3], vertices[5], vertices[7], normais[4], 0, 0); // TETO

	glBindTexture(GL_TEXTURE_2D, NULL);
	glPopMatrix();

}


void renderMap(GLuint texID, Quarto qa)
{
	vector<pos_t> pos = qa.generateDimensoes();
	vector<pos_t> luzes = qa.getPosLuzes();
	vector<Bloco> blocos = qa.getBlocos();
	vector<Porta> portas = qa.getPortas();
	desenhaChao(pos, texID);
	desenhaLabirinto(blocos, 1);
	desenhaPortas(portas, 1, 1);
	desenhaAlcapoes(qa.getAlcapoes());

	Quarto qs = mapa.getQuartoInicial();
	if (qs.getID() == qa.getID())
	{
		modelo.objecto.pos = mapa.getCoordInicioPersonagem();
		modelo.objecto.pos.y += OBJECTO_ALTURA * 0.5;
	}
	else {
		modelo.objecto.pos.x = qa.getPortas().at(nPorta).getPosicao().x;
		modelo.objecto.pos.z = qa.getPortas().at(nPorta).getPosicao().y;
		modelo.objecto.pos.y = OBJECTO_ALTURA * 0.5;

	}
}

void createDisplayLists(int janelaID, Quarto toDraw)
{
	modelo.labirinto[janelaID] = glGenLists(2);
	glNewList(modelo.labirinto[janelaID], GL_COMPILE);
	glPushAttrib(GL_COLOR_BUFFER_BIT | GL_CURRENT_BIT | GL_ENABLE_BIT);
	glPopAttrib();
	//desenhaEixos();
	glEndList();

	modelo.chao[janelaID] = 1;
	glNewList(modelo.chao[janelaID], GL_COMPILE);
	glPushAttrib(GL_COLOR_BUFFER_BIT | GL_CURRENT_BIT | GL_ENABLE_BIT);
	renderMap(modelo.texID[janelaID][ID_TEXTURA_CHAO], toDraw);
	glPopAttrib();
	glEndList();
	n++;
}


///////////////////////////////////
/// Movimento Rato
void motionNavigateSubwindow(int x, int y)
{
	int dif;
	dif = y - modelo.yMouse;
	if (dif > 0){//olhar para baixo
		estado.camera.dir_lat -= dif*RAD(EYE_ROTACAO);
		if (estado.camera.dir_lat < -RAD(45))
			estado.camera.dir_lat = -RAD(45);
	}

	if (dif<0){//olhar para cima
		estado.camera.dir_lat += abs(dif)*RAD(EYE_ROTACAO);
		if (estado.camera.dir_lat>RAD(45))
			estado.camera.dir_lat = RAD(45);
	}

	dif = x - modelo.xMouse;

	if (dif > 0){ //olhar para a direita
		estado.camera.dir_long -= dif*RAD(EYE_ROTACAO);
		/*
		if(estado.camera.dir_long<modelo.objecto.dir-RAD(45))
		estado.camera.dir_long=modelo.objecto.dir-RAD(45);
		*/
	}
	if (dif < 0){//olhar para a esquerda
		estado.camera.dir_long += abs(dif)*RAD(EYE_ROTACAO);
		/*
		if(estado.camera.dir_long>modelo.objecto.dir+RAD(45))
		estado.camera.dir_long=modelo.objecto.dir+RAD(45);
		*/

	}
	modelo.xMouse = x;
	modelo.yMouse = y;
}

void mouseNavigateSubwindow(int button, int state, int x, int y)
{
	if (button == GLUT_RIGHT_BUTTON)
	{
		if (state == GLUT_DOWN)
		{
			modelo.xMouse = x;
			modelo.yMouse = y;
			glutMotionFunc(motionNavigateSubwindow);
		}
		else
			glutMotionFunc(NULL);
	}
	if (button == GLUT_LEFT_BUTTON)
	{
		if (state == GLUT_UP)
		{
			estado.teclas.disparo = GL_TRUE;
		}
	}

}

////////////////////////////////////
/// Iluminaçăo e materiais

void setMaterial()
{
	GLfloat mat_specular[] = { 0.8f, 0.8f, 0.8f, 1.0f };
	GLfloat mat_shininess = 104;

	// criaçăo automática das componentes Ambiente e Difusa do material a partir das cores
	glEnable(GL_COLOR_MATERIAL);
	glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);

	// definir de outros parâmetros dos materiais estáticamente
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, mat_shininess);
}

///////////////////////////////////
//// Redisplays

void redisplayTopSubwindow(int width, int height)
{
	// glViewport(botom, left, width, height)
	// define parte da janela a ser utilizada pelo OpenGL
	glViewport(50, 50, (GLint)width, (GLint)height);
	// Matriz Projeccao
	// Matriz onde se define como o mundo e apresentado na janela
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60, (GLfloat)width / height, .5, 100);
	// Matriz Modelview
	// Matriz onde săo realizadas as tranformacoes dos modelos desenhados
	glMatrixMode(GL_MODELVIEW);

}

void reshapeNavigateSubwindow(int width, int height)
{
	// glViewport(botom, left, width, height)
	// define parte da janela a ser utilizada pelo OpenGL
	glViewport(0, 0, (GLint)width, (GLint)height);
	// Matriz Projeccao
	// Matriz onde se define como o mundo e apresentado na janela
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(estado.camera.fov, (GLfloat)width / height, 0.1, 50);
	// Matriz Modelview
	// Matriz onde săo realizadas as tranformacoes dos modelos desenhados
	glMatrixMode(GL_MODELVIEW);
}

void reshapeMainWindow(int width, int height)
{
	GLint w, h;
	w = (width - GAP * 3)*.5;
	h = (height - GAP * 2);
	//glutSetWindow(estado.topSubwindow);
	//glutPositionWindow(GAP, GAP);
	//glutReshapeWindow(w, h);
	glutSetWindow(estado.navigateSubwindow);
	glutPositionWindow(0, 0);
	glutReshapeWindow(width, height);

}

void strokeCenterString(char *str, double x, double y, double z, double s)
{
	int i, n;

	n = strlen(str);
	glPushMatrix();
	glTranslated(x - glutStrokeLength(GLUT_STROKE_ROMAN, (const unsigned char*)str)*0.5*s, y - 119.05*0.5*s, z);
	glScaled(s, s, s);
	for (i = 0; i < n; i++)
		glutStrokeCharacter(GLUT_STROKE_ROMAN, (int)str[i]);
	glPopMatrix();

}

// DEBUG
void desenhaEixos()
{
	// x
	glBegin(GL_LINES);
	glColor3f(1, 0, 0); //RED
	glVertex3f(4, 0, 0);
	glVertex3f(0, 0, 0);
	glEnd();

	// y
	glBegin(GL_LINES);
	glColor3f(0, 1, 0); //GREEN
	glVertex3f(0, 4, 0);
	glVertex3f(0, 0, 0);
	glEnd();

	// z
	glBegin(GL_LINES);
	glColor3f(0, 0, 1); //BLUE
	glVertex3f(0, 0, 4);
	glVertex3f(0, 0, 0);
	glEnd();
}

// passar 0,1,2,3,4. Outro numero usa a textura default

void desenhaBussola(int width, int height)  // largura e altura da janela
{

	// Altera viewport e projecçăo para 2D (copia de um reshape de um projecto 2D)

	glViewport(width - 60, 0, 60, 60);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(-30, 30, -30, 30);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	// Blending (transparencias)
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_COLOR_MATERIAL);


	//desenha bussola 2D
	glLineWidth(1.0);
	glColor3f(1, 0.4, 0.4);
	strokeCenterString("N", 0, 20, 0, 0.1); // string, x ,y ,z ,scale

	// ropőe estado
	glDisable(GL_BLEND);
	glEnable(GL_LIGHTING);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_DEPTH_TEST);


	//repőe projecçăo chamando redisplay
	reshapeNavigateSubwindow(width, height);

}

void desenhaAngVisao(camera_t *cam)
{
	GLfloat ratio;
	ratio = (GLfloat)glutGet(GLUT_WINDOW_WIDTH) / glutGet(GLUT_WINDOW_HEIGHT); // proporçăo 
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthMask(GL_FALSE);

	glPushMatrix();
	glTranslatef(cam->eye.x, OBJECTO_ALTURA, cam->eye.z);
	glColor4f(0, 0, 1, 0.2);
	glRotatef(GRAUS(cam->dir_long), 0, 1, 0);

	glBegin(GL_TRIANGLES);
	glVertex3f(0, 0, 0);
	glVertex3f(5 * cos(RAD(cam->fov*ratio*0.5)), 0, -5 * sin(RAD(cam->fov*ratio*0.5)));
	glVertex3f(5 * cos(RAD(cam->fov*ratio*0.5)), 0, 5 * sin(RAD(cam->fov*ratio*0.5)));
	glEnd();
	glPopMatrix();

	glDepthMask(GL_TRUE);
	glDisable(GL_BLEND);
}

void desenhaModelo()
{
	glColor3f(0, 1, 0);
	glutSolidCube(OBJECTO_ALTURA);
	glPushMatrix();
	glColor3f(1, 0, 0);
	glTranslatef(0, OBJECTO_ALTURA*0.75, 0);
	glRotatef(GRAUS(estado.camera.dir_long - modelo.objecto.dir), 0, 1, 0);
	glutSolidCube(OBJECTO_ALTURA*0.5);
	glPopMatrix();
}

void drawViewVolume(GLfloat x1, GLfloat x2, GLfloat y1,
	GLfloat y2, GLfloat z1, GLfloat z2)
{
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_LINE_LOOP);
	glVertex3f(x1, y1, -z1);
	glVertex3f(x2, y1, -z1);
	glVertex3f(x2, y2, -z1);
	glVertex3f(x1, y2, -z1);
	glEnd();

	glBegin(GL_LINE_LOOP);
	glVertex3f(x1, y1, -z2);
	glVertex3f(x2, y1, -z2);
	glVertex3f(x2, y2, -z2);
	glVertex3f(x1, y2, -z2);
	glEnd();

	glBegin(GL_LINES);	/*  4 lines	*/
	glVertex3f(x1, y1, -z1);
	glVertex3f(x1, y1, -z2);
	glVertex3f(x1, y2, -z1);
	glVertex3f(x1, y2, -z2);
	glVertex3f(x2, y1, -z1);
	glVertex3f(x2, y1, -z2);
	glVertex3f(x2, y2, -z1);
	glVertex3f(x2, y2, -z2);
	glEnd();
}

/* processHits prints out the contents of the selection array
*/
vector<Colisao> processHits(GLint hits, GLuint buffer[])
{
	vector<Colisao> colisoes;
	GLuint *ptr;
	int names;

	ptr = (GLuint *)buffer;
	for (int i = 0; i < hits; i++) {	/*  for each hit  */
		names = (int)*ptr;
		Colisao col;
		ptr++;
		col.z1 = (float)*ptr;
		ptr++;
		col.z2 = (float)*ptr;
		ptr++;
		col.nome = *ptr;
		if (names > 1)
			col.indice = *(++ptr);
		colisoes.push_back(col);
	}

	return colisoes;
}

#define BUFSIZE 512

void desenhaInimigos(vector<objecto_t> inimigos, bool modoColisao);

vector<Colisao> desenhaLabirintoColisoes(GLdouble left, GLdouble right, GLdouble bottom, GLdouble top, GLdouble zNear, GLdouble zFar, bool paraInimigos){
	GLuint selectBuf[BUFSIZE];
	GLint hits;
	glSelectBuffer(BUFSIZE, selectBuf);
	(void)glRenderMode(GL_SELECT);
	glInitNames();
	glPushMatrix();
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	// rever o volume...

	glOrtho(left, right, bottom, top, zNear, zFar);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();


	vector<Bloco> blocos = q.getBlocos();

	desenhaLabirinto2(blocos, VISTA_NAVIGATE);
	desenhaAlcapoes(q.getAlcapoes());

	desenhaPortas(q.getPortas(), 1, 1);

	if (!paraInimigos)
		desenhaInimigos(q.getInimigos(), true);

	glPopMatrix();
	glFlush();
	hits = glRenderMode(GL_RENDER);

	return processHits(hits, selectBuf);
}


void restorePerspectiveProjection() {

	glMatrixMode(GL_PROJECTION);
	// restore previous projection matrix
	glPopMatrix();

	// get back to modelview mode
	glMatrixMode(GL_MODELVIEW);
}

void setOrthographicProjection() {

	// switch to projection mode
	glMatrixMode(GL_PROJECTION);

	// save previous matrix which contains the
	//settings for the perspective projection
	glPushMatrix();

	// reset matrix
	glLoadIdentity();

	// set a 2D orthographic projection
	gluOrtho2D(0, 200, 200, 0);

	// switch back to modelview mode
	glMatrixMode(GL_MODELVIEW);
}

void displayText(int x, int y, char* text, int color)
{
	//color 0 = yellow, 1 = blue
	setOrthographicProjection();

	glPushMatrix();
	glDisable(GL_LIGHTING);
	glLoadIdentity();
	int leng, ig;
	if (color == 0){
		glColor3f(1.0f, 1.0f, 0.0f);
	}
	else {
		glColor3f(0.0f, 0.0f, 1.0f);
	}
	glRasterPos2f(x, y);
	leng = (int)strlen(text);
	for (ig = 0; ig < leng; ig++)
	{
		glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, text[ig]);
	}
	glPopMatrix();

	restorePerspectiveProjection();
}

void setNavigateSubwindowCamera(camera_t *cam, objecto_t obj)
{
	pos_t center;

	if (estado.vista[VISTA_NAVIGATE])
	{
		// 3ª pessoa
		cam->eye.x = obj.pos.x;
		cam->eye.y = obj.pos.y + .2;
		cam->eye.z = obj.pos.z;

		center.x = obj.pos.x + cos(cam->dir_long) * cos(cam->dir_lat);
		center.z = obj.pos.z + sin(-cam->dir_long) * cos(cam->dir_lat);
		center.y = cam->eye.y + sin(cam->dir_lat);
	}
	else
	{
		// 1ª pessoa
		center.x = obj.pos.x;
		center.y = obj.pos.y + 0.3;
		center.z = obj.pos.z;

		cam->eye.x = center.x - cos(cam->dir_long);
		cam->eye.z = center.z - sin(-cam->dir_long);
		cam->eye.y = center.y + sin(cam->dir_lat);

	}
	gluLookAt(cam->eye.x, cam->eye.y, cam->eye.z, center.x, center.y, center.z, 0, 1, 0);
}


/////////////////////////////////////
//topSubwindow
void setTopViewCamera(camera_t *cam, objecto_t obj)
{
	vector<pos_t> lim = q.generateDimensoes();
	GLdouble lookX = obj.pos.x;
	GLdouble lookZ = obj.pos.z;
	cout << "homer z : " << -obj.pos.z << endl << "limite z : " <<lim[2].y << endl;
	//limite horizontal
	if ((obj.pos.x) >= lim[2].x-4)
	{
		lookX = lim[2].x-4;
	}
	else {
		if ((obj.pos.x) <= lim[0].x + 4)
		{
			lookX = lim[0].x + 4;
		}
	}

	//limite vertical
	if ((obj.pos.z) >= lim[2].y - 3)
	{
		lookZ = lim[2].y - 3;
	}
	else {
		if ((obj.pos.z) <= lim[0].y + 3)
		{
			lookZ = lim[0].y + 3;
		}
	}

	gluLookAt(lookX, 10 * 4, lookZ, lookX, obj.pos.y,lookZ, 0, 0, -1);
}

void desenhaHUD(int width, int height)
{
	int vPort[4];

	glGetIntegerv(GL_VIEWPORT, vPort);

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();

	glOrtho(0, vPort[2], 0, vPort[3], -1, 1);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glDisable(GL_LIGHTING);
	glLoadIdentity();

	//Barra cinzenta
	glBegin(GL_POLYGON);
	glColor3f(0.6, 0.6, 0.6);
	glVertex2d(0, 0);
	glVertex2d(0, 0.05*glutGet(GLUT_WINDOW_HEIGHT));
	glVertex2d(0.175*glutGet(GLUT_WINDOW_WIDTH), 0.05*glutGet(GLUT_WINDOW_HEIGHT));
	glVertex2d(0.175*glutGet(GLUT_WINDOW_WIDTH), 0);
	glEnd();

	//100 - 175
	//vida - pixel
	//pixel = 175*vida/100
	//pixel = vida*175/100


	GLint pixel = ((modelo.objecto.vida * (0.17*glutGet(GLUT_WINDOW_WIDTH))) / 100);
	if (modelo.objecto.vida <= 2)
		pixel = 0.004*glutGet(GLUT_WINDOW_WIDTH);

	glBegin(GL_POLYGON);
	glColor3f(0.0, 1.0, 0.0);
	glVertex2d(0.004*glutGet(GLUT_WINDOW_WIDTH), 0.004*glutGet(GLUT_WINDOW_HEIGHT));
	glVertex2d(0.004*glutGet(GLUT_WINDOW_WIDTH), 0.045*glutGet(GLUT_WINDOW_HEIGHT));
	glVertex2d(pixel, 0.045*glutGet(GLUT_WINDOW_HEIGHT));
	glVertex2d(pixel, 0.004*glutGet(GLUT_WINDOW_HEIGHT));
	glEnd();


	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
	//repoe projecçao chamando redisplay
	reshapeNavigateSubwindow(width, height);



}

void desenhaMiniArrow()
{
	glBegin(GL_TRIANGLES);
	glVertex3f(-0.3, -0.3, 0.0);
	glVertex3f(-0.3, 0.3, 0.0);
	glVertex3f(0.5, 0.0, 0.0);
	glEnd();
}

void desenhaMiniDot()
{
	float x, y;
	float radius = 0.2f;
	glBegin(GL_LINES);

	x = (float)radius * cos(359 * M_PI / 180.0f);
	y = (float)radius * sin(359 * M_PI / 180.0f);
	for (int j = 0; j < 360; j++)
	{
		glVertex2f(x, y);
		x = (float)radius * cos(j * M_PI / 180.0f);
		y = (float)radius * sin(j * M_PI / 180.0f);
		glVertex2f(x, y);
	}
	glEnd();
}

void desenhaMiniMapa(int width, int height)
{
	// Altera viewport e projecçao para 2D (copia de um reshape de um projecto 2D)

	glViewport(width - (width / 4), 0, width / 4, height / 4);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	//gluPerspective(30, (float)(width) / (float)(height), 0.1, 1000);
	glOrtho(-4,4,-3,3,-1000,1000);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glEnable(GL_SCISSOR_TEST);
	glScissor(width - (width / 4), 0, width / 4, height / 4);
	glClear(GL_DEPTH_BUFFER_BIT);
	glDisable(GL_SCISSOR_TEST);
	// Blending (transparencias)
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);

	//Colocar câmara topView
	camera_t cameraTopView;
	cameraTopView.dir_long = 0;
	cameraTopView.fov = 60;
	setTopViewCamera(&cameraTopView, modelo.objecto);
	//setLight(estado, q);
	glDisable(GL_TEXTURE_2D);
	glCallList(modelo.labirinto[VISTA_TOP]);

	glCallList(modelo.chao[VISTA_TOP]);

	glPushMatrix();
	glTranslatef(modelo.objecto.pos.x, modelo.objecto.pos.y, modelo.objecto.pos.z);
	glRotatef(GRAUS(modelo.objecto.dir), 0, 1, 0);
	glRotatef(-90, 1, 0, 0);
	glColor3f(0.0f, 0.0f, 1.0f);

	// Simbolo do homer
	desenhaMiniArrow();
	//desenhaMiniDot();
	glPopMatrix();

	// Desenhar os pontos dos inimigos
	vector<objecto_t> inimigos = q.getInimigos();
	for (int i = 0; i < q.getNumInimigos(); i++)
	{
		if (inimigos.at(i).vida != 0)
		{
			glPushMatrix();
			glTranslatef(q.getInimigo(i).pos.x, 0.28, -q.getInimigo(i).pos.y);
			glRotatef(GRAUS(q.getInimigo(i).dir), 0, 1, 0);
			glRotatef(-90, 1, 0, 0);
			glColor3f(1.0f, 0.0f, 0.0f);
			desenhaMiniArrow();
			//desenhaMiniDot();
			glPopMatrix();
		}
	}
	// ropoe estado
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);
	//repoe projecçao chamando redisplay
	reshapeNavigateSubwindow(width, height);
}

string floatToStringWithPrecision(float x, int p)
{
	ostringstream strout;
	strout << fixed << setprecision(p) << x;
	string str = strout.str();
	size_t end = str.find_last_not_of('0') + 1;
	return str.erase(end);
}

void uploadGlui_cb(int op)
{
	string ficheiro_caminho, nome_caminho;
	switch (op)
	{
	case 1:
		webs.sendScore(estado.ficheiroMapa, p.getTime());

		ficheiro_caminho = CAMINHOS_LOCATION + string(estado.ficheiroMapa) + "_" + webs.getNickName() + ".path";
		nome_caminho = string(estado.ficheiroMapa) + "_" + webs.getNickName();

		caminho.exportToFile(ficheiro_caminho);
		webs.SendPath(estado.ficheiroMapa + string(".mtdts"), nome_caminho, ficheiro_caminho);
		reiniciarJogo();
		upload->hide();
		break;
	case 2:
		reiniciarJogo();
		upload->hide();
		break;
	default:
		break;
	}
}

void mudarQuarto(Porta po)
{
	Quarto qa;

	//int indice;
	string id = po.getQuartoDestino();
	for (Quarto qua : mapa.getQuartos())
	{
		for (size_t i = 0; i < qua.getPortas().size(); i++)
		{
			if (qua.getPortas().at(i).getId() == id)
			{
				qa = qua;
				nPorta = i;
			}
		}
	}

	//cout << "nextRoom: " << qa.getID() << " ; this room= " << q.getID() << endl;
	if (stoi(qa.getID()) != stoi(q.getID()))
	{
		glDeleteLists(modelo.chao[VISTA_NAVIGATE], 1);
		q = qa;
		modelo.quartoAtual = stringToChar(q.getID());
		createDisplayLists(VISTA_NAVIGATE, q);
		Porta pa = q.getPortas().at(nPorta);
		//printf("Porta: \nx - %f\ny - %f\nz - %f\nHomer: \nx - %f\ny - %f\nz - %f\n", pa.getPosicao().x, pa.getPosicao().y, pa.getPosicao().z, modelo.objecto.pos.x, modelo.objecto.pos.y, modelo.objecto.pos.z);
		//float aux = ((pa.getPosicao().y + 0.15) - pa.getPosicao().y - 0.15) / (pa.getPosicao().x + 0.15 - pa.getPosicao().x - 0.15);

		//float rot = pa.getRotacao()-GRAUS(atan(aux));
		//modelo.objecto.dir = RAD(pa.getRotacao());
		//cout << "direccao: " << GRAUS(modelo.objecto.dir) << endl;
		modelo.objecto.pos.x = pa.getPosicao().x + cos(modelo.objecto.dir)*(modelo.objecto.vel*0.05);

		modelo.objecto.pos.z = -pa.getPosicao().y + sin(-modelo.objecto.dir)*(modelo.objecto.vel*0.05);
		emPorta = true;

	}
	else {
		emPausa = TRUE;
		p.stop();
		upload = GLUI_Master.create_glui("Game Over!", 0, glutGet(GLUT_WINDOW_WIDTH) / 2, glutGet(GLUT_WINDOW_HEIGHT) / 2);

		upload->add_separator();

		char pontuacaoFinal[] = "Score: ";
		char showPontuacao[40];
		char* pontuacaoFloat = stringToChar(to_string(p.getTime()));
		strcpy(showPontuacao, pontuacaoFinal);
		strcat(showPontuacao, pontuacaoFloat);
		pontuacao = upload->add_statictext(showPontuacao);

		upload->add_separator();

		uploadBot = upload->add_button("Send Score", 1, (GLUI_Update_CB)uploadGlui_cb);
		restartBot = upload->add_button("Restart", 2, (GLUI_Update_CB)uploadGlui_cb);
		exitBot = upload->add_button("Exit", 0, (GLUI_Update_CB)exit);
		upload->set_main_gfx_window(estado.mainWindow);
		go = stringToChar("Game Over! Tempo: " + floatToStringWithPrecision(p.getTime(), 2));
		q = mapa.getQuartoInicial();
		glDeleteLists(modelo.chao[VISTA_NAVIGATE], 1);
		createDisplayLists(VISTA_NAVIGATE, q);
	}
}

void trataColisoes(){
	if (!emPorta)
	{
		vector<Colisao> cols;

		if (estado.teclas.colisoes)
		{
			cols = desenhaLabirintoColisoes(modelo.objecto.nextPos.x - 0.15, modelo.objecto.nextPos.x + 0.15,
				modelo.objecto.nextPos.y + 20, modelo.objecto.nextPos.y - 20,
				-modelo.objecto.nextPos.z - 0.15, -modelo.objecto.nextPos.z + 0.15, false);
		}

		modelo.aCair = GL_FALSE;
		int cairFrame = 0;

		if (cols.size() == 0){
			modelo.objecto.pos.x = modelo.objecto.nextPos.x;
			modelo.objecto.pos.z = modelo.objecto.nextPos.z;
			float alturaInicial = mapa.getCoordInicioPersonagem().y + OBJECTO_ALTURA * 0.5;

			if (!estado.aEntrarQuatroSecreto){
				if (modelo.objecto.pos.y > alturaInicial){
					modelo.objecto.pos.y -= OBJECTO_VELOCIDADE_QUEDA;
					modelo.aCair = GL_TRUE;
					cairFrame++;
					if (cairFrame == 1)
						if (modelo.objecto.vida >= 12){
							modelo.objecto.vida -= 0.18;
						}
						else {
							modelo.objecto.vida = 0;
						}
				}
				else{
					modelo.objecto.pos.y = alturaInicial;
				}
			}
			else{
				if (modelo.objecto.nextPos.y > alturaInicial){
					modelo.objecto.nextPos.y -= OBJECTO_VELOCIDADE_QUEDA;
					modelo.objecto.pos.y = modelo.objecto.nextPos.y;
				}
				else{
					modelo.objecto.pos.y = alturaInicial;
					estado.aEntrarQuatroSecreto = false;
				}
			}
			modelo.aColidir = GL_FALSE;
		}
		else{
			float altura;
			Alcapao al;
			if (cols[0].nome > 3000)
			{
				int s = q.getPortaById(cols[0].nome - NOME_PORTAS);
				if (s != -1)
				{
					nPorta = s;
					Porta po = q.getPortas().at(s);
					mudarQuarto(po);
				}

			}
			else {
				switch (cols[0].nome){
					case NOME_PORTA_SECRETA:
						modelo.objecto.pos.x = modelo.objecto.nextPos.x;
						modelo.objecto.pos.z = modelo.objecto.nextPos.z;
						modelo.aColidir = GL_FALSE;
						break;
					case NOME_ALCAPOES:
						dohFX.switchAudio(AL_TRUE);

						al = q.getAlcapoes()[cols[0].indice];
						if (al.isSecreto()) estado.aEntrarQuatroSecreto = true;

						modelo.objecto.pos.x = modelo.objecto.nextPos.x;
						modelo.objecto.pos.z = modelo.objecto.nextPos.z;
						modelo.objecto.pos.y -= OBJECTO_VELOCIDADE_QUEDA;
						if (modelo.homer[VISTA_NAVIGATE].GetSequence() != 20) modelo.homer[VISTA_NAVIGATE].SetSequence(20);
						if (modelo.objecto.pos.y < -1.5){
							if (estado.aEntrarQuatroSecreto){
								modelo.objecto.nextPos = al.getPosicaoDestino();
							}
							else
								reiniciarJogo();
						}
						break;

					case NOME_OBSTACULOS:

						altura = 0.5;
						if (modelo.objecto.pos.y >= altura - 0.21 &&
							modelo.objecto.pos.y <= altura + 0.21){
							modelo.objecto.pos.x = modelo.objecto.nextPos.x;
							modelo.objecto.pos.z = modelo.objecto.nextPos.z;
							modelo.objecto.pos.y = altura + OBJECTO_ALTURA * 0.5;
							modelo.aColidir = GL_FALSE;
						}
						else{
							modelo.aColidir = GL_TRUE;

						}
						break;

					default:
						if (cols[0].nome > 0 && cols[0].nome < NOME_PAREDES)
						{
							if (q.getBlocos()[cols[0].nome].isRampa()){
								float novaAltura = q.getBlocos()[cols[0].nome].getAlturaEmPonto(modelo.objecto.nextPos);

								if (abs(novaAltura - (modelo.objecto.pos.y - OBJECTO_ALTURA * 0.5) <= 0.2)){
									modelo.objecto.pos.x = modelo.objecto.nextPos.x;
									modelo.objecto.pos.z = modelo.objecto.nextPos.z;
									modelo.objecto.pos.y = novaAltura + OBJECTO_ALTURA * 0.5;
									modelo.aColidir = GL_FALSE;
								}
							}
							else{
								altura = q.getBlocos()[cols[0].nome].getAlturaMaxima();
								if (modelo.objecto.pos.y >= altura - 0.21 &&
									modelo.objecto.pos.y <= altura + 0.21){
									modelo.objecto.pos.x = modelo.objecto.nextPos.x;
									modelo.objecto.pos.z = modelo.objecto.nextPos.z;
									modelo.objecto.pos.y = altura + OBJECTO_ALTURA * 0.5;
									modelo.aColidir = GL_FALSE;
								}
								else{
									modelo.aColidir = GL_TRUE;

								}
							}
						}
						if (cols[0].nome >= NOME_INIMIGO_MIN && cols[0].nome < NOME_INIMIGO_MAX)
						{
							modelo.objecto.vida -= 5;
							if (modelo.objecto.vida <= 0)
							{
								go = stringToChar("Game Over! Tempo: " + floatToStringWithPrecision(p.getTime(), 2));
								p.stop();
								modelo.objecto.vida = 0;
								reiniciarJogo();
							}

							break;
						}

				}
			}
		}
	}
	else {
		modelo.objecto.pos.x = modelo.objecto.nextPos.x;
		modelo.objecto.pos.z = modelo.objecto.nextPos.z;
		if ((sqrt((pow(modelo.objecto.nextPos.x - q.getPortas().at(nPorta).getPosicao().x, 2) +
			(pow(modelo.objecto.nextPos.z - (-q.getPortas().at(nPorta).getPosicao().y), 2))))) > 0.25)
		{
			emPorta = false;
		}
	}

	vector<Colisao> cols;

	//Colisoes dos projecteis
	vector<Projectil>::iterator it = projecteis.begin();
	for (; it != projecteis.end();) {
		pos_t posicao = it->getPosicaoActual();
		cols = desenhaLabirintoColisoes(posicao.x - 0.05, posicao.x + 0.05,
			posicao.y + 20, posicao.y,
			-posicao.z - 0.05, -posicao.z + 0.05, false);

		if (cols.size() > 0){
			//TODO EFEITOS ESPECIAIS
			//explosao aqui?
			for (int i = 0; i < 20; i++)
			{
				Projectil proj(it->getPosicaoActual(), (modelo.objecto.dir + RAD((int)(rand() % 50)+170)), VELOCIDADE_EXPLOSAO);
				explosao.push_back(proj);
			}
			it = projecteis.erase(it);
			if (cols[0].nome >= NOME_INIMIGO_MIN && cols[0].nome < NOME_INIMIGO_MIN + q.getNumInimigos())
			{
				int indexInimigo = cols[0].nome - NOME_INIMIGO_MIN;
				objecto_t inimigo = q.getInimigo(indexInimigo);
				inimigo.vida -= 10;
				if (inimigo.vida <= 0)
					q.killInimigo(indexInimigo);
				else
					q.updateInimigo(inimigo, indexInimigo);
				break;
			}

		}
		else
			++it;
	}

	vector<objecto_t> inimigos = q.getInimigos();
	for (size_t i = 0; i < inimigos.size(); i++)
	{
		cols = desenhaLabirintoColisoes(inimigos[i].nextPos.x - 0.15, inimigos[i].nextPos.x + 0.15,
			inimigos[i].nextPos.z + 20, inimigos[i].nextPos.z - 20,
			inimigos[i].nextPos.y - 0.15, inimigos[i].nextPos.y + 0.15, true);

		if (cols.size() == 0){
			if (inimigos[i].pos.x != inimigos[i].nextPos.x || inimigos[i].pos.y != inimigos[i].nextPos.y){
				inimigos[i].pos = inimigos[i].nextPos;
				q.updateInimigo(inimigos[i], i);
				if (modelo.inimigo[VISTA_NAVIGATE].GetSequence() != 3) modelo.inimigo[VISTA_NAVIGATE].SetSequence(3);
			}
			else{
				if (modelo.inimigo[VISTA_NAVIGATE].GetSequence() != 0) modelo.inimigo[VISTA_NAVIGATE].SetSequence(0);
			}
		}
	}
}

void billboardCheatSphericalBegin() {

	float modelview[16];
	int i, j;

	// save the current modelview matrix
	glPushMatrix();

	// get the current modelview matrix
	glGetFloatv(GL_MODELVIEW_MATRIX, modelview);

	// undo all rotations
	// beware all scaling is lost as well 
	for (i = 0; i < 3; i++)
		for (j = 0; j < 3; j++) {
			if (i == j)
				modelview[i * 4 + j] = 1.0;
			else
				modelview[i * 4 + j] = 0.0;
		}

	// set the modelview with no rotations
	glLoadMatrixf(modelview);
}

void desenhaHealth(int life)
{
	//0.4 = 100
	//intervalo = life
	double pixel = -0.2 + ((life*0.4) / 100);
	billboardCheatSphericalBegin();
	glTranslatef(0.0, 0.2, 0.0);
	glBegin(GL_POLYGON);
	glColor3f(1.0, 0.0, 0.0);
	glVertex2d(-0.2, 0.1);
	glVertex2d(-0.2, 0.13);
	glVertex2d((pixel), 0.13);
	glVertex2d((pixel), 0.1);
	glEnd();
	glPopMatrix();
}

void desenhaInimigos(vector<objecto_t> inimigos, bool modoColisao)
{
	//ENEMIES
	for (size_t i = 0; i < inimigos.size(); i++)
	{
		if (inimigos.at(i).vida != 0)
		{
			glPushName(NOME_INIMIGO_MIN + i);
			glPushMatrix();
			glTranslatef(inimigos.at(i).pos.x, 0.28, -inimigos.at(i).pos.y);


			if (!modoColisao){
				glPushMatrix();
				glRotatef(GRAUS(inimigos.at(i).dir), 0, 1, 0);
				glRotatef(-90, 1, 0, 0);
				glScalef(SCALE_ZOMBIE, SCALE_ZOMBIE, SCALE_ZOMBIE);
				mdlviewer_display(modelo.inimigo[VISTA_NAVIGATE]);
				glPopMatrix();
				desenhaHealth(inimigos.at(i).vida);
			}
			else{
				glScalef(0.5, 1, 0.5);
				desenhaCubo(0, modelo.texID[VISTA_NAVIGATE][ID_TEXTURA_CUBOS]);
			}
			glPopMatrix();
			glPopName();
		}
	}
}

void desenhaExplosao()
{
	vector<Projectil>::iterator it = explosao.begin();
	GLfloat colors[3][3] =
	{
		{ 1.0f, 0.0f, 0.0f }, { 1.0f, 0.6f, 0.0f }, { 1.0f, 1.0f, 0.0f }
	};
	if (!explosao.empty()) {
		for (; it != explosao.end();)  {
			glColor3fv(colors[(int)(rand() % 3)]);
			glMatrixMode(GL_RENDER_MODE);
			glPushMatrix();
			pos_t posicao =it->getPosicaoActual();
			glTranslatef(posicao.x, posicao.y, posicao.z);
			glutSolidSphere(0.01, 4, 4);
			it->mover(1);
			glPopMatrix();
			if (it->getDistanciaPercorrida() > 1) {
				it = explosao.erase(it);
				explosaoFX.switchAudio(AL_FALSE);
			}
			else {
				++it;
				explosaoFX.switchAudio(AL_TRUE);
			}
		}
	}
}

void desenhaProjectil()
{
	for (size_t i = 0; i < projecteis.size(); i++)
	{
		glPushMatrix();
		pos_t posicao = projecteis[i].getPosicaoActual();

		glTranslatef(posicao.x, posicao.y, posicao.z);
		glutSolidSphere(0.025, 10, 10);
		projecteis[i].mover(0);
		glPopMatrix();
	}

}

void displayNavigateSubwindow()
{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glLoadIdentity();

		setNavigateSubwindowCamera(&estado.camera, modelo.objecto);
		setLight(estado, q, modelo.objecto);

		//glCallList(modelo.labirinto[VISTA_NAVIGATE]);
		glCallList(modelo.chao[VISTA_NAVIGATE]);

		if (!estado.vista[VISTA_NAVIGATE])
		{
			//HOMER
			glPushMatrix();
			glTranslatef(modelo.objecto.pos.x, modelo.objecto.pos.y, modelo.objecto.pos.z);
			glRotatef(GRAUS(modelo.objecto.dir), 0, 1, 0);
			glRotatef(-90, 1, 0, 0);
			glScalef(SCALE_HOMER, SCALE_HOMER, SCALE_HOMER);
			mdlviewer_display(modelo.homer[VISTA_NAVIGATE]);
			glPopMatrix();
			//INIMIGOS
			desenhaInimigos(q.getInimigos(), false);

			//RAIN
			chuva.drawRain();
			desenhaCeu(q.generateDimensoes());
			desenhaProjectil();
			desenhaExplosao();

			trataColisoes();

		}

		desenhaHUD(glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));
		desenhaMiniMapa(glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));

		p.start();

		char* disp = stringToChar("Tempo: " + to_string(p.getTimeInSeconds())); // em segundos
		//char* disp = stringToChar("Tempo: " + floatToStringWithPrecision(p.getTime(), 2)); // em segundos.centésimos de segundo
		displayText(170, 10, disp, 0);
		displayText(2, 20, go, 0);
		glutSwapBuffers();
}

void redisplayAll(void)
{
	glutSetWindow(estado.mainWindow);
	glutPostRedisplay();
	glutSetWindow(estado.navigateSubwindow);
	glutPostRedisplay();
}

void displayMainWindow()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glutSwapBuffers();
}

/************************
LOGICA
*************************/






void moverPersonagem(float velocidade)
{
	modelo.andar = GL_FALSE;

	modelo.objecto.nextPos.x = modelo.objecto.pos.x;
	modelo.objecto.nextPos.z = modelo.objecto.pos.z;

	if (estado.teclas.up){
		modelo.objecto.nextPos.x = modelo.objecto.pos.x + cos(modelo.objecto.dir)*velocidade;
		modelo.objecto.nextPos.z = modelo.objecto.pos.z + sin(-modelo.objecto.dir)*velocidade;
		if (!modelo.aColidir)
			modelo.andar = GL_TRUE;

	}
	if (estado.teclas.down){
		modelo.objecto.nextPos.x = modelo.objecto.pos.x - cos(modelo.objecto.dir)*velocidade;
		modelo.objecto.nextPos.z = modelo.objecto.pos.z - sin(-modelo.objecto.dir)*velocidade;
		if (!modelo.aColidir)
			modelo.andar = GL_TRUE;
	}

	if (estado.teclas.left){
		modelo.objecto.dir += RAD(OBJECTO_ROTACAO);
		estado.camera.dir_long += RAD(OBJECTO_ROTACAO);
	}
	if (estado.teclas.right){
		modelo.objecto.dir -= RAD(OBJECTO_ROTACAO);
		estado.camera.dir_long -= RAD(OBJECTO_ROTACAO);
	}
}

void moverPersonagem(float velocidade, pos_t nextPos)
{
	modelo.andar = GL_FALSE;
	pos_t posAtual = modelo.objecto.pos;
	if (posAtual.x != nextPos.x || posAtual.y != nextPos.y || posAtual.z != nextPos.z ){
		if (!modelo.aColidir)
			modelo.andar = GL_TRUE;
	}
}

void playReplay(float velocidade)
{
	for (Quarto qua : mapa.getQuartos()){

		if (qua.getID() == caminho.getIdQuarto(caminho.getTickReproducao()) && qua.getID() != q.getID()) // erro aqui pk a leitura do ficheiro está a retornar erros no vector dos quarto
		{
			glDeleteLists(modelo.chao[VISTA_NAVIGATE], 1);
			q = qua;
			createDisplayLists(VISTA_NAVIGATE, q);
			break;
		}
	}
	if (caminho.getTickReproducao() == 0)
		reiniciarCamara();

	objecto_t it = caminho.getPos(caminho.getTickReproducao());

	pos_t posAtual = modelo.objecto.pos;
	pos_t posSeguinte = it.pos;
	moverPersonagem(velocidade, posSeguinte);

	estado.camera.dir_long = it.dir;
	modelo.objecto = it;
	if (!caminho.avancaTickTempo())
		estado.teclas.replay = !estado.teclas.replay;
}

time_t getCurrentTime() {
	time_t current;
	time(&current);
	return current;
}

void Timer(int value)
{
	if (emPausa == FALSE) {
		GLuint curr = GetTickCount();
		float velocidade = modelo.objecto.vel*(curr - modelo.prev)*0.001;
		if (getCurrentTime() >= tempoTiro + 1) {
			tiroFX.switchAudio(AL_FALSE);
		}

		if (modelo.homer[VISTA_NAVIGATE].GetSequence() != 20)
		{
			glutTimerFunc(estado.timer, Timer, 0);
		}
		else
			if (value < 4500)
			{
				glutTimerFunc(estado.timer, Timer, value + curr - modelo.prev);
				redisplayAll();
				return;
			}
			else
			{
				modelo.homer[VISTA_NAVIGATE].SetSequence(0);
				glutTimerFunc(estado.timer, Timer, 0);
			}

		modelo.prev = curr;

		if (estado.teclas.replay)
		{
			if (!estado.modoReplay)
			{
				caminho.importFromFile(ficheiroCaminho);
				estado.modoReplay = !estado.modoReplay;

			}
			playReplay(velocidade);
		}
		else
		{
			moverPersonagem(velocidade);
			caminho.addPosicao(modelo.quartoAtual, modelo.objecto);

		}

		// DISPARAR PROJETIL
		if (estado.teclas.disparo)
		{
			Projectil projectil(modelo.objecto.pos, modelo.objecto.dir, VELOCIDADE_PROJECTIL);
			projecteis.push_back(projectil);
			estado.teclas.disparo = GL_FALSE;
			tempoTiro = getCurrentTime();
			tiroFX.switchAudio(AL_TRUE);
		}

		if (modelo.homer[VISTA_NAVIGATE].GetSequence() != 20)
		{
			if (estado.aEntrarQuatroSecreto){
				modelo.homer[VISTA_NAVIGATE].SetSequence(20);

			}
			else
				if (modelo.aCair && modelo.homer[VISTA_NAVIGATE].GetSequence() != 23)
					modelo.homer[VISTA_NAVIGATE].SetSequence(23);
				else
					if (!modelo.aCair && modelo.andar && modelo.homer[VISTA_NAVIGATE].GetSequence() != 3)
						modelo.homer[VISTA_NAVIGATE].SetSequence(3);
					else
						if (!modelo.aCair &&!modelo.andar && modelo.homer[VISTA_NAVIGATE].GetSequence() != 0)
							modelo.homer[VISTA_NAVIGATE].SetSequence(0);

		}
		CheckCloseNPC(estado, modelo.objecto.pos, q);

		backgroundMusic.switchAudio(estado.teclas.som);


		redisplayAll();
	} else {
		glutTimerFunc(estado.timer, Timer, 0);
	}
}



void imprime_ajuda(void)
{
	printf("\n\nDesenho de um quadrado\n");
	printf("h,H - Ajuda \n");
	printf("******* Debug ******* \n");
	printf("c,C - Ativar/destivar colisoes");
	printf("******* Diversos ******* \n");
	printf("k,K - Liga/Desliga luz dinamica (ex. lanterna)\n");
	printf("l,L - Alterna entre modo de dia e noite\n");
	printf("w,W - Wireframe \n");
	printf("f,F - Fill \n");
	printf("s,S - Solid. retira texturas. \n");
	printf("a,A - tocar/parar o som. \n");
	printf("r,R - reiniciar o jogo. \n");
	printf("click rato esquerdo - disparar projeteis. \n");
	printf("p,P - reiniciar camara. \n");
	printf("******* Movimento ******* \n");
	printf("up  - Acelera \n");
	printf("down- Trava \n");
	printf("left- Vira para a esquerda\n");
	printf("righ- Vira para a direita\n");
	printf("******* Camara ******* \n");
	printf("F1 - Alterna camara da janela da Esquerda \n");
	printf("F2 - Alterna camara da janela da Direita \n");
	printf("PAGE_UP, PAGE_DOWN - Altera abertura da camara \n");
	printf("botao esquerdo + movimento na Janela da Direita altera o olhar \n");
	printf("ESC - Sair\n");
}


/************************
INPUT
*************************/

void Key(unsigned char key, int x, int y)
{
	vector<pos_t> pos = q.generateDimensoes();
	switch (key) {
		case 27:
			exit(1);
			break;
		case 'h':
		case 'H':
			imprime_ajuda();
			break;
		case 'k':
		case 'K':
			if (estado.teclas.lanterna)
				estado.teclas.lanterna = AL_FALSE;
			else
				estado.teclas.lanterna = AL_TRUE;
			break;
		case 'l':
		case 'L':
			if (estado.teclas.luz)
				estado.teclas.luz = AL_FALSE;
			else
				estado.teclas.luz = AL_TRUE;
			break;
		case 'w':
		case 'W':
			glutSetWindow(estado.navigateSubwindow);
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			glDisable(GL_TEXTURE_2D);
			glutSetWindow(estado.topSubwindow);
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			glDisable(GL_TEXTURE_2D);
			break;
		case 's':
		case 'S':
			glutSetWindow(estado.navigateSubwindow);
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			glDisable(GL_TEXTURE_2D);
			glutSetWindow(estado.topSubwindow);
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			glDisable(GL_TEXTURE_2D);
			break;
		case 'f':
		case 'F':
			glutSetWindow(estado.navigateSubwindow);
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			glEnable(GL_TEXTURE_2D);
			glutSetWindow(estado.topSubwindow);
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			glEnable(GL_TEXTURE_2D);
			break;
		case 'a':
		case 'A':
			estado.teclas.som = !estado.teclas.som;
			break;
		case 'r':
		case 'R':
			reiniciarJogo();
			break;
		case 'c':
		case 'C':
			estado.teclas.colisoes = !estado.teclas.colisoes;
			break;
		case 'p':
		case 'P':
			reiniciarCamara();
			break;
		case 'n':
		case 'N':
			estado.teclas.replay = !estado.teclas.replay;
			break;
		case '1':
			if (fileExists(MUSIC_BACKGROUND_FILE))
			{
				if (estado.teclas.som)
				{
					backgroundMusic.switchAudio(GL_FALSE);
					backgroundMusic.InitAudio(MUSIC_BACKGROUND_FILE);
					backgroundMusic.switchAudio(GL_TRUE);
				}
				else
					backgroundMusic.InitAudio(MUSIC_BACKGROUND_FILE);
			}
			break;
		case '2':
			if (fileExists(MUSIC_BACKGROUND_FILE_CUSTOM))
			{
				if (estado.teclas.som)
				{
					backgroundMusic.switchAudio(GL_FALSE);
					backgroundMusic.InitAudio(MUSIC_BACKGROUND_FILE_CUSTOM);
					backgroundMusic.switchAudio(GL_TRUE);
				}
				else
					backgroundMusic.InitAudio(MUSIC_BACKGROUND_FILE_CUSTOM);
			}
			break;
	}

}
void SpecialKey(int key, int x, int y)
{

	switch (key) {
		case GLUT_KEY_UP: estado.teclas.up = GL_TRUE;
			break;
		case GLUT_KEY_DOWN: estado.teclas.down = GL_TRUE;
			break;
		case GLUT_KEY_LEFT: estado.teclas.left = GL_TRUE;
			break;
		case GLUT_KEY_RIGHT: estado.teclas.right = GL_TRUE;
			break;
		case GLUT_KEY_F1: estado.vista[VISTA_TOP] = !estado.vista[VISTA_TOP];
			break;
		case GLUT_KEY_F2: estado.vista[VISTA_NAVIGATE] = !estado.vista[VISTA_NAVIGATE];
			break;
		case GLUT_KEY_PAGE_UP:
			if (estado.camera.fov > 20)
			{
				estado.camera.fov--;
				glutSetWindow(estado.navigateSubwindow);
				reshapeNavigateSubwindow(glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));
				redisplayAll();
			}
			break;
		case GLUT_KEY_PAGE_DOWN:
			if (estado.camera.fov < 130)
			{
				estado.camera.fov++;
				glutSetWindow(estado.navigateSubwindow);
				reshapeNavigateSubwindow(glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));
				redisplayAll();
			}
			break;
	}

}
// Callback para interaccao via teclas especiais (largar na tecla)
void SpecialKeyUp(int key, int x, int y)
{
	switch (key) {
		case GLUT_KEY_UP: estado.teclas.up = GL_FALSE;
			break;
		case GLUT_KEY_DOWN: estado.teclas.down = GL_FALSE;
			break;
		case GLUT_KEY_LEFT: estado.teclas.left = GL_FALSE;
			break;
		case GLUT_KEY_RIGHT: estado.teclas.right = GL_FALSE;
			break;
	}
}

////////////////////////////////////
// Inicializaçőes

void init(string mapFileName)
{
	backgroundMusic.InitAudio(MUSIC_BACKGROUND_FILE);
	
	estado.teclas.som = AL_FALSE;
	dohFX.InitAudio(MUSIC_DOH_FILE);
	tiroFX.InitAudio(MUSIC_FIRE_FILE);
	explosaoFX.InitAudio(MUSIC_EXPLOSION_FILE);

	GLfloat amb[] = { 0.3f, 0.3f, 0.3f, 1.0f };

	string mapLocation = MAPA_FILE;

	mapa = leFicheiro(mapLocation);

	caminho.setNomeMapa(mapFileName);

	q = mapa.getQuartoInicial();

	estado.timer = 100;

	estado.ficheiroMapa = stringToChar(mapFileName);

	estado.teclas.colisoes = true;

	estado.camera.eye.x = 0;
	estado.camera.eye.y = OBJECTO_ALTURA * 2;
	estado.camera.eye.z = 0;
	estado.camera.dir_long = 0;
	estado.camera.dir_lat = 0;
	estado.camera.fov = 60;

	estado.localViewer = 1;
	estado.vista[VISTA_NAVIGATE] = 0;

	estado.aEntrarQuatroSecreto = false;

	modelo.objecto.pos = mapa.getCoordInicioPersonagem();
	modelo.objecto.pos.y += OBJECTO_ALTURA*.5;
	modelo.objecto.pos.z = 0;

	modelo.objecto.dir = 0;
	modelo.objecto.vel = OBJECTO_VELOCIDADE;
	modelo.objecto.vida = 100;

	modelo.objecto.nextPos = mapa.getCoordInicioPersonagem();
	modelo.objecto.nextPos.y += OBJECTO_ALTURA*.5;

	modelo.xMouse = modelo.yMouse = -1;
	modelo.andar = GL_FALSE;
	modelo.aColidir = GL_FALSE;
	Quarto quartoInicial = mapa.getQuartoInicial();

	modelo.quartoAtual = stringToChar(quartoInicial.getID());

	glEnable(GL_DEPTH_TEST);
	glShadeModel(GL_SMOOTH);
	glEnable(GL_POINT_SMOOTH);
	glEnable(GL_LINE_SMOOTH);
	glEnable(GL_POLYGON_SMOOTH);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_NORMALIZE);  // por causa do Scale ao Homer

	if (glutGetWindow() == estado.mainWindow)
		glClearColor(0.8f, 0.8f, 0.8f, 0.0f);
	else
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, amb);
}

void myGlutIdle(void)
{
	/* According to the GLUT specification, the current window is
	undefined during an idle callback.  So we need to explicitly change
	it if necessary */
	if (glutGetWindow() != estado.mainWindow)
		glutSetWindow(estado.mainWindow);

	glutPostRedisplay();

	glui->sync_live();
}

void glui_callback(int op)
{
	vector<FileListElement> maps;
	int i, seleccionado;
	string nomeMapa;
	switch (op)
	{
		case 1:
			if (webs.loginService(username, password) == -1)
			{
				mensagemErroLogin->set_text("Error trying to login! Try again.");
				break;
			}
			else{
				maps = webs.getMapList();

				char mensagemBemVindo[] = "Welcome ";
				char mensagemLogin[31];
				strcpy(mensagemLogin, mensagemBemVindo);
				strcat(mensagemLogin, username);
				mensagemErroLogin->set_text(mensagemLogin);
				loginBot->disable();
				exitBot->disable();
				usernameTxt->disable();
				passwordTxt->disable();
			}
			glui->add_separator();
			glui->add_statictext("Select map:");
			mapas = glui->add_listbox("Map:");

			i = 0;
			for each (auto var in maps)
			{
				mapas->add_item(i, WebService::wchar_to_char(var.fileName));
				i++;
			}
			selectBot = glui->add_button("Select", 3, (GLUI_Update_CB)glui_callback);
			exitFim = glui->add_button("Exit", 0, (GLUI_Update_CB)exit);
			break;
		case 3:
			mensagemErroMapa = glui->add_statictext("");
			////DOWNLOAD DO MAPA COMPLETO COM AS TEXTURAS E MUSICAS
			seleccionado = mapas->get_int_val();
			glui->add_separator();
			mensagemErroMapa->set_text("A carregar o mapa...");
			maps = webs.getMapList();
			nomeMapa = webs.getMapComplete(maps.at(seleccionado));
			if (nomeMapa != "")
				mensagemErroMapa->set_text("Map loaded correctly!");
			else
				mensagemErroMapa->set_text("Error trying to load the map!");

			// Registar callbacks do GLUT da janela principal
			estado.ficheiroMapa = stringToChar(nomeMapa);
			init(estado.ficheiroMapa);
			glutReshapeFunc(reshapeMainWindow);
			glutDisplayFunc(displayMainWindow);

			glutTimerFunc(estado.timer, Timer, 0);
			glutKeyboardFunc(Key);
			glutSpecialFunc(SpecialKey);
			glutSpecialUpFunc(SpecialKeyUp);


			createDisplayLists(VISTA_TOP, q);

			// criar a sub window navigateSubwindow
			estado.navigateSubwindow = glutCreateSubWindow(estado.mainWindow, 0, 0, 1024, 600);
			//init(estado.ficheiroMapa);
			setLight(estado, q, modelo.objecto);
			setMaterial();

			createTextures(modelo.texID[VISTA_NAVIGATE]);

			createDisplayLists(VISTA_NAVIGATE, q);
			mdlviewer_init(MODELO_PERSONAGEM, modelo.homer[VISTA_NAVIGATE]);
			mdlviewer_init(MODELO_INIMIGO, modelo.inimigo[VISTA_NAVIGATE]);

			glutReshapeFunc(reshapeNavigateSubwindow);
			glutDisplayFunc(displayNavigateSubwindow);
			glutMouseFunc(mouseNavigateSubwindow);

			glutTimerFunc(estado.timer, Timer, 0);
			glutKeyboardFunc(Key);
			glutSpecialFunc(SpecialKey);
			glutSpecialUpFunc(SpecialKeyUp);
			glui->hide();
			break;
		default:
			break;
	}
}

MODELO s;

void renderLoginScene() {

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	glEnable(GL_TEXTURE_2D);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glBindTexture(GL_TEXTURE_2D, s.texID[VISTA_NAVIGATE][ID_TEXTURA_MENU_BACKGROUND]); // erro parece estar aqui

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glBegin(GL_POLYGON);
	glColor3f(0.3, 0.4, 0.6);

	glTexCoord2f(0, 0);
	glVertex3f(-1.0, 1.0, 0.0);

	glTexCoord2f(0, 1);
	glVertex3f(-1.0, -1.0, 0.0);

	glTexCoord2f(1, 1);
	glVertex3f(1.0, -1.0, 0.0);

	glTexCoord2f(1, 0);
	glVertex3f(1.0, 1.0, 0.0);

	glEnd();
	glBindTexture(GL_TEXTURE_2D, NULL);

	glutSwapBuffers();
}

/////////////////////////////////////
int main(int argc, char **argv)
{
	//////////////////////////////////////////////////////////////////////////
	//TEMPORARIO
	//webs.teste("p1","qwerty");
	//////////////////////////////////////////////////////////////////////////
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowPosition(10, 10);
	glutInitWindowSize(1024, 600);
	if ((estado.mainWindow = glutCreateWindow("Labirinto")) == GL_FALSE)
		exit(1);

	/*	glutGameModeString("1024x768:32@75"); // 1ş teste
	if (glutGameModeGet(GLUT_GAME_MODE_POSSIBLE))
	glutEnterGameMode();
	else
	{
	glutGameModeString("800x600:32@60"); // 2ş teste
	if (glutGameModeGet(GLUT_GAME_MODE_POSSIBLE))
	glutEnterGameMode();
	else // Cria Janela Normal
	{
	glutInitWindowPosition(10, 10);
	glutInitWindowSize(800, 600);
	if ((estado.mainWindow = glutCreateWindow("Labirinto")) == GL_FALSE)
	exit(1);
	}
	}*/


	createBGTextures(s.texID[VISTA_NAVIGATE]);

	glutDisplayFunc(renderLoginScene);

	////char username[20], password[20];
	glui = GLUI_Master.create_glui("Login", 0, glutGet(GLUT_WINDOW_WIDTH) / 2, glutGet(GLUT_WINDOW_HEIGHT) / 2);

	alutInit(&argc, argv);

	// Código para criar janela de Login.
	usernameTxt = glui->add_edittext("Username:", GLUI_EDITTEXT_TEXT, &username);
	passwordTxt = glui->add_edittext("Password:", GLUI_EDITTEXT_TEXT, &password);
	loginBot = glui->add_button("Login", 1, (GLUI_Update_CB)glui_callback);
	exitBot = glui->add_button("Exit", 0, (GLUI_Update_CB)exit);
	glui->add_separator();
	mensagemErroLogin = glui->add_statictext("");

	imprime_ajuda();

	glui->set_main_gfx_window(estado.mainWindow);

	GLUI_Master.set_glutIdleFunc(myGlutIdle);

	/*

	estado.ficheiroMapa = stringToChar("demo3.mtdts");
	init(estado.ficheiroMapa);
	glutReshapeFunc(reshapeMainWindow);
	glutDisplayFunc(displayMainWindow);

	glutTimerFunc(estado.timer, Timer, 0);
	glutKeyboardFunc(Key);
	glutSpecialFunc(SpecialKey);
	glutSpecialUpFunc(SpecialKeyUp);


	createDisplayLists(VISTA_TOP, q);

	// criar a sub window navigateSubwindow
	estado.navigateSubwindow = glutCreateSubWindow(estado.mainWindow, 0, 0, 1024, 600);
	init(estado.ficheiroMapa);
	setLight(estado, q);
	setMaterial();

	createTextures(modelo.texID[VISTA_NAVIGATE]);
	//q = mapa.getQuartos().at(0);
	createDisplayLists(VISTA_NAVIGATE, q);
	mdlviewer_init("homer.mdl", modelo.homer[VISTA_NAVIGATE]);
	mdlviewer_init("zombie.mdl", modelo.rivalModel[VISTA_NAVIGATE]);

	glutReshapeFunc(reshapeNavigateSubwindow);
	glutDisplayFunc(displayNavigateSubwindow);
	glutMouseFunc(mouseNavigateSubwindow);

	glutTimerFunc(estado.timer, Timer, 0);
	glutKeyboardFunc(Key);
	glutSpecialFunc(SpecialKey);
	glutSpecialUpFunc(SpecialKeyUp);
	*/
	glutMainLoop();

	return 0;
}