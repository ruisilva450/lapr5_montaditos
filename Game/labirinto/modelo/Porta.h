#pragma once
#include <iostream>

class Porta
{
private:

	string id, quartoDestino;
	pos_t posicao;
	float rotacao;

public:
	Porta(string id, pos_t posicao, float rotacao);
	Porta(string id, string quartoDestino, pos_t posicao, float rotacao);
	~Porta();

	string getId();
	string getQuartoDestino();
	const pos_t getPosicao();
	float getRotacao();

	void draw(int janelaID);
};


Porta::Porta(string id, pos_t posicao, float rotacao)
{
	this->id = id;
	this->posicao = posicao;
	this->rotacao = rotacao;
	this->quartoDestino = id;
}

Porta::Porta(string id, string quartoDestino, pos_t posicao, float rotacao) : Porta(id, posicao, rotacao)
{
	this->quartoDestino = quartoDestino;
}

Porta::~Porta()
{
}

string Porta::getId()
{
	return id;
}

string Porta::getQuartoDestino()
{
	return quartoDestino;
}

const pos_t Porta::getPosicao()
{
	return posicao;
}

float Porta::getRotacao()
{
	return rotacao;
}
