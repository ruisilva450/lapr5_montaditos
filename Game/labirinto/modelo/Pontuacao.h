/*
	C�digo retirado do site http://www.cplusplus.com/forum/beginner/317/ no dia 17/12
*/
#include <iostream>
#include <conio.h>
#include <time.h>	

class Pontuacao {
public:
	Pontuacao();
	void           start();
	void           stop();
	void           reset();
	bool           isRunning();
	unsigned long  getTimeInSeconds();
	float		   getTime();
	bool           isOver(unsigned long seconds);
private:
	bool           resetted;
	bool           running;
	unsigned long  beg;
	unsigned long  end;
};


Pontuacao::Pontuacao() {
	resetted = true;
	running = false;
	beg = 0;
	end = 0;
}


void Pontuacao::start() {
	if (!running) {
		if (resetted)
			beg = (unsigned long)clock();
		else
			beg -= end - (unsigned long)clock();
		running = true;
		resetted = false;
	}
}


void Pontuacao::stop() {
	if (running) {
		end = (unsigned long)clock();
		running = false;
	}
}


void Pontuacao::reset() {
	bool wereRunning = running;
	if (wereRunning)
		stop();
	resetted = true;
	beg = 0;
	end = 0;
	if (wereRunning)
		start();
}


bool Pontuacao::isRunning() {
	return running;
}


unsigned long Pontuacao::getTimeInSeconds() {
	if (running)
		return ((unsigned long)clock() - beg) / CLOCKS_PER_SEC;
	else
		return end - beg;
}


float Pontuacao::getTime() {
	float time;
	time = ((float)clock() - beg) / CLOCKS_PER_SEC;
	return time;
}


bool Pontuacao::isOver(unsigned long seconds) {
	return seconds >= getTime();
}
