
class Projectil
{
public:
	Projectil(pos_t posO, float direcao, float veloc);
	~Projectil();
	void mover(int op);
	float getDistanciaPercorrida();
	pos_t getPosicaoActual();

private:
	pos_t posOrigem;
	pos_t posActual;
	float direcao;
	float velocidade;
};

Projectil::Projectil(pos_t posO, float dir, float veloc)
{
	this->posOrigem = this->posActual=posO;
	this->direcao = dir;
	this->velocidade = veloc;
}

Projectil::~Projectil()
{
}

void Projectil::mover(int op)
{
	this->posActual.x = this->posActual.x + cos(this->direcao)*this->velocidade;
	this->posActual.z = this->posActual.z + sin(-this->direcao)*this->velocidade;
	if (op == 1)
	{
		this->posActual.y = this->posActual.y + (0.005 * ((int)(rand() % 3) + (-1)) *((int)(rand() % 9) + 1));
	}
}

float Projectil::getDistanciaPercorrida()
{
	return sqrt((pow(posActual.x - posOrigem.x, 2) + (pow(posActual.y - posOrigem.y, 2)) + (pow(posActual.z - posOrigem.z, 2))));
}

pos_t Projectil::getPosicaoActual()
{
	return posActual;
}