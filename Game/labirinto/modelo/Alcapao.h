class Alcapao
{
public:
	Alcapao();
	Alcapao(pos_t posicao);
	Alcapao(pos_t posicao, string quartoDestino, pos_t destino);
	~Alcapao();
	bool isSecreto();
	pos_t getPosicao();
	pos_t getPosicaoDestino();

private:
	pos_t posicao, destino;
	string quartoDestino;
	bool secreto;
};

Alcapao::Alcapao()
{
}

Alcapao::Alcapao(pos_t posicao)
{
	this->posicao = posicao;
	secreto = false;
}

Alcapao::Alcapao(pos_t posicao, string quartoDestino, pos_t destino)
{
	this->posicao = posicao;
	this->destino = destino;
	this->quartoDestino = quartoDestino;
	secreto = true;
}

Alcapao::~Alcapao()
{
}

bool Alcapao::isSecreto(){
	return secreto;
}

pos_t Alcapao::getPosicao(){
	return posicao;
}

pos_t Alcapao::getPosicaoDestino(){
	return destino;
}

