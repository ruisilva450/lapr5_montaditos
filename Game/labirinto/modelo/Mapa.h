#pragma once
#include <vector>
#include "Quarto.h"

class Mapa
{
private:
	string idQuartoInicial;
	pos_t coordenadasInicioPersonagem;
	string idQuartoFinal;
	pos_t posicaoDestino;
	vector<Quarto> quartos;

public:
	Mapa();
	Mapa(string idQuartoInicial, pos_t coordenadasInicioPersonagem);
	Mapa(string idQuartoInicial, pos_t coordenadasInicioPersonagem, const vector<Quarto>& quartos);
	~Mapa();
	void addQuarto(Quarto quarto);
	const vector<Quarto>& getQuartos();
	const Quarto getQuartoInicial();
	const pos_t getCoordInicioPersonagem();
	void setIdQuartoFinal(string idPorta);
	string getIdQuartoFinal();
	void setPosDestino(pos_t posDestino);
	pos_t getPosDestino();
};

Mapa::Mapa(){}

Mapa::Mapa(string idQuartoInicial, pos_t coordenadasInicioPersonagem){
	this->idQuartoInicial = idQuartoInicial;
	this->coordenadasInicioPersonagem = coordenadasInicioPersonagem;
}

Mapa::Mapa(string idQuartoInicial, pos_t coordenadasInicioPersonagem, const vector<Quarto>& quartos) : Mapa(idQuartoInicial, coordenadasInicioPersonagem)
{
	this->quartos = quartos;
}

Mapa::~Mapa()
{
}

void Mapa::addQuarto(Quarto quarto){
	quartos.push_back(quarto);
}

const vector<Quarto>& Mapa::getQuartos(){
	return quartos;
}

const Quarto Mapa::getQuartoInicial()
{
	for each (Quarto item in quartos)
	{
		if (item.isThisQuarto(idQuartoInicial))
		{
			return item;
		}
	}
	return quartos.front();
}

const pos_t Mapa::getCoordInicioPersonagem()
{
	return this->coordenadasInicioPersonagem;
}

void Mapa::setIdQuartoFinal(string idPorta)
{
	this->idQuartoFinal = idPorta;
}

std::string Mapa::getIdQuartoFinal()
{
	return this->idQuartoFinal;
}

void Mapa::setPosDestino(pos_t posDestino)
{
	this->posicaoDestino = posDestino;
}

pos_t Mapa::getPosDestino()
{
	return posicaoDestino;
}
