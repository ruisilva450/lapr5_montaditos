#include <fstream>

class Caminho
{
public:
	Caminho();
	Caminho(Caminho &c);
	~Caminho();
	void setNomeMapa(string nomeMapa);
	void addPosicao(string quarto, objecto_t posicao);
	string getIdQuarto(unsigned long index);
	objecto_t getPos(unsigned long index);
	void exportToFile(string nomeFicheiro);
	void importFromFile(string nomeFicheiro);
	int getTickReproducao();
	bool avancaTickTempo();
private:
	vector<string> quarto;
	vector<objecto_t> percurso;
	int tickReproducao;
	string nomeMapa;
};

Caminho::Caminho()
{
	tickReproducao = 0;
}

Caminho::Caminho(Caminho &c)
{
	this->quarto = c.quarto;
	this->percurso = c.percurso;
	this->tickReproducao = 0;
}

Caminho::~Caminho()
{
}

void Caminho::setNomeMapa(string nomeMapa){
	this->nomeMapa = nomeMapa;
}

void Caminho::addPosicao(string quarto, objecto_t posicao)
{
	this->quarto.push_back(quarto);
	this->percurso.push_back(posicao);
}

string Caminho::getIdQuarto(unsigned long index)
{
	return quarto.at(index);
}

objecto_t Caminho::getPos(unsigned long index)
{
	return percurso.at(index);

}

void Caminho::exportToFile(string nomeFicheiro){
	ofstream ofs(nomeFicheiro, ios::binary);
	ofs.write((char *)this, sizeof(*this));
	ofs.close();
	//FILE * pFile1;
	//errno_t err1;
	//err1 = fopen_s(&pFile1, nomeFicheiro.c_str(), "wb");
	//if (err1 == 0)
	//{
	//	//TODO ler e alocar ao proprio ficheiro
	//}
	
}

void Caminho::importFromFile(string nomeFicheiro){
	ifstream ifs(nomeFicheiro, ios::binary);
	ifs.read((char *)this, sizeof(*this));
	ifs.close();
}

int Caminho::getTickReproducao()
{
	return tickReproducao;
}

bool Caminho::avancaTickTempo()
{
	if ((size_t)tickReproducao < percurso.size()-1)
	{
		tickReproducao++;
		return true;
	}
	return false;
}
