#pragma once

class Luz
{
private:
	pos_t posicao;
	float corR, corG, corB, alpha;

public:
	Luz(pos_t posicao, float corR, float corG, float corB, float alpha);
	~Luz();
	pos_t getPosicao();
};

Luz::Luz(pos_t posicao, float corR, float corG, float corB, float alpha)
{
	this->posicao = posicao;
	this->corR = corR;
	this->corG = corG;
	this->corB = corB;
	this->alpha = alpha;
}

Luz::~Luz()
{
}

pos_t Luz::getPosicao()
{
	return this->posicao;
}