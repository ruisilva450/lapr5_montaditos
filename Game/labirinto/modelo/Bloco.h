#pragma once

class Bloco
{
private:
	pos_t posicaoCentral, posInicial, posFinal;
	float comprimento, rotacao, anguloDeclive, zi, zf;
	bool isSolido, muro;

public:
	Bloco(pos_t posInicial, pos_t posFinal,bool isSecreto);
	~Bloco();

	vector<pos_t> getPosicoes();
	float getRotacao();
	float getComprimento();
	pos_t getPosicaoCentral();
	pos_t getPosicaoInicial();
	pos_t getPosicaoFinal();
	float getAlturaMaxima();
	float getAlturaEmPonto(pos_t ponto);
	bool isParede();
	bool isRampa();
	bool isBlocoSolido();
	bool isMuro();
	void setMuro();
};

Bloco::Bloco(pos_t posInicial, pos_t posFinal, bool isSolido = true)
{
	posInicial.y = posInicial.y * -1;
	posFinal.y = posFinal.y * -1;
	this->posInicial = posInicial;
	this->posFinal = posFinal;
	this->posicaoCentral.x = (posFinal.x + posInicial.x) / 2;
	this->posicaoCentral.y = (posFinal.y + posInicial.y) / 2;
	this->posicaoCentral.z = (posFinal.z + posInicial.z) / 2;
	//this->rotacao = GRAUS(atan2(this->posicaoCentral.y, this->posicaoCentral.x));
	float aux = (this->posFinal.y - this->posInicial.y) / (this->posFinal.x - this->posInicial.x);
	this->rotacao = GRAUS(atan(aux));
	float altura = posFinal.z > posInicial.z ? posFinal.z : posInicial.z;
	this->anguloDeclive = GRAUS(atan(altura / sqrt((pow(posFinal.x - posInicial.x, 2) + (pow(posFinal.y - posInicial.y, 2))))));
	this->comprimento = sqrt((pow(posFinal.x - posInicial.x, 2) + (pow(posFinal.y - posInicial.y, 2)) + (pow(posFinal.z - posInicial.z, 2))));
	this->isSolido = isSolido;
	muro = false;
}

Bloco::~Bloco()
{
}

vector<pos_t> Bloco::getPosicoes()
{
	vector<pos_t> posicoes;
	posicoes.push_back(posicaoCentral);
	posicoes.push_back(posInicial);
	posicoes.push_back(posFinal);
	return posicoes;
}

float Bloco::getRotacao()
{
	return rotacao;
}

float Bloco::getComprimento()
{
	return comprimento;
}

pos_t Bloco::getPosicaoCentral()
{
	return posicaoCentral;
}

pos_t Bloco::getPosicaoInicial()
{
	return posInicial;
}

pos_t Bloco::getPosicaoFinal()
{
	return posFinal;
}

float Bloco::getAlturaEmPonto(pos_t ponto){
	pos_t pontoMaisBaixo = posInicial.z < posFinal.z ? posInicial : posFinal;
	float distancia = sqrt((pow(pontoMaisBaixo.x - ponto.x, 2) + (pow(pontoMaisBaixo.y - (-ponto.z), 2))));

	return tan(RAD(this->anguloDeclive))*distancia;

}

float Bloco::getAlturaMaxima(){
	return posInicial.z > posFinal.z ? posInicial.z : posFinal.z;
}

bool Bloco::isRampa(){
	return posInicial.z != posFinal.z;
}

bool Bloco::isBlocoSolido()
{
	return isSolido;
}

bool Bloco::isMuro()
{
	return muro;
}

void Bloco::setMuro()
{
	muro = true;
}

// TODO: � necess�rio alterar o parser para separar as paredes dos muros
/*
bool Bloco::isParede()
{
	if (posInicial.z == posFinal.z)
 	return true;
}
*/
