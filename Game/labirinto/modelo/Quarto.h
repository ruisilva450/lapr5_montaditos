#pragma once
#include <limits.h>
#include <vector>
#include "Bloco.h"
#include "Luz.h"
#include "Porta.h"
#include "Alcapao.h"

class Quarto
{
private:
	string id;
	vector<Bloco> blocos;
	vector<Porta> portas;
	vector<Luz> luzes;
	vector<objecto_t> inimigos;
	vector<Alcapao> alcapoes;
	vector<objecto_t> obstaculos;


public:
	Quarto();
	Quarto(string id, const vector<Bloco>& blocos, const vector<Porta>& portas, const vector<Luz>& luzes);
	Quarto(string id);
	~Quarto();
	void addBloco(Bloco bloco);
	void addLuz(Luz bloco);
	void addPorta(Porta bloco);
	void addInimigo(objecto_t Inimigo);
	void addAlcapoes(Alcapao alcapao);
	void addObstaculo(objecto_t obstaculo);
	boolean isThisQuarto(string id);
	vector<pos_t> generateDimensoes();
	string getID();
	vector<Bloco> getParedes();
	vector<Bloco> getBlocos();
	vector<Porta> getPortas();
	vector<pos_t> getPosLuzes();
	vector<objecto_t> getInimigos();
	objecto_t getInimigo(int index);
	int getNumInimigos();
	void updateInimigo(objecto_t inimigo, int index);
	void killInimigo(int index);
	vector<Alcapao> getAlcapoes();
	vector<objecto_t> getObstaculos();
	int getPortaById(int id);
};

Quarto::Quarto(){}

Quarto::Quarto(string id, const vector<Bloco>& blocos, const vector<Porta>& portas, const vector<Luz>& luzes)
{
	this->id = id;
	this->blocos = blocos;
	this->portas = portas;
	this->luzes = luzes;
}

Quarto::Quarto(string id){
	this->id = id;
}

Quarto::~Quarto()
{
}

void Quarto::addBloco(Bloco bloco){
	blocos.push_back(bloco);
}

void Quarto::addLuz(Luz luz){
	luzes.push_back(luz);
}

void Quarto::addPorta(Porta porta){
	portas.push_back(porta);
}

void Quarto::addInimigo(objecto_t inimigo){
	this->inimigos.push_back(inimigo);
}

void Quarto::updateInimigo(objecto_t inimigo, int index)
{
	this->inimigos.at(index) = inimigo;
}

void Quarto::addAlcapoes(Alcapao alcapao){
	alcapoes.push_back(alcapao);
}

void Quarto::addObstaculo(objecto_t obstaculo)
{
	obstaculos.push_back(obstaculo);
}

boolean Quarto::isThisQuarto(string id)
{
	return this->id == id;
}

vector<pos_t> Quarto::generateDimensoes()
{
	vector<Bloco> blocos = this->blocos;
	vector<pos_t> posicoes;
	
	float minX = MAXINT32;
	float maxX = MININT32;

	float minY = MAXINT32;
	float maxY = MININT32;

	for each (Bloco bloco in blocos)
	{
		posicoes = bloco.getPosicoes();
		for each (pos_t pos in posicoes)
		{
			if (pos.x > maxX)
				maxX = pos.x;

			if (pos.y > maxY)
				maxY = pos.y;

			if (pos.x < minX)
				minX = pos.x;

			if (pos.y < minY)
				minY = pos.y;
		}
	}

	vector<pos_t> extremos;

	pos_t inf_esq;
	inf_esq.x = maxX;
	inf_esq.y = minY;

	pos_t sup_esq;
	sup_esq.x = minX;
	sup_esq.y = minY;

	pos_t inf_dir;
	inf_dir.x = maxX;
	inf_dir.y = maxY;

	pos_t sup_dir;
	sup_dir.x = minX;
	sup_dir.y = maxY;

	extremos.push_back(sup_esq);
	extremos.push_back(inf_esq);
	extremos.push_back(inf_dir);
	extremos.push_back(sup_dir);

	return extremos;
}

string Quarto::getID()
{
	return id;
}

vector<pos_t> Quarto::getPosLuzes()
{
	vector<Luz> luzes = this->luzes;
	vector<pos_t> posLuzes;
	for each (Luz luz in luzes)
	{
		posLuzes.push_back(luz.getPosicao());
	}
	return posLuzes;
}

vector<Bloco> Quarto::getBlocos()
{
	return blocos;
}

vector<Porta> Quarto::getPortas()
{
	return portas;
}

objecto_t Quarto::getInimigo(int index)
{
	return inimigos.at(index);
}

vector<objecto_t> Quarto::getInimigos()
{
	return inimigos;
}


int Quarto::getNumInimigos()
{
	return this->inimigos.size();
}

void Quarto::killInimigo(int index){
	this->inimigos.at(index).vida = 0;
}

vector<Alcapao> Quarto::getAlcapoes()
{
	return alcapoes;
}

vector<objecto_t> Quarto::getObstaculos()
{
	return obstaculos;
}

int Quarto::getPortaById(int id)
{
	int res = -1;
	for (size_t i = 0; i < portas.size(); i++)
	{
		if (stoi(portas.at(i).getId()) == id)
		{
			res = i;
		}

	}
	return res;
}